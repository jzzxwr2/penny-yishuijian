﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Autodesk.Revit;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.PracticeR
{
    
    [Transaction(TransactionMode.Manual)]
    class PR02 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {

            var type = typeof(Element);
            var methods = type.GetMethods();
            var fields = type.GetFields();
            var properties = type.GetProperties();
            
            var slmethods = "";
            var slfields = "";
            var slproperties = "";

            foreach (var item in methods)
            {
                slmethods = slmethods + "\n" + item.Name;
            }
            MessageBox.Show(slmethods);
            var mefile = File.CreateText(@" C:\Repositories\methods.txt");
            mefile.WriteLine(slmethods);

            foreach (var item in fields)
            {
                slfields = slfields + "\n" + item.Name;
            }
            MessageBox.Show(slfields);
            var fiefile = File.CreateText(@" C:\Repositories\fields.txt");
            fiefile.WriteLine(slfields);

            foreach (var item in properties)
            {
                slproperties = slproperties + "\n" + item.Name;
            }
            MessageBox.Show(slproperties);
            var prfile = File.CreateText(@" C:\Repositories\properties.txt");
            prfile.WriteLine(slproperties);
            prfile.Close();

            return Result.Succeeded;
        }
    }
}
