﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;


namespace _09几何图元_标注_
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            //1.手动画一块楼板，选择楼板的顶面，获得面的闭合轮廓
            //var reference = sel.PickObject(ObjectType.Face,"选择");
            //var ele = doc.GetElement(reference);
            //var face = ele.GetGeometryObjectFromReference(reference) as Face;
            ////TaskDialog.Show("结果是",(face as PlanarFace).FaceNormal.ToString());

            //2.通过闭合轮廓创建拉伸，得到拉伸实体
            //var trans = new Transaction(doc, "修改");
            //trans.Start();
            //var solid = GeometryCreationUtilities.CreateExtrusionGeometry(face.GetEdgesAsCurveLoops(), XYZ.BasisZ, 100);
            //trans.Commit();
            ////TaskDialog.Show("结果是",solid.Volume.ToString());

            //3.通过实体生成内建模型

            //var trans2 = new Transaction(doc, "修改");
            //trans2.Start();
            //var directShape = DirectShape.CreateElement(doc, ele.Category.Id);
            //directShape.AppendShape(new List<GeometryObject> {solid});
            //trans2.Commit();

            //4.手动画一条墙标注这个墙的两端
            var wall = doc.GetElement(sel.PickObject(ObjectType.Element));
            var view= new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Views).
                WhereElementIsNotElementType().Where(o => o.Name == "标高 1").First() as View;
            var line = (wall.Location as LocationCurve).Curve as Line;
            var dimension= new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Dimensions).
                WhereElementIsNotElementType().Where(o => o.Name == "对角线 - 3mm RomanD").First() as Dimension;
            var refArray = new ReferenceArray();

            var opt = new Options();
            opt.ComputeReferences = true;
            opt.DetailLevel = ViewDetailLevel.Fine;
            var geometry = wall.get_Geometry(opt);//获取墙几何

            var array = new ReferenceArray();

            //foreach (var item in geometry)//遍历几何
            //{
            //    var solid = item as Solid;//获取实体
            //    if (solid !=null)
            //    {
            //        var face = solid.Faces;//获取面
            //        foreach (var item2 in face)
            //        {
            //            if ((item2 as PlanarFace).FaceNormal.IsAlmostEqualTo(new XYZ(0,0,1)))// 获取顶面
            //            {
            //                var edge = (item2 as Face).EdgeLoops.get_Item(0);
            //                foreach (var item3 in edge)
            //                {
            //                    var ed = item3 as Edge;
            //                    var ln = ed.AsCurve() as Line;
            //                    if (ln.Direction.IsAlmostEqualTo(new XYZ(0, 1, 0)) ||
            //                        ln.Direction.IsAlmostEqualTo(new XYZ(0, -1, 0)))//获取墙两端edge
            //                    {
            //                        array.Append(ed.Reference);
            //                    }
            //                }
            //            }

            //        }
            //    }
            //}
            //foreach (var item in geometry)//遍历几何
            //{
            //    var solid = item as Solid;//获取实体
            //    if (solid != null)
            //    {
            //        var face = solid.Faces;//获取面
            //        foreach (var item2 in face)
            //        {
            //            if ((item2 as PlanarFace).FaceNormal.IsAlmostEqualTo(line.Direction) ||
            //                (item2 as PlanarFace).FaceNormal.IsAlmostEqualTo(-line.Direction))// 获取墙两端面
            //            {
            //                var fa = item2 as Face;
            //                array.Append(fa.Reference);
            //            }
            //        }
            //    }
            //}
            TaskDialog.Show("结果是", array.Size.ToString());
            var trans3 = new Transaction(doc, "修改");
            trans3.Start();
            doc.Create.NewDimension(view, line, array, dimension.DimensionType);
            trans3.Commit();



            return Result.Succeeded;
        }
    }
}
