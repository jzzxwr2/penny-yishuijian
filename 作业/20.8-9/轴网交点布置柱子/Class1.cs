﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB.Structure;

namespace 轴网交点布置柱子
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            FilteredElementCollector collector = new FilteredElementCollector(doc);

            /*List<Line> gridlines = new List<Line>();
            List<XYZ> intPos = new List<XYZ>();

            var ele = collector.OfClass(typeof(Grid));
            foreach (Grid item in ele)
            {
                gridlines.Add(item.Curve as Line);
            }
            //TaskDialog.Show("结果是", gridlines.Count().ToString());

            foreach (Line ln1 in gridlines)
            {
                foreach (Line ln2 in gridlines)
                {
                    XYZ normal1 = ln1.Direction;//得到的是直线的方向向量
                    XYZ normal2 = ln2.Direction;

                    //如果两根轴线方向相同,则遍历下一组（目的是排除平行与重合的线）
                    if (normal1.IsAlmostEqualTo(normal2)) continue;
                    IntersectionResultArray results;
                    SetComparisonResult intRst = ln1.Intersect(ln2, out results);//如果两根轴线相交,则输出交点

                    if (intRst == SetComparisonResult.Overlap && results.Size == 1)//排除轴线是曲线，交点不止一个的情况
                    {
                        XYZ tp = results.get_Item(0).XYZPoint;//上面得到的交点
                        //比较得到的交点和intPos数组里面的元素是否相同，不同才Add到intPos数组中，作用是排除重复的点
                        if (intPos.Where(m => m.IsAlmostEqualTo(tp)).Count() == 0)
                        {
                            intPos.Add(tp); //收集所有的交点
                        }
                    }

                }
            }
            using (Transaction trans = new Transaction(doc))
            {
                trans.Start("任务开始");
                Level level = doc.GetElement(new ElementId(311)) as Level;//你的图上的标高的id
                FamilyInstance f = doc.GetElement(new ElementId(336647)) as FamilyInstance;//创建一个柱子,其Id为332661
                FamilySymbol familySymbol = f.Symbol;//判断族类型
                if (!familySymbol.IsActive)
                {
                    familySymbol.Activate();
                }
                foreach (XYZ p in intPos)
                {
                    //创建柱子
                    FamilyInstance familyInstance = doc.Create.NewFamilyInstance(p, familySymbol, level, StructuralType.NonStructural);
                }
                trans.Commit();
            }*/

            var columns = new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_StructuralColumns).WhereElementIsNotElementType().ToList();
            var grids = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Grids).WhereElementIsNotElementType().ToList();

            var tr = new Transaction(doc, "添加轴号");
            tr.Start();

            for (int i = 0; i < grids.Count; i++)
            {
                for (int j = i + 1; j < grids.Count; j++)
                {
                    (grids[i] as Grid).Curve.Intersect((grids[j] as Grid).Curve, out IntersectionResultArray result);
                    if (result != null && result.Size != 0)
                    {
                        var po = result.get_Item(0).XYZPoint;
                        foreach (var item in columns)
                        {
                            if ((item.Location as LocationPoint).Point.DistanceTo(po) < 1e-6)
                            {
                                if (double.TryParse(grids[i].Name, out double num))
                                {
                                    item.get_Parameter(BuiltInParameter.DOOR_NUMBER).Set(grids[i].Name + "-" + grids[j].Name);
                                }
                                else
                                {
                                    item.get_Parameter(BuiltInParameter.DOOR_NUMBER).Set(grids[j].Name + "-" + grids[i].Name);
                                }
                                columns.Remove(item);
                                break;
                            };
                        }
                    }
                }

            }

            tr.Commit();

            return Result.Succeeded;
        }
    }
}
