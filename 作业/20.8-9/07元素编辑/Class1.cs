﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;


namespace _07元素编辑
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            var ele1 = doc.GetElement(sel.PickObject(ObjectType.Element));
            //var ele2 = doc.GetElement(sel.PickObject(ObjectType.Element));

            //1.连接几何图形

            //var trans = new Transaction(doc,"连接几何图形");
            //trans.Start();
            //JoinGeometryUtils.JoinGeometry(doc, ele1, ele2);
            //trans.Commit();

            //var trans2 = new Transaction(doc, "取消连接几何图形");
            //trans2.Start();
            //JoinGeometryUtils.UnjoinGeometry(doc, ele1, ele2);
            //trans2.Commit();

            //2.镜像
            //Plane plane = (ele2 as ReferencePlane).GetPlane();
            //var trans = new Transaction(doc, "修改");
            //trans.Start();
            //ElementTransformUtils.MirrorElement(doc, ele1.Id, plane);
            //trans.Commit();

            //3.移动、复制
            //var trans = new Transaction(doc, "修改");
            //trans.Start();
            ////ElementTransformUtils.MoveElement(doc, ele1.Id, new XYZ(50, 60, 0));//移动
            //ElementTransformUtils.CopyElement(doc, ele1.Id, new XYZ(50, 60, 0));//复制
            //trans.Commit();

            //4.旋转
            var trans = new Transaction(doc, "修改");
            trans.Start();
            Line line = Line.CreateBound(new XYZ(0,0,1),XYZ.Zero);
            ElementTransformUtils.RotateElement(doc, ele1.Id, line, 90);
            trans.Commit();

            //5.锁定
            //var trans = new Transaction(doc, "修改");
            //trans.Start();
            ////ele1.Pinned=true;
            //ele1.Pinned = false;
            //trans.Commit();

            //6.手动放置一个门，将门转换为零件（思路：获取到门的solid一个个转换）



            return Result.Succeeded;
        }
    }
}
