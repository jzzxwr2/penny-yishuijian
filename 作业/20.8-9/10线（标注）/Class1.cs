﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;


namespace _10线_标注_
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;
            //1.手动画一面墙
            //1.1获得墙的顶面
            //1.2将顶面的四条边转换为模型线
            //1.3修改模型线的线样式为细线
            //1.4平行的模型线两两标注
            var faceRefer = sel.PickObject(ObjectType.Face);//获取顶面参照
            var faceEle = doc.GetElement(faceRefer);//获取顶面元素
            Face face = faceEle.GetGeometryObjectFromReference(faceRefer) as Face;
            var style = (new FilteredElementCollector(doc)).OfClass(typeof(GraphicsStyle)).
                Where(o => o.Name == "细线").First();
            var curveArray = new List<Curve>();
            var view = (new FilteredElementCollector(doc)).OfCategory(BuiltInCategory.OST_Views).
                WhereElementIsNotElementType().Where(o => o.Name == "标高 1").First() as View;
            foreach (var item in face.EdgeLoops.get_Item(0))
            {
                var line = (item as Edge).AsCurve();
                var trans = new Transaction(doc, "修改");
                trans.Start();
                var sketchPlane = SketchPlane.Create(doc, faceRefer);
                var modelCurve = doc.Create.NewModelCurve(line, sketchPlane);//将顶面四条边转换为模型线
                modelCurve.LookupParameter("线样式").Set(style.Id); //修改模型线的线样式为细线
                curveArray.Add(modelCurve.GeometryCurve as Curve);//四条模型线放到一个数组
                trans.Commit();
                
            }
            //TaskDialog.Show("结果是",curveArray.Size.ToString());
            curveArray.Sort((a, b) =>
            {
                return a.ApproximateLength.CompareTo(b.ApproximateLength);
            });
            //TaskDialog.Show("结果是", curveArray.Count.ToString());
            var array1 = new ReferenceArray();
            var array2 = new ReferenceArray();
            array1.Append(curveArray[0].Reference);
            array1.Append(curveArray[1].Reference);
            array2.Append(curveArray[2].Reference);
            array2.Append(curveArray[3].Reference);
            //TaskDialog.Show("结果是", array1.Size.ToString());
            var trans2 = new Transaction(doc, "修改");
            trans2.Start();
            doc.Create.NewDimension(doc.ActiveView, curveArray[2] as Line, array1);
            doc.Create.NewDimension(doc.ActiveView, curveArray[0] as Line, array2);
            trans2.Commit();


            //var trans = new Transaction(doc, "修改");
            //trans.Start();

            //trans.Commit();


            //TaskDialog.Show("结果是", ....);
            return Result.Succeeded;
        }
    }
}
