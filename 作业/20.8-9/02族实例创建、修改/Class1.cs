﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;

namespace _02族实例创建_修改
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        private object uiDoc;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            /*//一、代码创建一面结构墙
            //获取某种墙类型
            var walltype = collector.OfCategory(BuiltInCategory.OST_Walls).
                      OfClass(typeof(WallType)).WhereElementIsElementType().
                      First(o => o.Name == "CW 102-85-100p") as WallType;
            //获取标高
            var level = new FilteredElementCollector(doc).OfClass(typeof(Level)).
                        First(o => o.Name == "标高 1") as Level;
            //创建线
            var start = new XYZ(0, 0, 0);
            var end = new XYZ(10, 10, 0);
            var geomline = Line.CreateBound(start, end);
            //墙的高度
            Double height = 15 / 0.3048;
            Double offset = 0;
            //创建墙
            var trans = new Transaction(doc,"创建墙");
            trans.Start();
            var wall = Wall.Create(doc, geomline, walltype.Id, level.Id,
                      height, offset, false, true);
            trans.Commit(); */

            //一、①改变墙的实例参数--底部偏移为-50
            //获取墙①②③
            /*var wall = collector.OfCategory(BuiltInCategory.OST_Walls).
                      OfClass(typeof(Wall)).WhereElementIsNotElementType().
                      First(o => o.Name == "CW 102-85-100p") as Wall;
            sel.SetElementIds(new List<ElementId>() { wall.Id });*/

            //修改墙的属性:①改变墙的实例参数--底部偏移为-50
            /*var trans = new Transaction(doc, "创建墙");
            trans.Start();
            wall.LookupParameter("底部偏移").Set(5 / 0.3048);
            trans.Commit();

            var walloffsetchanged = wall.LookupParameter("底部偏移").AsDouble() * 0.3048;
            TaskDialog.Show("显示信息", $"墙修改后的底部偏移为{walloffsetchanged}");*/


            //修改墙的属性:②改变墙的实例参数--结构为取消√
            /*var trans = new Transaction(doc, "改变结构墙");
            trans.Start();
            wall.LookupParameter("结构").Set(0);
            trans.Commit();*/


            //修改墙的属性:③改变墙的实例参数--标记为墙1
            /*var trans = new Transaction(doc, "改变标记");
            trans.Start();
            wall.LookupParameter("标记").Set("墙1");
            trans.Commit();*/


            //修改墙的属性:④改变墙的类型名称为墙-150mm
            //获取墙类型
            /*var wall = collector.OfCategory(BuiltInCategory.OST_Walls).WhereElementIsElementType().
                First(o => o.Name == "CW 102-85-100p");
            var trans = new Transaction(doc, "改变类型名");
            trans.Start();
            wall.Name = "墙-150mm";
            trans.Commit();*/


            return Result.Succeeded;
        }
    }
}
