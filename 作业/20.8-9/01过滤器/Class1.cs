﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;

namespace _01过滤器
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection.GetElementIds();

            FilteredElementCollector collector = new FilteredElementCollector(doc);

            //【1】过滤出项目中所有实例墙
            //var ele=collector.OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType();

            //【2】过滤出项目中所有梁类型
            //var ele=collector.OfCategory(BuiltInCategory.OST_StructuralFraming).WhereElementIsElementType().ToList();


            //【3】过滤出项目中名称为“300x600mm”的梁实例
            //var ele=collector.OfCategory(BuiltInCategory.OST_StructuralFraming).WhereElementIsNotElementType().Where(o => o.Name == "300 x 600mm").ToList();

            //【4】过滤出项目中的标高，取第一个
            //var ele=collector.OfCategory(BuiltInCategory.OST_Levels).WhereElementIsNotElementType().FirstElement();
            //TaskDialog.Show("查看结果",ele.Name);

            //【5】过滤出项目中（名称为“300x600”并且族名为“测试梁”）的梁实例
            var ele = collector.OfCategory(BuiltInCategory.OST_StructuralFraming).WhereElementIsNotElementType().
                Where(o => o.Name == "300 x 600mm" && (o as FamilyInstance).Symbol.FamilyName == "测试梁");

            //【】高亮显示
            foreach (var item in ele)
            {
                TaskDialog.Show("查看结果", item.Name);
                //    sel.Add(item.Id);
            }
            //uidoc.Selection.SetElementIds(sel);

            //6.过滤标注类型
            var result = collector.OfClass(typeof(DimensionType)).ToList();

            //TaskDialog.Show("结果是", result.Count.ToString());
            foreach (var item in result)
            {

                sel.Add(item.Id);
                TaskDialog.Show("结果是", item.Name);
            }


            return Result.Succeeded;
        }
    }
}
