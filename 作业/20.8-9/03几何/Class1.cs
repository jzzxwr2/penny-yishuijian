﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;

namespace _03几何
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;
            FilteredElementCollector collector = new FilteredElementCollector(doc);

            var wall = collector.OfCategory(BuiltInCategory.OST_Walls).
                      OfClass(typeof(Wall)).WhereElementIsNotElementType().
                      First(o => o.Name == "常规 - 200mm") as Wall;
            sel.SetElementIds(new List<ElementId>() { wall.Id });
            var options = new Options();
            var geometry = wall.get_Geometry(options);

            //1、获得墙体积
            /*foreach (GeometryObject obj in geometry)
            {
                if (obj is Solid)
                {
                    var solid = obj as Solid;
                    if (solid != null)
                    {
                        var volume = solid.Volume.ToString();
                        TaskDialog.Show("墙的体积是", volume);
                    }
                }
            }*/

            //2、获得墙的所有面，然后取第一个面的面积
            /*foreach (GeometryObject obj in geometry)
            {
                if (obj is Solid)
                {
                    var solid = obj as Solid;
                    if (solid != null)
                    {
                        var area = solid.Faces.get_Item(0).Area.ToString();
                        TaskDialog.Show("墙的第一个面面积是", area);
                    }
                }
            }*/

            //3、获得这些面中faceNormal(面的方向)为（0,0,1）的面
            /*foreach (GeometryObject obj in geometry)
            {
                if (obj is Solid)
                {
                    var solid = obj as Solid;
                    if (solid != null)
                    {
                        var face = solid.Faces;
                        for (int i=0;i<6;i++)
                        {                            
                            var facenormal=(face.get_Item(i) as PlanarFace).FaceNormal;
                            if (facenormal.IsAlmostEqualTo(new XYZ(0, 0, 1)))
                            TaskDialog.Show("结果是",facenormal.ToString());
                        }
                        
                    }
                }
            }*/

            //4、获得墙所有线中最短的线
            foreach (GeometryObject obj in geometry)
            {
                if (obj is Solid)
                {
                    var solid = obj as Solid;
                    if (solid != null)
                    {
                        var edge = solid.Edges;
                        var min = edge.get_Item(0).ApproximateLength;
                        for (int i = 0; i <edge.Size; i++)
                        {
                            var edge_i = edge.get_Item(i).ApproximateLength;
                            
                            if (edge_i < min)
                            {
                                min= edge_i;
                                
                            }
                            
                        }
                        TaskDialog.Show("结果是", min.ToString());

                    }
                }
            }

            //var arr = new List<Solid>();
            //Options opt = new Options();
            //opt.ComputeReferences = true;
            //opt.DetailLevel = ViewDetailLevel.Fine;
            //GeometryElement e = ele.get_Geometry(opt);
            //foreach (GeometryObject obj in e)
            //{
            //    if (obj is Solid)
            //    {
            //        var Object = obj as Solid;
            //        if (Object.SurfaceArea != 0) arr.Add(Object);
            //    }
            //    else if (obj is GeometryInstance)
            //    {
            //        GeometryInstance geoInstance = obj as GeometryInstance;
            //        GeometryElement geoElement = geoInstance.GetInstanceGeometry();
            //        foreach (GeometryObject obj2 in geoElement)
            //        {
            //            if (obj2 is Solid)
            //            {
            //                var Object2 = obj2 as Solid;
            //                if (Object2.SurfaceArea != 0) arr.Add(Object2);
            //            }
            //        }
            //    }
            //}

            return Result.Succeeded;
        }
    }
}
