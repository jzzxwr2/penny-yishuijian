﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;


namespace _06房间
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            var level = (new FilteredElementCollector(doc)).OfCategory(BuiltInCategory.OST_Levels).
                WhereElementIsNotElementType().First(o => o.Name == "标高 1") as Level;//获取标高

            var pointStart = new XYZ(0, 0, 0);
            var pointEnd = new XYZ(0, 10, 0);

            var trans = new Transaction(doc, "创建");
            trans.Start();
            var wall1 = Wall.Create(doc, Line.CreateBound(new XYZ(0, 0, 0), new XYZ(0, 10, 0)),
                level.Id, false);
            var wall2 = Wall.Create(doc, Line.CreateBound(new XYZ(0, 10, 0), new XYZ(10, 10, 0)),
                level.Id, false);
            var wall3 = Wall.Create(doc, Line.CreateBound(new XYZ(10, 10, 0), new XYZ(10, 0, 0)),
                level.Id, false);
            var wall4 = Wall.Create(doc, Line.CreateBound(new XYZ(10, 0, 0), new XYZ(0, 0, 0)),
                level.Id, false);

            var newRoom = doc.Create.NewRoom(level, new UV(5, 5));//创建房间
            var tag = doc.Create.NewRoomTag(new LinkElementId(newRoom.Id),
                new UV(5, 5), (newRoom.Level as Level).FindAssociatedPlanViewId());//创建房间标记

            trans.Commit();

            var c1 = new CurveArray();
            var opts = new SpatialElementBoundaryOptions();
            IList<IList<BoundarySegment>> blist = newRoom.GetBoundarySegments(opts);
            IList<BoundarySegment> bsFlist = blist.First();
            
            foreach (var item in bsFlist)
            {
                c1.Append(item.GetCurve());
            }

            var floorType = (new FilteredElementCollector(doc)).OfCategory(BuiltInCategory.OST_Floors).
                WhereElementIsElementType().First() as FloorType;

            var trans2 = new Transaction(doc, "创建");
            trans2.Start();
            var newFloor = doc.Create.NewFloor(c1,floorType,level,false);
            trans2.Commit();



            return Result.Succeeded;
        }
    }
}
