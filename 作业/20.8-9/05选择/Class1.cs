﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Windows.Forms;

namespace _05选择
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            //1.交互：选择一个面，输出面的方向
            //Reference ele = sel.PickObject(ObjectType.Face,"选面");//选择面类型  获得构件中所有几何中reference和选中的那个面的reference一致的那个面
            //Element face = doc.GetElement(ele);//实体  face变量还是保存着那个面对应的构件
            //Face face2 = face.GetGeometryObjectFromReference(ele) as Face;  //得到那个面
            //var result = (face2 as PlanarFace).FaceNormal;
            //TaskDialog.Show("结果是", result.ToString());


            //2.交互：选中一个柱，修改柱名称，输出柱的族名称（注：选中物的数据类型；对应revit修改名称操作——类型名称）
            //Reference refer = sel.PickObject(ObjectType.Element, "选择");
            //Element ele = doc.GetElement(refer.ElementId);
            //var fl = (ele as FamilyInstance).Symbol;

            //Transaction trans = new Transaction(doc, "修改柱名称");
            //trans.Start();
            //fl.Name = "new" + fl.Name;
            //trans.Commit();
            //TaskDialog.Show("结果是", fl.Name);
            //TaskDialog.Show("族名称", fl.Family.Name);


            //3.交互：框择多个柱构件，用循环输出构件名称，并修改柱类型（点击插件，让你选了构件，点完成才出结果）

            //var results = sel.PickObjects(ObjectType.Element, "选择");
            //TaskDialog.Show("结果是", results.Count.ToString());

            //3.1 循环输出构件名称（3.1、3.2可以合并）
            //foreach (var item in results)
            //{
            //    Element ele = doc.GetElement(item.ElementId);
            //    FamilyInstance fl = ele as FamilyInstance;
            //    TaskDialog.Show("结果是", fl.Name);
            //}

            //3.2 修改柱类型
            //var symbol = (doc.GetElement(sel.PickObject(ObjectType.Element, "选择").ElementId) as FamilyInstance).Symbol;
            //Transaction trans = new Transaction(doc, "修改柱类型");
            //trans.Start();
            //foreach (var item in results)
            //{
            //    Element ele = doc.GetElement(item.ElementId);
            //    FamilyInstance fl = ele as FamilyInstance;
            //    fl.Symbol = symbol;
            //    TaskDialog.Show("结果是", fl.Name);
            //}
            //trans.Commit();


            //4.交互功能效果：手动选中几个构件，点击插件后，不用再操作即可用循环输出构件名称（插件识别你当前选了多少个构件，然后出结果）
            var results = sel.GetElementIds();
            foreach (var item in results)
            {
                //MessageBox.Show(doc.GetElement(item).Name);
                TaskDialog.Show("结果是",doc.GetElement(item).Name);
            }


            return Result.Succeeded;
        }
    }
}
