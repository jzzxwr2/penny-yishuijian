﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;


namespace _08视图_几何
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            //1.新建一个三维视图
            //var viewType = (new FilteredElementCollector(doc)).OfClass(typeof(ViewFamilyType)).
            //    WhereElementIsElementType().Where(o => o.Name == "三维视图").First();
            //var trans = new Transaction(doc, "修改");
            //trans.Start();
            //var newView = View3D.CreateIsometric(doc, viewType.Id);
            //trans.Commit();

            //2.激活打开这个视图
            //uidoc.ActiveView= (new FilteredElementCollector(doc)).OfCategory(BuiltInCategory.OST_Views).
            //WhereElementIsNotElementType().Where(o => o.Name == "三维视图 1").First() as View;
            //uidoc.ActiveView = newView;

            //3.设置这个视图精细度为：中等
            //newView.LookupParameter("详细程度").Set(2);

            //4.设置模式为着色模式
            //var trans2 = new Transaction(doc, "修改");
            //trans2.Start();
            //newView.get_Parameter(BuiltInParameter.MODEL_GRAPHICS_STYLE).Set((int)DisplayStyle.FlatColors);
            //trans2.Commit();

            //5.墙、板类别可见性为隐藏
            //var wallType = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Walls).First().Category;
            //var floorType= new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Floors).First().Category;

            //var trans3 = new Transaction(doc, "修改");
            //trans3.Start();
            //doc.ActiveView.SetCategoryHidden(wallType.Id, true);
            //doc.ActiveView.SetCategoryHidden(floorType.Id, true);
            //trans3.Commit();

            //6.判断该视图是否应用视图样板
            //var result = doc.ActiveView.LookupParameter("视图样板").AsElementId();
            //TaskDialog.Show("结果是", result.ToString());

            //7.打开视图的剖面框
            //var trans4 = new Transaction(doc, "修改");
            //trans4.Start();
            //doc.ActiveView.LookupParameter("剖面框").Set(1);
            //trans4.Commit();

            //8.几何 手动画三面墙，用代码获得三面墙：
            //8.1获得墙的定位线，以及计算线的中点，放置结构柱
            //var wall1 = doc.GetElement(new ElementId(336387));
            //var line1 = (wall1.Location as LocationCurve).Curve as Line;
            //var startPoint = line1.Origin;
            //var endPoint = line1.GetEndPoint(1);

            //var middlePoint = new XYZ((startPoint.X + endPoint.X) / 2, (startPoint.Y + endPoint.Y) / 2, (startPoint.Z + endPoint.Z) / 2);
            //var familySymbol = doc.GetElement(new ElementId(12190)) as FamilySymbol;
            //var level = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Levels).
            //    WhereElementIsNotElementType().Where(o => o.Name == "标高 1").First() as Level;

            //var trans5 = new Transaction(doc, "修改");
            //trans5.Start();
            //doc.Create.NewFamilyInstance(middlePoint, familySymbol, level, Autodesk.Revit.DB.Structure.StructuralType.Column);
            //trans5.Commit();


            //8.2获得墙的某一端点，沿着墙方向使墙延伸1000mm
            //var wall2 = doc.GetElement(new ElementId(336453));
            //var line2 = (wall2.Location as LocationCurve).Curve as Line;
            //var endPoint = line2.GetEndPoint(1);
            //var newEndPoint = new XYZ(endPoint.X + 1000 /304.8 * line2.Direction.X, endPoint.Y + 1000 /304.8 * line2.Direction.Y, endPoint.Z);

            //var trans = new Transaction(doc, "修改");
            //var newLine = Line.CreateBound(line2.Origin, newEndPoint);
            //trans.Start();
            //(wall2.Location as LocationCurve).Curve = newLine;
            //trans.Commit();


            //8.3获得墙的一个实体，并根据实体获得体积（转换为公尺）
            var wall3 = doc.GetElement(new ElementId(336510));
            var options = new Options();
            var geometry = wall3.get_Geometry(options);
            foreach (var item in geometry)
            {
                if (item is Solid)
                {
                    var solid = item as Solid;
                    if (solid != null)
                    {
                        var volume = (solid.Volume * 304.8 * 304.8 * 304.8).ToString();
                        TaskDialog.Show("结果是",volume);
                    }
                }
            }


            return Result.Succeeded;
        }
    }
}
