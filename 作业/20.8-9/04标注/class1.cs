﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace _04标注
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        private ElementId ownerDBViewId;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection.GetElementIds();

            //1.尺寸标注
            //选择两个构件
            /*Element ele1 = doc.GetElement(sel.PickObject(ObjectType.Element));
            Element ele2 = doc.GetElement(sel.PickObject(ObjectType.Element));

            //获得每个墙的四条边
            var arr1 = GetEdge(ele1, ele1.LookupParameter("长度").AsDouble());
            var arr2 = GetEdge(ele2, ele2.LookupParameter("长度").AsDouble());

            var max = double.MaxValue;
            ReferenceArray refArry = new ReferenceArray();
            var po1 = new XYZ();
            var po2 = new XYZ();

            foreach (var i in arr1)
            {
                var line = i.AsCurve();
                if (Math.Abs(line.GetEndPoint(0).Z) < 1e-6)//过滤两条边
                {
                    foreach (var j in arr2)
                    {
                        var line2 = j.AsCurve();
                        if (Math.Abs(line2.GetEndPoint(0).Z) < 1e-6)//过滤两条边
                        {
                            var nowpo1 = line.Project(line2.GetEndPoint(0)).XYZPoint;
                            var po2_result = line2.Project(nowpo1);
                            if (po2_result.Distance < max)
                            {
                                max = po2_result.Distance;
                                po1 = nowpo1;
                                po2 = po2_result.XYZPoint;
                                refArry.Clear();
                                refArry.Append(i.Reference);
                                refArry.Append(j.Reference);
                            }
                        }
                    }
                }
            }

            var newLine = Line.CreateBound(po1, po2);

            var tr = new Transaction(doc, "创建标注");
            tr.Start();

            doc.Create.NewDimension(doc.ActiveView, newLine, refArry);

            tr.Commit();*/


            //2.文字注释
            /*FilteredElementCollector collector = new FilteredElementCollector(doc);

            XYZ textloc = sel.PickPoint("选择一个点");
            ElementId defaultTextTypeId = doc.GetDefaultElementTypeId(ElementTypeGroup.TextNoteType);
            TextNoteOptions opts = new TextNoteOptions(defaultTextTypeId);
            opts.HorizontalAlignment = HorizontalTextAlignment.Left;
            opts.Rotation = 0;

            Transaction trans = new Transaction(doc, "textnote creation");
            trans.Start();
            TextNote textNote = TextNote.Create(doc, doc.ActiveView.Id, textloc, "排风井", opts);
            trans.Commit();*/


            //3.标记门
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            //var ele = collector.OfCategory(BuiltInCategory.OST_DoorTags).WhereElementIsElementType().FirstElement();
            //TaskDialog.Show("结果是",ele.Name);

            //ElementId symId = ele.Id;
            //ElementId ownerDBViewId = doc.ActiveView.Id;
            //Reference referenceToTag = sel.PickObject(ObjectType.Element);

            //Transaction trans = new Transaction(doc, "creation");
            //trans.Start();
            //IndependentTag tag = IndependentTag.Create(doc, symId, ownerDBViewId, referenceToTag, true, TagOrientation.Vertical, new XYZ(0, 0, 0));
            //trans.Commit();
                               

            return Result.Succeeded;
        }
        public List<Edge> GetEdge(Element ele, double leng)
        {
            var arr = new List<Edge>();
            Options opt = new Options();
            opt.ComputeReferences = true;
            opt.DetailLevel = ViewDetailLevel.Fine;
            GeometryElement e = ele.get_Geometry(opt);
            foreach (GeometryObject obj in e)
            {
                if (obj is Solid)
                {
                    var Object = obj as Solid;
                    if (Object.SurfaceArea != 0)
                    {
                        foreach (Edge item in Object.Edges)
                        {
                            if (Math.Abs(item.ApproximateLength - leng) < 1e-6)
                            {
                                arr.Add(item);
                            }
                        }
                    };
                }
                else if (obj is GeometryInstance)
                {
                    GeometryInstance geoInstance = obj as GeometryInstance;
                    GeometryElement geoElement = geoInstance.GetInstanceGeometry();
                    foreach (GeometryObject obj2 in geoElement)
                    {
                        if (obj2 is Solid)
                        {
                            var Object2 = obj2 as Solid;
                            if (Object2.SurfaceArea != 0)
                            {
                                foreach (Edge item in Object2.Edges)
                                {
                                    if (Math.Abs(item.ApproximateLength - leng) < 1e-6)
                                    {
                                        arr.Add(item);
                                    }
                                }
                            };
                        }
                    }
                }
            }
            return arr;
        }
    }
    
}
