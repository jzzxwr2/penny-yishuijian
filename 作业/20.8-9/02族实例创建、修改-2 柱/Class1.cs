﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;

namespace _02族实例创建_修改_2_柱
{
    [Transaction(TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uidoc = commandData.Application.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            FilteredElementCollector collector2 = new FilteredElementCollector(doc);
            //获取族类型
            /*var colType = collector.OfCategory(BuiltInCategory.OST_Columns).
                          WhereElementIsElementType().First(o=>o.Name== "475 x 610mm") as FamilySymbol;
            //获取标高
            var level = new FilteredElementCollector(doc).OfClass(typeof(Level)).
                        First(o => o.Name == "标高 1") as Level;
                        
            
            //创建柱
            var trans = new Transaction(doc, "创建柱");
            trans.Start();
            var column = doc.Create.NewFamilyInstance(new XYZ(0, 0, 0), colType, level, 
                         Autodesk.Revit.DB.Structure.StructuralType.Column);
            trans.Commit(); */


            // 获取柱
            /*var col = collector.OfCategory(BuiltInCategory.OST_Columns).
                      OfClass(typeof(FamilyInstance)).WhereElementIsNotElementType().
                      First(o => o.Name == "475 x 610mm") as FamilyInstance;
            sel.SetElementIds(new List<ElementId>() { col.Id });*/

            //①读取柱的实例参数--体积
            /*var colVol = col.LookupParameter("体积").AsDouble();
            TaskDialog.Show("显示信息", $"柱的体积为{colVol}"); */

            //②读取柱的类型参数--尺寸（长或宽度都可以）
            /*var ele=collector.OfCategory(BuiltInCategory.OST_Columns).WhereElementIsElementType().
                    First(o => o.Name == "475 x 610mm");
            
            var eleWi = ele.LookupParameter("宽度").AsDouble();
            
            TaskDialog.Show("显示信息", $"柱的类型参数宽度为{eleWi}");*/


            //附加题：代码新建一个柱类型，类型名字为柱1
            /*var colType = collector.OfCategory(BuiltInCategory.OST_Columns).
                          WhereElementIsElementType().First(o => o.Name == "475 x 610mm") as FamilySymbol;
            var trans = new Transaction(doc, "创建新的柱类型");
            trans.Start();
            colType.Duplicate("柱1");
            trans.Commit();*/

            //并将上面用代码创建的实例柱的类型切换为柱1
            var column=collector.OfCategory(BuiltInCategory.OST_Columns).WhereElementIsNotElementType().First(o => o.Name == "475 x 610mm") as FamilyInstance;
            var colType=collector2.OfCategory(BuiltInCategory.OST_Columns).WhereElementIsElementType().First(o => o.Name == "柱1") as FamilySymbol; 
            var trans = new Transaction(doc, "修改柱类型");
            trans.Start();
            column.Symbol=colType;
            trans.Commit();

            
            return Result.Succeeded;
        }
    }
}
