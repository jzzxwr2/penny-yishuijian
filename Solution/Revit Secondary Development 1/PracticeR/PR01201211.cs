﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Autodesk.Revit;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.PracticeR
{
    //class PR01
    //{
    //    static void main(string[] args)
    //    {
    //        var type = typeof(Element);
    //        var methods = type.GetMethods(System.Reflection.BindingFlags.Public 
    //            | System.Reflection.BindingFlags.Static);
    //        var groups = methods.GroupBy(m => m.Name);
    //        Console.WriteLine(groups.Count().ToString());
    //        foreach (var item in groups)
    //        {
    //            Console.WriteLine(item.First().Name);
    //        }
    //    }
    //}
    [Transaction(TransactionMode.Manual)]
    class PR01 : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {

            var type = typeof(Element);
            var members = type.GetMembers();


            var stringlist = "";// new List<string>();

            foreach (var item in members)
            {
               stringlist= stringlist + "\n" + item.Name;
            }
            //Console.WriteLine();
            MessageBox.Show(stringlist);

            //var logfile = File.CreateText(@" C:\Users\PC\Desktop\aaa.txt");
            var logfile = File.CreateText(@" C:\aaa.txt");
            logfile.WriteLine(stringlist);
            logfile.Close();
            return Result.Succeeded;
        }
    }
}
