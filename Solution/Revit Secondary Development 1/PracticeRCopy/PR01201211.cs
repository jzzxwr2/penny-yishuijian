﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit;
using Autodesk.Revit.DB;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using System.Transactions;
using System.Windows;
using System.IO;

namespace Revit_Secondary_Development.PracticeR2
{
    [Transaction(TransactionMode.Manual)]
    class PR01Reflection : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var type = typeof(Element);
            var methods = type.GetMembers();

            var stringlist = "";

            foreach (var item in methods)
            {
                stringlist = stringlist + "\n" + item.Name;
            }
            MessageBox.Show(stringlist);
            var logfile = File.CreateText(@" C:\Repositories\AAA.txt");
            logfile.WriteLine(stringlist);
            logfile.Close();
            return Result.Succeeded;
        }
    }
}
