﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Charpter06.U02
{
    class U0200
    {
        static void main(string[] args)
        {
            StreamReader sr = null;
            try
            {
                sr = File.OpenText("d:\\1\\1.txt");
                string s;
                while (sr.Peek() != -1)
                {
                    s = sr.ReadLine();
                    Console.WriteLine(s);
                }
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine("文件夹未被发现");
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("文件未被发现");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
    }
}
