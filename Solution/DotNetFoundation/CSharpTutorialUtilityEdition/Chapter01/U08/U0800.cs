﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U08
{
    class U0800
    {
        static void main(string[] args)
        {
            int[] array = new int[3];
            for (int i = 0; i < array.Length; i++)
            {
                /*array[i] = Convert.ToInt32(Console.ReadLine());*/
                array[i] = i * i;
            }


            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }
    }
}
