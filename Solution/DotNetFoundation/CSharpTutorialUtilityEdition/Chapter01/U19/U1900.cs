﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U19
{
    class U1900
    {
        static void main(string[] args)
        {
            string s = "askdflwe";
            char[] array = s.ToCharArray();
            cU1900 c=new cU1900();
            char[] uparray = c.ToUpper(array);
            char[] uparray2 = cU1900.stToUpper(array);
            foreach (var c1 in uparray)
            {
                Console.WriteLine(c1);
            }

            foreach (var c1 in uparray2)
            {
                Console.WriteLine(c1);
            }

            //Console.WriteLine(Char.ToUpper('a'));
        }
    }

    class cU1900
    {
        /*public char[] ToUpper(char[] arr)
        {
            char[] result = null;
            foreach (char c in arr)
            {
                result.Append(Char.ToUpper(c));
            }

            return result;
        }*/
        /*private char ch;
        public char ToUpper(char[] arr)
        {
            
            foreach (char c in arr)
            {
                ch+=Char.ToUpper(c);
            }
            return ch;
        }*/
        public char[] ToUpper(char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = Char.ToUpper(arr[i]);
            }
            return arr;
        }

        public static char[] stToUpper(char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = Char.ToUpper(arr[i]);
            }

            return arr;
        }
    }
}
