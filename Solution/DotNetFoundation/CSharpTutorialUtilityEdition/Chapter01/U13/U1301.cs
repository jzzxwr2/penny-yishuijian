﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U13
{
    class U1301
    {
        static void main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (i==0||i==4)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        if (j==0||j==7)
                        {
                            Console.Write("*");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                }
                Console.Write("\n");
            }
        }
    }
}
