﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U13
{
    class U1300
    {
        static void main(string[] args)
        {
            /*for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine("*");
                }
            }*/
            /*for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    string s = string.Empty;
                    s += "*";
                    Console.WriteLine(s);
                }
            }*/
            /*for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("*");
                }
            }*/
            /*for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    string s = string.Empty;
                    s += "*";
                    Console.Write(s);
                }
            }*/
            /*for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (i==0||i==5)
                    {
                        Console.WriteLine("*");
                    }
                    else
                    {
                        if (j==0||j==4)
                        {
                            Console.WriteLine("*");
                        }
                        else
                        {
                            Console.WriteLine(" ");
                        }
                    }
                }

                Console.WriteLine("\r\n");
            }*/
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (i == 0 || i == 5)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        if (j == 0 || j == 4)
                        {
                            Console.Write("*");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                }

                Console.Write("\n");
            }

        }
    }
}
