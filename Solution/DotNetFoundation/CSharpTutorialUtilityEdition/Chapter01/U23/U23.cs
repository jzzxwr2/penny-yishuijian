﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U23
{
    class U23
    {
        static void main(string[] args)
        {
            /*RecU23 rec = new RecU23(1, 1, 3, 5);
            Console.WriteLine(Area(rec));
            CirU23 c = new CirU23(1, 1, 6);
            Console.WriteLine(Area(c));*/

            RecU23 rec = new RecU23(2, 3);
            CirU23 c = new CirU23(10);
            rec.Area();
            c.Area();
        }

        /*static double Area(RecU23 rec)
        {
            double result = rec.Length * rec.Width;
            return result;
        }

        static double Area(CirU23 c)
        {
            double result = Math.PI * c.R * c.R;
            return result;
        }*/
    }



    /*class RecU23:PointU23
    {
        public double Length { get; set; }
        public double Width { get; set; }

        public RecU23(int x, int y, double length, double width) : base(x, y)
        {
            Length = length;
            Width = width;
        }
    }

    class CirU23 : PointU23
    {
        public double R { get; set; }

        public CirU23(int x, int y, double r) : base(x, y)
        {
            R = r;
        }
    }
    class PointU23
    {
        public int X { get; set; }
        public int Y { get; set; }

        public PointU23(int x, int y)
        {
            X = x;
            Y = y;
        }
    }*/
    class RecU23 : PointU23
    {
        public RecU23(int a, int b) : base(a, b)
        {
        }

        public override void Area()
        {
            Console.WriteLine(x*y);
        }
    }

    class CirU23 : PointU23
    {
        /*public CirU23(int a, int b) : base(a, b)
        {
        }*/
        public CirU23(int a) : base(a, a)
        {
        }

        public override void Area()
        {
            Console.WriteLine(3.14*x*y);
        }
    }
    abstract class PointU23
    {
        protected int x, y;
        /*protected PointU23(int x, int y)
        {
            this.x = x;
            this.y = y;
        }*/
        public PointU23(int a, int b)
        {
            x = a;
            y = b;
        }
        public abstract void Area();
    }
}
