﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U23.copy
{
    class U2301
    {
        static void main(string[] args)
        {
            RecU2301 rec = new RecU2301(1, 1, 3, 5);
            CirU2301 c = new CirU2301(1, 3);
            rec.Area();
            c.Area();
        }
    }

    class RecU2301 : PointU23
    {
        public double  Length { get; set; }
        public double  Width { get; set; }

        public RecU2301(int a, int b, double length, double width) : base(a, b)
        {
            Length = length;
            Width = width;
        }

        public override void Area()
        {
            Console.WriteLine(this.Length*this.Width);
        }
    }

    class CirU2301 : PointU23
    {
        public double R { get; set; }

        public CirU2301(int a, double r) : base(a, a)
        {
            R = r;
        }

        public override void Area()
        {
            Console.WriteLine(3.14*R*R);
        }
    }
    abstract class PointU2301
    {
        /*public int X { get; set; }
        public int Y { get; set; }*/
        protected int X { get; set; }
        protected int Y { get; set; }

        protected PointU2301(int x, int y)
        {
            X = x;
            Y = y;
        }

        public abstract void Area();

    }
}
