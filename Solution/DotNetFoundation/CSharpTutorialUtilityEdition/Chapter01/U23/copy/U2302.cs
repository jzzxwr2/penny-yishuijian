﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U23.copy
{
    class U2302
    {
        static void main(string[] args)
        {
            RecU2302 rec = new RecU2302(1, 1, 3, 6);
            CirU2302 c = new CirU2302(1, 3);
            rec.Area();
            c.Area();
        }
    }

    class RecU2302 : PointU2302
    {
        public double Length { get; set; }
        public double Width { get; set; }

        public RecU2302(double x, double y, double length, double width) : base(x, y)
        {
            Length = length;
            Width = width;
        }

        public override void Area()
        {
            Console.WriteLine(Length*Width);
        }
    }

    class CirU2302 : PointU2302
    {
        public double R { get; set; }

        public CirU2302(double x,double r) : base(x, x)
        {
            R = r;
        }

        public override void Area()
        {
            Console.WriteLine(3.14*R*R);
        }
    }
    abstract class PointU2302
    {
        public double X { get; set; }
        public double Y { get; set; }

        protected PointU2302(double x, double y)
        {
            X = x;
            Y = y;
        }

        public abstract void Area();
    }
}
