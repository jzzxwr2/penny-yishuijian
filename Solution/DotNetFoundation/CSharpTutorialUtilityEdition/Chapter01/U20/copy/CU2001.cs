﻿using System;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20.copy
{
    class CU2001
    {
        static void main(string[] args)
        {
            FractionU2001 f1 = new FractionU2001();
            f1.Numerator = 1;
            f1.Denominator = 2;

            FractionU2001 f2 = new FractionU2001();
            f2.Numerator = 3;
            f2.Denominator = 9;
            f2.Reduction();

            FractionU2001[] fs = new FractionU2001[5];
            fs[0] = f1.Add(f2);
            fs[1] = f1.Sub(f2);
            fs[2] = f1.Mult(f2);
            fs[3] = f1.Division(f2);
            fs[4] = f1.Ramind(f2);
            foreach (var f in fs)
            {
                f.Print();
            }
        }
    }

    class FractionU2001
    {
        private int _numerator;
        private int _denominator;

        public int Numerator
        {
            set { _numerator = value; }
            get { return _numerator; }
        }

        public int Denominator
        {
            set
            {
                if (value != 0)
                    _denominator = value;
            }
            get { return _denominator; }
        }

        public void Print()
        {
            Console.WriteLine(_numerator + "/" + _denominator);
        }

        private int GCD(int a, int b)
        {
            int temp = 0;
            while (b!=0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }
        public void Reduction()
        {
            int gcd = GCD(this.Numerator, this.Denominator);
            this.Numerator /= gcd;
            this.Denominator /= gcd;
        }

        public FractionU2001 Exchange()
        {
            FractionU2001 newFra = new FractionU2001();
            newFra._denominator = this._numerator;
            newFra._numerator = this._denominator;
            return newFra;
        }

        public FractionU2001 Add(FractionU2001 fra)
        {
            FractionU2001 result = new FractionU2001();
            result.Numerator = this._numerator * fra.Denominator + fra.Numerator * this._denominator;
            result.Denominator = this.Denominator * fra.Denominator;
            result.Reduction();
            return result;
        }

        public FractionU2001 Sub(FractionU2001 fra)
        {
            FractionU2001 result = new FractionU2001();
            result.Numerator = this._numerator * fra.Denominator - fra.Numerator * this._denominator;
            result.Denominator = this.Denominator * fra.Denominator;
            result.Reduction();
            return result;
        }

        public FractionU2001 Mult(FractionU2001 fra)
        {
            FractionU2001 result = new FractionU2001();
            result.Numerator = this._numerator * fra.Numerator;
            result.Denominator = this.Denominator * fra.Denominator;
            result.Reduction();
            return result;
        }

        public FractionU2001 Division(FractionU2001 fra)
        {
            FractionU2001 result = new FractionU2001();
            FractionU2001 newfra = fra.Exchange();

            result.Numerator = this._numerator * newfra._numerator;
            result.Denominator = this._denominator * newfra._denominator;

            result.Reduction();
            return result;
        }

        public FractionU2001 Ramind(FractionU2001 fra)
        {
            FractionU2001 result = new FractionU2001();
            result._numerator = (this._numerator * fra._denominator) % (fra._numerator * this._denominator);
            result._denominator = this._denominator * fra._denominator;
            return result;
        }
    }

}
