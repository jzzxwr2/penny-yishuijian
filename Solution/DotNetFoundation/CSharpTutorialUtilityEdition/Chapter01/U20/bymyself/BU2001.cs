﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20.bymyself
{
    class BU2001
    {
        static void main(string[] args)
        {
            FractionBU2001 f1 = new FractionBU2001();
            f1.Num = 6;
            f1.Den = 10;
            Console.WriteLine();
        }
    }

    class FractionBU2001
    {
        private int _num;
        private int _den;

        public int Num
        {
            set { _num = value; }
            get { return _num; }
        }

        public int Den
        {
            set
            {
                if (value != 0)
                    _den = value;
            }
            get { return _den; }
        }

        public void Print()
        {
            Console.WriteLine(_num + "/" + _den);
        }
        private int GCD(int a, int b)
        {
            int temp = 0;
            while (b!=0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }

        public FractionBU2001 Reduction(FractionBU2001 fra)
        {
            FractionBU2001 result = new FractionBU2001();
            int gcd=GCD(this._num, this._den);
            result.Num=this._num /= gcd;
            result.Den=this._den /= gcd;
            return result;
        }
        //public 
    }
}
