﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20.bymyself
{
    class BU2002
    {
        static void main(string[] args)
        {
            FractionBU2002 f1 = new FractionBU2002();
            f1.Num = 10;
            f1.Den = 12;
            f1.Reduction();
            f1.Print();

            FractionBU2002 f2 = new FractionBU2002();
            f2.Num = 3;
            f2.Den = 8;

            FractionBU2002 add = f1.Add(f2);
            add.Print();
            FractionBU2002 sub = f1.Sub(f2);
            FractionBU2002 sub2 = f2.Sub(f1);
            sub.Print();
            sub2.Print();
        }
    }

    public class FractionBU2002
    {
        private int _num;
        private int _den;

        public int Num
        {
            set { _num = value; }
            get { return _num; }
        }

        public int Den
        {
            set
            {
                if (value != 0)
                    _den = value;
            }
            get { return _den; }
        }

        public void Print()
        {
            Console.WriteLine(Num+"/"+Den);
        }

        private int GCD(int a, int b)
        {
            int temp = 0;
            while (b!=0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }

        public void Reduction()
        {
            int gcd = GCD(this.Num, this.Den);
            this.Num /= gcd;
            this.Den /= gcd;
        }

        public FractionBU2002 Add(FractionBU2002 f)
        {
            FractionBU2002 result = new FractionBU2002();
            result.Num = this.Num * f.Den+f.Num*this.Den;
            result.Den = this.Den * f.Den;
            result.Reduction();
            return result;
        }

        public FractionBU2002 Sub(FractionBU2002 f)
        {
            FractionBU2002 result = new FractionBU2002();
            result.Num = this.Num * f.Den - f.Num * this.Den;
            result.Den = this.Den * f.Den;
            result.Reduction();
            return result;
        }
    }
}
