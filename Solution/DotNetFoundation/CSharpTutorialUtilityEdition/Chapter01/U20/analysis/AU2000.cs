﻿using System;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20.analysis
{
    class AU2000
    {
        //求最大公约数
        static void main(string[] args)
        {
            //Console.WriteLine(GCD2(4, 20));
            //Console.WriteLine(GCD(4, 20));
            /*Console.WriteLine(GCD2(4, 12));
            Console.WriteLine(GCD2(12, 4));
            Console.WriteLine(GCD2(3, 15));
            Console.WriteLine(GCD2(10, 12));
            Console.WriteLine(GCD2(12, 10));*/
            Console.WriteLine(GCD2(6,10));
        }

        /*static int GCD(int n1, int n2)
        {
            int max = n1 > n2 ? n1 : n2;
            int min = n1 < n2 ? n1 : n2;
            while (min!=0)
            {
                int temp = max%min;
                
            }

            return max;
        }*/
        static int GCD2(int a, int b)
        {
            int temp = 0;
            while (b != 0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }
    }
}
