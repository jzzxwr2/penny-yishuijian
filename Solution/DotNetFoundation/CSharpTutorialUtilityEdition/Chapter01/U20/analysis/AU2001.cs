﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20.analysis
{
    //约分
    class AU2001
    {
        static void main(string[] args)
        {
            /*int num = 10, den = 12;
            int gcd = GCD(num,den);
            Console.WriteLine(num /= gcd);
            Console.WriteLine(den /= gcd);*/

            cAU2001 f = new cAU2001();
            f.Num = 6;
            f.Den = 10;
            //Console.WriteLine(f.Reduction());
            //f.Reduction();
            f.Print();
            //Console.WriteLine("{0},{1}",f.Num,f.Den);
            //Console.WriteLine(f.GCD(f.Num,f.Den));
            //Console.WriteLine(f.GCD(6, 10));
        }

        static int GCD(int a, int b)
        {
            int temp = 0;
            while (b!=0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }
        
    }

    class cAU2001
    {
        private int _num;
        private int _den;
        public int Num { get; set; }

        public int Den
        {
            set
            {
                if (value != 0)
                    _den = value;
            }
            get { return _den; }
        }

        public void Print()
        {
            //Console.WriteLine(_num + "/" + _den);
            Console.WriteLine(Num+"/"+Den);
        }

        public int GCD(int a, int b)
        {
            int temp = 0;
            while (b!=0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }

        /*public cAU2001 Reduction()
        {
            /*cAU2001 result=new cAU2001();
            int gcd = GCD(result._num, result._den);
            result._num /= gcd;
            result._den /= gcd;
            return result;#1#

            /*cAU2001 result = new cAU2001();
            int gcd = GCD(result.Num, result.Den);
            result.Num /= gcd;
            result.Den /= gcd;
            return result;#1#
        }*/

        public void Reduction()
        {
            int gcd = GCD(this.Num, this.Den);
            this.Num /= gcd;
            this.Den /= gcd;
        }
    }
}
