﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20.analysis
{
    class AU2002
    {
        //字段、属性
        static void main(string[] args)
        {
            cAU2002 c = new cAU2002();
            c.Num = 5;
            Console.WriteLine(c.Num);
            c.Print();
        }
    }

    class cAU2002
    {
        private int _num;

        public int Num
        {
            set { _num = value; }
            get { return _num; }
        }

        public void Print()
        {
            Console.WriteLine(_num);
        }

    }
}
