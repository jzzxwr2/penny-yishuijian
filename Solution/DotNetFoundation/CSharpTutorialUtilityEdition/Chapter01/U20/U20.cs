﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U20
{
    class U20
    {
        static void main(string[] args)
        {
            FractionU2000 fra = new FractionU2000();
            fra.Numerator = 32;
            fra.Denominator = 64;
            fra.Reduction();

            FractionU2000 fra2 = new FractionU2000();
            fra2.Numerator = 12;
            fra2.Denominator = 31;

            //FractionU2000 sum = fra.Division(fra2);
            //sum.Print();

            /*FractionU2000 fra3 = new FractionU2000();
            fra3=fra.Add(fra2);
            fra3.Print();*/

            FractionU2000[] frs = new FractionU2000[5];
            frs[0] = fra.Add(fra2);
            frs[1] = fra.Sub(fra2);
            frs[2] = fra.Mult(fra2);
            frs[3] = fra.Division(fra2);
            frs[4] = fra.Ramind(fra2);
            foreach (FractionU2000 f in frs)
            {
                f.Print();
            }
        }
    }

    class FractionU2000
    {
        private int _numerator;
        private int _denominator;

        public int Numerator
        {
            set { _numerator = value; }
            get { return _numerator; }
        }

        public int Denominator
        {
            set
            {
                if (value != 0)
                    _denominator = value;
            }
            get { return _denominator; }
        }

        public void Print()
        {
            /*Console.WriteLine(Numerator + "/" + Denominator);*/

            Console.WriteLine(_numerator + "/" + _denominator);
        }
        public void Reduction()
        {
            int gcd = GCD(this.Numerator, this.Denominator);
            this.Numerator /= gcd;
            this.Denominator /= gcd;
        }

        private int GCD(int a, int b)
        {
            int temp = 0;
            while (b!=0)
            {
                temp = a % b;
                a = b;
                b = temp;
            }

            return a;
        }

        public FractionU2000 Exchange()
        {
            FractionU2000 newFra = new FractionU2000();
            /*newFra.Denominator = this.Numerator;
            newFra.Numerator = this.Denominator;*/

            newFra._denominator = this._numerator;
            newFra._numerator = this._denominator;

            return newFra;
        }
        public FractionU2000 Add(FractionU2000 fra)
        {
            FractionU2000 result = new FractionU2000();
            /*sum.Numerator = this.Numerator * fra.Denominator + fra.Numerator * this.Denominator;
            sum.Denominator = this.Denominator * fra.Denominator;*/

            result.Numerator = this._numerator * fra.Denominator + fra.Numerator * this._denominator;
            result.Denominator = this.Denominator* fra.Denominator;

            result.Reduction();
            return result;
        }

        public FractionU2000 Sub(FractionU2000 fra)
        {
            FractionU2000 result = new FractionU2000();
            result.Numerator = this._numerator * fra.Denominator - fra.Numerator * this._denominator;
            result.Denominator = this.Denominator * fra.Denominator;
            result.Reduction();
            return result;
        }

        public FractionU2000 Mult(FractionU2000 fra)
        {
            FractionU2000 result = new FractionU2000();
            result.Numerator = this._numerator * fra.Numerator;
            result.Denominator = this.Denominator * fra.Denominator;
            result.Reduction();
            return result;
        }


        public FractionU2000 Division(FractionU2000 fra)
        {
            FractionU2000 result = new FractionU2000();
            FractionU2000 newfra = fra.Exchange();

            /*sum.Numerator = this.Numerator * newfra.Numerator;*/

            result.Numerator = this._numerator * newfra._numerator;
            result.Denominator = this._denominator * newfra._denominator;

            result.Reduction();
            return result;
        }

        public FractionU2000 Ramind(FractionU2000 fra)
        {
            FractionU2000 result = new FractionU2000();
            result._numerator = (this._numerator * fra._denominator) % (fra._numerator * this._denominator);
            result._denominator = this._denominator * fra._denominator;
            return result;
        }
    }
}
