﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U17
{
    class U1700
    {
        static void main(string[] args)
        {
            RectangleU1700 rec1 = new RectangleU1700();
            RectangleU1700 rec2 = new RectangleU1700(1,1,5, 6);
            Console.WriteLine("{0},{1},{2},{3}", rec1.X, rec1.Y,rec1.Length, rec1.Width);
            Console.WriteLine("{0},{1},{2},{3}", rec2.X, rec2.Y, rec2.Length, rec2.Width);
        }
    }

    class RectangleU1700:PointU1700
    {
        public double Length { get; set; }
        public double Width { get; set; }

        public RectangleU1700()
        {
            Length = 2;
            Width = 2;
        }

        public RectangleU1700(double x, double y, double length, double width) : base(x, y)
        {
            Length = length;
            Width = width;
        }
    }
    class PointU1700
    {
        public double X { get; set; }
        public double Y { get; set; }

        public PointU1700()
        {
            X = 0;
            Y = 0;
        }

        public PointU1700(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
