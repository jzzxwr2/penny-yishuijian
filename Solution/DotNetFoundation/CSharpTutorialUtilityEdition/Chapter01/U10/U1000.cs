﻿using System;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U10
{
    internal class U1000
    {
        private static void main(string[] args)
        {
            string[] array = {"1", "2", "3", "4", "5"};
            var s = "";
            for (var i = 0; i < array.Length; i++)
            {
                s += array[i];
                if (i != array.Length - 1)
                    s += "   ";
            }

            Console.WriteLine(s);
        }
    }
}