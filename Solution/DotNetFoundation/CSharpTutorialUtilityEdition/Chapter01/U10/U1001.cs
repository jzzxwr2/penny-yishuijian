﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U10
{
    class U1001
    {
        static void main(string[] args)
        {
            string[] array = {"1", "1", "1", "1", "1", "1"};
            string s = string.Empty;
            for (int i = 0; i < array.Length; i++)
            {
                s += array[i];
                if (i != array.Length - 1)
                    s += "   ";
            }

            Console.WriteLine(s);
        }
    }
}
