﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U07
{
    class U0700
    {
        static void main(string[] args)
        {
            PointU0700 p1;
            PointU0700 p2;
            p1.x = 2;
            p1.y = 6;
            p2 = p1;
            PointU0700 p3 = new PointU0700();
            PointU0700 p4 = new PointU0700();
            p4.x = Convert.ToInt32(Console.ReadLine());
            p4.y = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("{0},{1}",p1.x,p1.y);
            Console.WriteLine("{0},{1}",p2.x,p2.y);
            Console.WriteLine("{0},{1}",p3.x,p3.y);
            Console.WriteLine("{0},{1}",p4.x,p4.y);
        }
    }

    struct PointU0700
    {
        public int x, y;
    }
}
