﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U25
{
    class U25
    {
        static void main(string[] args)
        {
            RecU25 rec = new RecU25(3, 5);
            CirU25 c = new CirU25(3);
            rec.Area();
            c.Area();
        }
    }
    class RecU25 : PointU25
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }

        public RecU25(double length, double width)
        {
            Length = length;
            Width = width;
        }

        public void Area()
        {
            Console.WriteLine(Length*Width);
        }
    }
    class CirU25 : PointU25
    {
        public CirU25(double r)
        {
            R = r;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double R { get; set; }
        public void Area()
        {
            Console.WriteLine(3.14 * R * R);
        }
    }

    interface PointU25
    {
        double X { get; set; }
        double Y { get; set; }
        void Area();

    }
}
