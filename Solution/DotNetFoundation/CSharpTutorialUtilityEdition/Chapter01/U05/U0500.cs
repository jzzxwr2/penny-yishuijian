﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U05
{
    class U0500
    {
        static void main(string[] args)
        {
            EnterCountU0500 ec = new EnterCountU0500();
            Console.WriteLine(ec.EnterToCount());
            Console.ReadKey(true);
        }
    }

    class EnterCountU0500
    {
        private int Entered = 0;

        public int EnterToCount()
        {
            while (true)
            {
                ConsoleKeyInfo cki = Console.ReadKey();
                if (cki.KeyChar == 13)
                {
                    Entered++;
                }
                else break;
            }

            return Entered;
        }
    }
}
