﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U05
{
    class U0501
    {
        static void main(string[] args)
        {

        }
    }

    class EnterToCountU0501
    {
        private int Entered = 0;

        public int EnterToCount()
        {
            while (true)
            {
                ConsoleKeyInfo cki = Console.ReadKey();
                if (cki.KeyChar == 13)
                {
                    Entered++;
                }
                else break;
            }

            return Entered;
        }
    }
}
