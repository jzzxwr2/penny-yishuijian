﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U21
{
    class U21
    {
        static void main(string[] args)
        {
            double d = Scroot(9);
            double d2 = Scroot(12);
            Console.WriteLine(d);
            Console.WriteLine(d2);
        }
        static double Scroot(int a)
        {
            return Math.Sqrt(a);
        }
        static double Scroot(long a)
        {
            return Math.Sqrt(a);
        }
        static double Scroot(double a)
        {
            return Math.Sqrt(a);
        }
    }
}
