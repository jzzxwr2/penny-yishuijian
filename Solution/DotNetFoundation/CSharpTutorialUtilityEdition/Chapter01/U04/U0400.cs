﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U03;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U04
{
    class U0400
    {
        static void main(string[] args)
        {
            RectangleU0400 rec = new RectangleU0400(3, 5, new PointU0300(2, 2));
            Console.WriteLine("{0},{1},{2},{3}",rec.Length,rec.Width,rec.StartPoint.X,rec.StartPoint.Y);
        }
    }

    class RectangleU0400
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public PointU0300 StartPoint { get; set; }

        public RectangleU0400()
        {
            Length = 1;
            Width = 1;
            StartPoint=new PointU0300(0,0);
        }

        public RectangleU0400(double length, double width, PointU0300 startPoint)
        {
            Length = length;
            Width = width;
            StartPoint = startPoint;
        }
    }
}
