﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U14
{
    class U1400
    {
        static void main(string[] args)
        {
            /*string s = Console.ReadLine();
            string[] array = s.Split();
            int len = 0;
            foreach (string s1 in array)
            {
                len += s1.Length;
            }
            Console.WriteLine(len);*/

            string s = Console.ReadLine();
            int len = 0;
            foreach (char c in s)
            {
                len++;
            }
            Console.WriteLine(len);
        }
    }
}
