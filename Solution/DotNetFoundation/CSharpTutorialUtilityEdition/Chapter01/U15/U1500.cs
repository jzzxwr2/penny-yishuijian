﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U15
{
    class U1500
    {
        static void main(string[] args)
        {
            try
            {
                int x = Convert.ToInt32(Console.ReadLine());
                int y = Convert.ToInt32(Console.ReadLine());
                int sum = x + y;
                Console.WriteLine(sum);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
