﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U03
{
    class U0300
    {
        static void main(string[] args)
        {
            PointU0300 p = new PointU0300();
            PointU0300 p2 = new PointU0300(1, 2);
            Console.WriteLine("{0},{1}",p.X,p.Y);
            Console.WriteLine("{0},{1}",p2.X,p2.Y);
        }
    }

    class PointU0300
    {
        /*private int _x;
        private int _y;

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }*/
        public double X { get; set; }
        public double Y { get; set; }

        public PointU0300()
        {
            X = 0;
            Y = 0;
        }

        public PointU0300(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
