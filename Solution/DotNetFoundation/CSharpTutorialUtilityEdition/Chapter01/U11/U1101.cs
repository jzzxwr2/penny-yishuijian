﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U11
{
    class U1101
    {
        static void main(string[] args)
        {
            for (int i = 0; i < 3; i++)
            {
                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        Console.WriteLine("小");
                        break;
                    case 6:
                        Console.WriteLine("猜对了！");
                        break;
                    case 7:
                    case 8:
                    case 9:
                        Console.WriteLine("大");
                        break;
                }
            }

        }
    }
}
