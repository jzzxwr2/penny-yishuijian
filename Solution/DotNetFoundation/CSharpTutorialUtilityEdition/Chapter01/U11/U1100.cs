﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U11
{
    class U1100
    {
        static void main(string[] args)
        {
            
            for (int i = 0; i < 3; i++)
            {
                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 6:
                    case 7:
                        Console.WriteLine("小");
                        break;
                    case 8:
                        Console.WriteLine("猜对了！");
                        break;
                    default:
                        Console.WriteLine("大");
                        break;

                }
            }
        }
    }
}
