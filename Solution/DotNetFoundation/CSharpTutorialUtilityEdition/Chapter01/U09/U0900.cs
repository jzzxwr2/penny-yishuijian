﻿using System;
using System.Linq;

namespace DotNetFoundation.CSharpTutorialUtilityEdition.Chapter01.U09
{
    internal class U0900
    {
        private static void main(string[] args)
        {
            /*string s = "  aCl df  eAjPs";
            Console.WriteLine(s.Trim());*/

            var s = "aYou win sOme.You loSe some.";
            //string[] subs = s.Split(' ');
            //string[] subs = s.Split('.');
            /*string[] subs = s.Split(' ','.');
            foreach (var sub in subs)
            {
                Console.WriteLine(sub);
            }*/

            /*Console.WriteLine(s.Substring(1));
            Console.WriteLine(s.Substring(1,2));*/

            /*char[] array = s.ToCharArray(1, 2);
            foreach (var c in array)
            {
                Console.WriteLine(c);
            }*/

            //string s2 = "df sA KDa ";
            string s2 = "df sA KDa .";
            Console.WriteLine(method(s2));

            string s3 = " 1 ";
            string s4 = "12345";
            //Console.WriteLine(s3[0]);
            //Console.WriteLine(s3[1]);
            //Console.WriteLine(s3.Length);
            //Console.WriteLine(s4.Length);
            //Console.WriteLine(s3.Split().Length);
            //Console.WriteLine(s4.Split().Length);
            //Console.WriteLine(s4.Split().Count());

            /*var strArray = s2.Split(" ".ToCharArray());
            foreach (var str in strArray) Console.WriteLine(str);

            Console.WriteLine(s2.Substring(0, 1).ToUpper());
            var s3 = string.Empty;
            s3 += s2.Substring(1);
            Console.WriteLine(s3);*/
        }

        private static string method(string str)
        {
            var strArray = str.Split(" ".ToCharArray());
            var result = string.Empty;
            foreach (var s in strArray) 
                result += s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower() + " ";

            return result;
        }
    }
}