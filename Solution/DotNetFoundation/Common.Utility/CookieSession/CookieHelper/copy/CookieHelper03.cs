﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DotNetFoundation.Common.Utility.CookieSession.CookieHelper.copy
{
    public class CookieHelper03
    {
        public static void ClearCookie(string cookiename)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookiename];
            if (cookie!=null)
            {
                cookie.Expires = DateTime.Now.AddYears(-3);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public static string GetCookieValue(string cookiename)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookiename];
            string str = string.Empty;
            if (cookie!=null)
            {
                str = HttpUtility.UrlDecode(cookie.Value);
            }

            return str;
        }

        public static void SetCookie(string cookiename, string cookievalue)
        {
            SetCookie(cookiename,cookievalue,DateTime.Now.AddDays(1.0));
        }
       
        public static void SetCookie(string cookiename, string cookievalue, DateTime expires)
        {
            HttpCookie cookie=new HttpCookie(cookiename)
            {
                Value=cookievalue,
                Expires=expires
            };
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}
