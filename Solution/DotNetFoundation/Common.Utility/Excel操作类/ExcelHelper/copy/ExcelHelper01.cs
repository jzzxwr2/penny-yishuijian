﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;

namespace DotNetFoundation.Common.Utility.Excel操作类.ExcelHelper.copy
{
    public class ExcelHelper01
    {
        /// <summary>
        /// 到处Excel文件，自动返回可下载的文件流
        /// </summary>
        /// <param name="dtData"></param>

        #region 数据导出至Excel文件

        public static void DataTable1Excel(DataTable dtData)
        {
            GridView gvExport = null;
            HttpContext curContext = HttpContext.Current;
            StringWriter strWriter = null;
            HtmlTextWriter htmlWriter = null;
            if (dtData!=null)
            {
                curContext.Response.ContentType = "application/vnd.ms-excel";
                curContext.Response.ContentEncoding = Encoding.GetEncoding("gb2312");
                curContext.Response.Charset = "utf-8";
                strWriter = new StringWriter();
                htmlWriter = new HtmlTextWriter(strWriter);
                gvExport = new GridView();
                gvExport.DataSource = dtData.DefaultView;
                gvExport.AllowPaging = false;
                gvExport.DataBind();
                gvExport.RenderControl(htmlWriter);
                curContext.Response.Write("<meta http-equiv=\"Content-Type\"content=\"text/html;charset=gb2312\"/>" +
                                          strWriter.ToString());
                curContext.Response.End();
            }
        }
        /// <summary>
        /// 导出Excel文件，转换为可读模式
        /// </summary>
        /// <param name="dtData"></param>
        public static void DataTable2Excel(DataTable dtData)
        {
            DataGrid dgExport = null;
            HttpContext curContext = HttpContext.Current;
            StringWriter strWriter = null;
            HtmlTextWriter htmlWriter = null;
            if (dtData!=null)
            {
                curContext.Response.ContentType = "application/vnd.ms-excel";
                curContext.Response.ContentEncoding = Encoding.UTF8;
                curContext.Response.Charset = "";
                strWriter = new StringWriter();
                htmlWriter = new HtmlTextWriter(strWriter);
                dgExport = new DataGrid();
                dgExport.DataSource = dtData.DefaultView;
                dgExport.AllowPaging = false;
                dgExport.DataBind();
                dgExport.RenderControl(htmlWriter);
                curContext.Response.Write(strWriter.ToString());
                curContext.Response.End();
            }
        }
        /// <summary>
        /// 导出Excel文件，并自定义文件名
        /// </summary>
        /// <param name="dtData"></param>
        /// <param name="FileName"></param>
        public static void DataTable3Excel(DataTable dtData, string FileName)
        {
            GridView dgExport = null;
            HttpContext curContext = HttpContext.Current;
            StringWriter strWriter = null;
            HtmlTextWriter htmlWriter = null;
            if (dtData!=null)
            {
                HttpUtility.UrlEncode(FileName, Encoding.UTF8);
                curContext.Response.AddHeader("content-disposition",
                    "attachment;filename=" + HttpUtility.UrlEncode(FileName, Encoding.UTF8) + ".xls");
                curContext.Response.ContentType = "applicaton nd.ms-excel";
                curContext.Response.ContentEncoding = Encoding.UTF8;
                curContext.Response.Charset = "GB2312";
                strWriter = new StringWriter();
                htmlWriter = new HtmlTextWriter(strWriter);
                dgExport = new GridView();
                dgExport.DataSource = dtData.DefaultView;
                dgExport.AllowPaging = false;
                dgExport.DataBind();
                dgExport.RenderControl(htmlWriter);
                curContext.Response.Write(strWriter.ToString());
                curContext.Response.End();
            }
        }
        /// <summary>
        /// 将数据导出至Excel文件
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="ExcelFilePath"></param>
        /// <returns></returns>
        public static bool OutputToExcel(DataTable Table, string ExcelFilePath)
        {
            if (File.Exists(ExcelFilePath))
            {
                throw new Exception("该文件已经存在！");
            }

            if ((Table.TableName.Trim().Length==0)|| (Table.TableName.ToLower()=="table"))
            {
                Table.TableName = "Sheet1";
            }

            int ColCount = Table.Columns.Count;

            int i = 0;

            OleDbParameter[] para = new OleDbParameter[ColCount];

            string TableStructStr = @"Creat Table" + Table.TableName + "(";

            string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + ExcelFilePath +
                                ";Extended Properties=Excel 8.0;";
            OleDbConnection objConn = new OleDbConnection(connString);

            OleDbCommand objCmd = new OleDbCommand();

            ArrayList DataTypeList = new ArrayList();
            DataTypeList.Add("System.Decimal");
            DataTypeList.Add("System.Double");
            DataTypeList.Add("System.Int16");
            DataTypeList.Add("System.Int32");
            DataTypeList.Add("System.Int64");
            DataTypeList.Add("System.Single");

            foreach (DataColumn col in Table.Columns)
            {
                if (DataTypeList.IndexOf(col.DataType.ToString())>0)
                {
                    para[i] = new OleDbParameter("@" + col.ColumnName, OleDbType.Double);
                    objCmd.Parameters.Add(para[i]);

                    if (i+1==ColCount)
                    {
                        TableStructStr += col.ColumnName + "double";
                    }
                    else
                    {
                        TableStructStr += col.ColumnName + "double,";
                    }
                }
                else
                {
                    para[i] = new OleDbParameter("@" + col.ColumnName, OleDbType.VarChar);
                    objCmd.Parameters.Add(para[i]);

                    if (i+1==ColCount)
                    {
                        TableStructStr += col.ColumnName + "varchar";
                    }
                    else
                    {
                        TableStructStr += col.ColumnName + "varchar,";
                    }
                }

                i++;
            }

            try
            {
                objCmd.Connection = objConn;
                objCmd.CommandText = TableStructStr;

                if (objConn.State==ConnectionState.Closed)
                {
                    objConn.Open();
                }

                objCmd.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                throw exp;
            }

            string InsertSql_1 = "Insert into" + Table.TableName + "(";
            string InsertSql_2 = "Values(";
            string InsertSql = "";

            for (int colID = 0; colID < ColCount; colID++)
            {
                if (colID+1==ColCount)
                {
                    InsertSql_1 += Table.Columns[colID].ColumnName + ")";
                    InsertSql_2 += "@" + Table.Columns[colID].ColumnName + ")";
                }
                else
                {
                    InsertSql_1 += Table.Columns[colID].ColumnName + ",";
                    InsertSql_2 += "@" + Table.Columns[colID].ColumnName + ",";
                }
            }

            InsertSql = InsertSql_1 + InsertSql_2;

            for (int rowID = 0; rowID < ColCount; rowID++)
            {
                for (int colID = 0; colID < ColCount; colID++)
                {
                    if (para[colID].DbType==DbType.Double&&Table.Rows[rowID][colID].ToString().Trim()=="")
                    {
                        para[colID].Value = 0;
                    }
                    else
                    {
                        para[colID].Value = Table.Rows[rowID][colID].ToString().Trim();
                    }
                }

                try
                {
                    objCmd.CommandText = InsertSql;
                    objCmd.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                }
            }

            try
            {
                if (objConn.State==ConnectionState.Open)
                {
                    objConn.Close();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return true;
        }
        /// <summary>
        /// 将数据导出至Excel文件
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="Columns"></param>
        /// <param name="ExcelFilePath"></param>
        /// <returns></returns>
        public static bool OutputToExcel(DataTable Table, ArrayList Columns, string ExcelFilePath)
        {
            if (File.Exists(ExcelFilePath))
            {
                throw new Exception("该文件一ing存在！");
            }

            if (Columns.Count>Table.Columns.Count)
            {
                for (int s = Table.Columns.Count; s <=Columns.Count; s++)
                {
                    Columns.RemoveAt(s);
                }
            }

            DataColumn column = new DataColumn();
            for (int j = 0; j < Columns.Count; j++)
            {
                try
                {
                    column = (DataColumn) Columns[j];
                }
                catch (Exception )
                {
                    Columns.RemoveAt(j);
                }
            }

            if ((Table.TableName.Trim().Length==0)||(Table.TableName.ToLower()=="table"))
            {
                Table.TableName = "Sheet1";
            }

            int ColCount = Columns.Count;

            OleDbParameter[] para = new OleDbParameter[ColCount];

            string TableStructStr = @"Creat Table" + Table.TableName + "(";

            string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + ExcelFilePath +
                                ";Extended Properties=Excel 8.0;";
            OleDbConnection objConn = new OleDbConnection(connString);

            OleDbCommand objCmd = new OleDbCommand();

            ArrayList DataTypeList = new ArrayList();
            DataTypeList.Add("System.Decimal");
            DataTypeList.Add("System.Double");
            DataTypeList.Add("System.Int16");
            DataTypeList.Add("System.Int32");
            DataTypeList.Add("System.Int64");
            DataTypeList.Add("System.IntSingle");

            DataColumn col = new DataColumn();
            for (int k = 0; k < ColCount; k++)
            {
                col = (DataColumn) Columns[k];

                if (DataTypeList.IndexOf(col.DataType.ToString().Trim())>=0)
                {
                    para[k] = new OleDbParameter("@" + col.Caption.Trim(), OleDbType.Double);
                    objCmd.Parameters.Add(para[k]);

                    if (k+1==ColCount)
                    {
                        TableStructStr += col.Caption.Trim() + "Double";
                    }
                    else
                    {
                        TableStructStr += col.Caption.Trim() + "Double,";
                    }
                }
                else
                {
                    para[k] = new OleDbParameter("@" + col.Caption.Trim(), OleDbType.VarChar);
                    objCmd.Parameters.Add(para[k]);

                    if (k+1==ColCount)
                    {
                        TableStructStr += col.Caption.Trim() + "VarChar";
                    }
                    else
                    {
                        TableStructStr += col.Caption.Trim() + "VarChar,";
                    }
                }
            }

            try
            {
                objCmd.Connection = objConn;
                objCmd.CommandText = TableStructStr;
                if (objConn.State==ConnectionState.Closed)
                {
                    objConn.Open();
                }

                objCmd.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                throw exp;
            }

            string InsertSql_1 = "Insert into" + Table.TableName + "(";
            string InsertSql_2 = "Values(";
            string InsertSql = "";

            for (int colID = 0; colID < ColCount; colID++)
            {
                if (colID+1==ColCount)
                {
                    InsertSql_1 += Columns[colID].ToString().Trim() + ")";
                    InsertSql_2 += "@" + Columns[colID].ToString().Trim() + ")";
                }
                else
                {
                    InsertSql_1 += Columns[colID].ToString().Trim() + ",";
                    InsertSql_2 += "@" + Columns[colID].ToString().Trim() + ",";
                }
            }

            InsertSql = InsertSql_1 + InsertSql_2;

            DataColumn DataCol = new DataColumn();
            for (int rowID = 0; rowID < Table.Rows.Count; rowID++)
            {
                for (int colID = 0; colID < ColCount; colID++)
                {
                    DataCol = (DataColumn) Columns[colID];
                    if (para[colID].DbType==DbType.Double&&Table.Rows[rowID][DataCol.Caption].ToString().Trim()=="")
                    {
                        para[colID].Value = 0;
                    }
                    else
                    {
                        para[colID].Value = Table.Rows[rowID][DataCol.Caption].ToString().Trim();
                    }
                }

                try
                {
                    objCmd.CommandText = InsertSql;
                    objCmd.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                }
            }

            try
            {
                if (objConn.State==ConnectionState.Open)
                {
                    objConn.Close();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return true;
        }

        #endregion
        /// <summary>
        /// 获取Excel文件数据表列表
        /// </summary>
        /// <param name="ExcelFileName"></param>
        /// <returns></returns>
        /*public static ArrayList GetExcelTables(string ExcelFileName)
        {

        }*/
    }
}
