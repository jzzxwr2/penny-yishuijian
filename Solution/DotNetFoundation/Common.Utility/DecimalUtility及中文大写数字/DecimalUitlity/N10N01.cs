﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Common.Utility.DecimalUtility及中文大写数字.DecimalUitlity
{
    class N10N01
    {
        static void main(string[] args)
        {
            /*string value = "1345,978";
            NumberStyles style = NumberStyles.Number;
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
            decimal number;
            if (Decimal.TryParse(value,style,culture,out number))
            {
                Console.WriteLine("{0},{1}",value,number);
            }
            else
            {
                Console.WriteLine(value);
            }*/

            /*object decimalObj="123,45";
            Decimal decValue=236.23M;
            Decimal.TryParse(decimalObj.ToString(), out decValue);
            Console.WriteLine(decValue);*/

            /*decimal d = 12.3M;
            Console.WriteLine(Decimal.Truncate(d));*/

            /*Console.WriteLine(int.Parse("1234"));
            Console.WriteLine(int.Parse("abcd"));*/

            /*string s = "abcdefg";
            Console.WriteLine(s.Substring(0,1));
            Console.WriteLine(s.Substring(1,3));*/

            /*Console.WriteLine(Math.Pow(2,3));
            Console.WriteLine(Math.Floor(12.3));*/

            Console.WriteLine(ToFixed(12.345678, 3));
        }
        public static double ToFixed(double d, int s)
        {
            double sp = Math.Pow(10, s);
//            return Math.Truncate(d) + Math.Truncate((d - Math.Truncate(d)) * sp) / sp;
            return Math.Floor(d) + Math.Truncate((d - Math.Truncate(d)) * sp) / sp;
        }
    }
}
