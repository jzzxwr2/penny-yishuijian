﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Common.Utility.Chart图形.OWCChart11.copy
{
    class OWCChartc01
    {
        #region 属性

        private string _phaysicalimagepath;
        private string _title;
        private string _seriesname;
        private int _picwidth;
        private int _pichight;
        private DataTable _datasource;
        private string strCategory;
        private string strValue;

        public string PhaysicalImagePath
        {
            set { _phaysicalimagepath = value; }
            get { return _phaysicalimagepath; }
        }

        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }

        public string SeriesName
        {
            set { _seriesname = value; }
            get { return _seriesname; }
        }

        public int PicWidth
        {
            set { _pichight = value; }
            get { return _picwidth; }
        }

        public int PicHight
        {
            set { _pichight = value; }
            get { return _pichight; }
        }

        private string GetColumnsStr(DataTable dt)
        {
            StringBuilder strList = new StringBuilder();
            foreach (DataRow r in dt.Rows)
            {
                strList.Append(r[0].ToString() + '\t');
            }
            return strList.ToString();
        }

        private string GetValueStr(DataTable dt)
        {
            StringBuilder strList = new StringBuilder();
            foreach (DataRow r in dt.Rows)
            {
                strList.Append(r[1].ToString() + '\t');
            }

            return strList.ToString();
        }
        public DataTable DataSource
        {
            set
            {
                _datasource = value;
                strCategory = GetColumnsStr(_datasource);
                strValue = GetValueStr(_datasource);
            }
            get { return _datasource; }
        }

        #endregion

        public OWCChartc01()
        { }

        public OWCChartc01(string PhaysicalImagePath,string Title,string SeriesName)
        {
            _phaysicalimagepath = PhaysicalImagePath;
            _title = Title;
            _seriesname = SeriesName;
        }
        /// <summary>
        /// 柱形图
        /// </summary>
        /// <returns></returns>
        /*public string CreateColumn()
        {
            Microsoft.Office.Interop.Owc11.ChartSpace objCSpace=new
        }*/
        
    }
}
