﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E0
{
    class P11E01
    {
        static void main(string[] args)
        {
            int[] nums = {1, 3, 2, 8, 6};
            IEnumerable<int> lownums =
                from n in nums
                where n < 5
                select n;
            foreach (var i in lownums)
            {
                Console.WriteLine(i);
            }
        }
    }
}
