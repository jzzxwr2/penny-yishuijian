﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E0
{
    class P11E02
    {
        static void main(string[] args)
        {
            int[] nums = {1, 5, 8, 12, 56, 70};
            IEnumerable<int> lownums =
                from n in nums
                where n < 10
                select n;
            foreach (var i in lownums)
            {
                Console.WriteLine(i);
            }
        }
    }
}
