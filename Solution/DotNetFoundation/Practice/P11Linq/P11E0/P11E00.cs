﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DotNetFoundation.Practice.P11Linq.P11E0
{
    /*
     * 11 linq
     * 11.0 简单示例
     */
    class P11E00
    {
        static void main(string[] args)
        {
            int[] nums = {2, 60, 1, 9, 3};
            IEnumerable<int> lownums =
                from n in nums
                where n < 10
                select n;
            foreach (var i in lownums)
            {
                Console.WriteLine(i);
            }
        }
    }
}
