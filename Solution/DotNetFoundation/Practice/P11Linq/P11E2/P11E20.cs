﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E2
{
    /*
     * 11 linq
     * 11.1 匿名类型
     * 例2
     */
    class P11E20
    {
        static void main(string[] args)
        {
            string Major = "history";
            var student = new {Age = 20, otherP11E20.Name, Major};
            Console.WriteLine("{0},{1},{2}",student.Name,student.Major,student.Age);
        }
    }

    class otherP11E20
    {
        static public string Name = "a";
    }
}
