﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E4
{
    /*
     * 11 linq
     * 11.3 Join子句
     * 例4
     */
    class P11E40
    {
        
        static void main(string[] args)
        {
            StudentP11E40[] students = new StudentP11E40[]
            {
                new StudentP11E40(){sId=1,LastName="a"}, 
                new StudentP11E40(){sId=2,LastName="b"}, 
                new StudentP11E40(){sId=3,LastName="c"}, 
            };
            CourseStudentP11E40[] studentInCourses=new CourseStudentP11E40[]
            {
                new CourseStudentP11E40(){courseName="art",stId  =2}, 
                new CourseStudentP11E40(){courseName="art",stId  =1}, 
                new CourseStudentP11E40(){courseName="history",stId  =1}, 
                new CourseStudentP11E40(){courseName="history",stId  =3}, 
                new CourseStudentP11E40(){courseName="physics",stId =3}, 
            };
            IEnumerable<string> query = from s in students
                join c in studentInCourses on s.sId equals c.stId
                where c.courseName == "history"
                select s.LastName;
            foreach (var q in query)
            {
                Console.WriteLine(q);
            }
        }
    }

    class StudentP11E40
    {
        public int sId;
        public string LastName;
    }

    class CourseStudentP11E40
    {
        public string courseName;
        public int stId;
    }
}
