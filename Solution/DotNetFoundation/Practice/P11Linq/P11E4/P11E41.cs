﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E4
{
    class P11E41
    {
        static void main(string[] args)
        {
            sP11E41[] sts=new sP11E41[]
            {
                new sP11E41(){Name="a",Id=1}, 
                new sP11E41(){Name="b",Id=2}, 
                new sP11E41(){Name="c",Id=3}, 
            };
            sincP11E41[] sincs=new sincP11E41[]
            {
                new sincP11E41(){courseName="history",stId=3}, 
                new sincP11E41(){courseName="history",stId=2}, 
                new sincP11E41(){courseName="art",stId=1}, 
                new sincP11E41(){courseName="art",stId=2}, 
                new sincP11E41(){courseName="math",stId=3}, 
            };
            IEnumerable<string> query = from s in sts
                join c in sincs on s.Id equals c.stId
                where c.courseName == "art"
                select s.Name;
            foreach (string s in query)
            {
                Console.WriteLine(s);
            }
        }
    }

    class sP11E41
    {
        public string Name;
        public int Id;
    }

    class sincP11E41
    {
        public string courseName;
        public int stId;
    }
}
