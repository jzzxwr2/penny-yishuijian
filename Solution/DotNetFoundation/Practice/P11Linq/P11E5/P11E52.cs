﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotNetFoundation.Practice.P11Linq.P11E5
{
    class P11E52
    {
        static void main(string[] args)
        {
            var groupA = new[]{1, 2, 6, 8, 9};
            var gourpB = new[] {1, 6, 7, 12, 15};
            var nums=from a in groupA
                from b in gourpB
                where a<5&&b>10
                     select new { a, b, sum = a + b };
            foreach (var num in nums)
            {
                Console.WriteLine(num);
            }
        }
    }
}
