﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E5
{
    /*
     * 11 linq
     * 11.4 from..let..where片段
     * 例5  from子句
     */
    class P11E50
    {

        static void main(string[] args)
        {
            var groupA = new[] {3, 4, 5, 6};
            var groupB = new[] {6, 7, 8, 9};
            var nums = from a in groupA
                from b in groupB
                where a > 4 && b <= 8
                select new {a, b, sum = a + b};
            foreach (var num in nums)
            {
                Console.WriteLine(num);
            }
        }
    }
}
