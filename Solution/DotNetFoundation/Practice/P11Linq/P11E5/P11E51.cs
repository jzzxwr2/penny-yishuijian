﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E5
{
    class P11E51
    {
        static void main(string[] args)
        {
            var groupA = new[] {1, 3, 5, 6, 7};
            var groupB = new[] {2, 3, 6, 7, 9};
            var nums = from a in groupA
                from b in groupB
                where a > 5 && b < 5
                select new {a, b, sun = a + b};
            foreach (var num in nums)
            {
                Console.WriteLine(num);
            }
        }
    }
}
