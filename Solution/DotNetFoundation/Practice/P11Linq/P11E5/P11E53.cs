﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E5
{
    class P11E53
    {
        static void main(string[] args)
        {
            var groupA = new[] {1, 3, 6, 8, 9};
            var groupB = new[] {2, 3, 5, 15, 18};
            var nums = from a in groupA
                from b in groupB
                where a > 5 && b < 6
                select new {a, b, sum = a + b};
            foreach (var num in nums)
            {
                Console.WriteLine(num);
            }
        }
    }
}
