﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E3
{
    /*
     * 11 linq
     * 11.2 查询语法、方法语法
     * 例3
     */
    class P11E30
    {
        static void main(string[] args)
        {
            int[] nums = {3, 2, 6, 1, 9, 10, 20, 56};
            var numsQuery = from n in nums
                where n < 20
                select n;
            var numsMethod = nums.Where(x => x < 20);
            int numsCount = (from n in nums
                where n < 20
                select n).Count();
            foreach (var num in numsQuery)
            {
                Console.WriteLine(num);
            }

            foreach (var i in numsMethod)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();

            Console.WriteLine(numsCount);
        }
    }
}
