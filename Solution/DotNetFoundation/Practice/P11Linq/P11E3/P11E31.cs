﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E3
{
    class P11E31
    {
        static void main(string[] args)
        {
            int[] nums = {1, 6, 2, 20, 30, 18};
            var numsQuery = from n in nums
                where n < 10
                select n;
            var numsMethod = nums.Where(n => n < 10);
            var count = (from n in nums
                where n < 10
                select n).Count();
            foreach (var i in numsQuery)
            {
                Console.WriteLine(i);
            }

            foreach (var i in numsMethod)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();
            Console.WriteLine(count);
        }
    }
}
