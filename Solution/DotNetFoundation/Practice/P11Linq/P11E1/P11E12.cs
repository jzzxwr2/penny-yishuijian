﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E1
{
    class P11E12
    {
        static void main(string[] args)
        {
            var book = new {Name = "book1", Year = 1};
            Console.WriteLine("{0},{1}",book.Name,book.Year);
        }
    }
}
