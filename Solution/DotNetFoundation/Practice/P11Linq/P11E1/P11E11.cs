﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E1
{
    class P11E11
    {
        static void main(string[] args)
        {
            var student = new {Name = "a", Age = 20};
            Console.WriteLine("{0},{1}",student.Age,student.Name);
        }
    }
}
