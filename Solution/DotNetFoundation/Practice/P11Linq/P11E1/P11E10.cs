﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P11Linq.P11E1
{
    /*
     * 11 linq
     * 11.1 匿名类型
     * 例1
     */
    class P11E10
    {
        static void main(string[] args)
        {
            var student = new {Name = "a", Age = 10, Major = "History"};
            Console.WriteLine("{0},{1},{2}",student.Name,student.Age,student.Major);
        }
    }
}
