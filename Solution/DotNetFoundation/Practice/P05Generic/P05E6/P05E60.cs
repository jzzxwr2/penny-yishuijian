﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E6
{
    /*
     * 5 泛型
     * 5.6 泛型委托
     * 例1
     */
    class P05E60
    {
        static void main(string[] args)
        {
            deleP05E60<string> dele = new deleP05E60<string>(cP05E60.method);
//            deleP14E60<string> dele2 = cP14E60.method;
            dele += cP05E60.method2;
            dele("hi");
        }
    }
    delegate void deleP05E60<T>(T value);

    class cP05E60
    {
        public static void method(string s)
        {
            Console.WriteLine(s);
        }

        public static void method2(string s)
        {
            Console.WriteLine(s.ToUpper());
        }
    }
}
