﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E6
{
    class P05E61
    {
        static void main(string[] args)
        {
            deleP05E61<string> dele = cP05E61.method1;
            dele += cP05E61.method2;
            dele("HI");

        }
    }

    delegate void deleP05E61<T>(T value);

    class cP05E61
    {
        public static void method1(string s)
        {
            Console.WriteLine(s);
        }

        public static void method2(string s)
        {
            Console.WriteLine(s.ToLower());
        }
    }
}
