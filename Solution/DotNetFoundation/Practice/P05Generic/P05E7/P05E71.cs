﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E7
{
    class P05E71
    {
        static void main(string[] args)
        {
            deleP05E71<int, string, int> dele = cP05E71.method;
            
//            Console.WriteLine(dele(3, "test"));
            dele(1, "test");

        }
    }

    delegate TR deleP05E71<T1, T2, TR>(T1 p1, T2 p2);

    class cP05E71
    {
        public static int method(int i,string s)
        {
            Console.WriteLine(i + s.Length);
            return i + s.Length;
            
        }
    }
}
