﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E7
{
    /*
     * 5 泛型
     * 5.6 泛型委托
     * 例2
     */
    class P05E70
    {
        static void main(string[] args)
        {
            deleP05E70<int, int, string> dele = cP05E70.method;
//            var dele2=new deleP14E70<int,int,string>(cP14E70.method);
            Console.WriteLine(dele(1,2));
        }
    }

    delegate TR deleP05E70<T1, T2, TR>(T1 p1, T2 p2);

    
    class cP05E70
    {
        public static string method(int p1, int p2)
        {
            int total = p1 + p2;
            return total.ToString();
        }
    }
}
