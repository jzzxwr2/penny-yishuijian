﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E2
{
    /*
     * 5 泛型
     * 5.2 类型参数的约束
     * 例2 类约束
     */
    class P05E14
    {
        static void main(string[] args)
        {
            homeP05E14<Book> b = new homeP05E14<Book>();
            b.Push(new Book(10));
            b.Push(new Book(20));
            b.Print();

            //homeP14E14<Phone> p = new homeP14E14<Phone>();
            //p.Push(new Phone("a"));
            //p.Push(new Phone("b"));
            //p.Push(new Phone("c"));
            //p.Push(new Phone("d"));
            //p.Push(new Phone("e"));
            //p.Print();
        }
    }
    class homeP05E14<T>
        where T : Book
    {
        T[] home;
        int Pointer = 0;
        const int Max = 3;
        bool IsFull { get { return Pointer >= Max; } }
        bool IsEmptry { get { return Pointer <= 0; } }
        public void Push(T x)
        {
            if (!IsFull) home[Pointer++] = x;
        }
        public void Print()
        {
            for (int i =Pointer-1; i >=0; i--)
            {
                Console.WriteLine(home[i]);
            }
        }
        public homeP05E14()
        {
            home = new T[Max];
        }
    }
    class Book
    {
        public int Year { get; set; }
        public Book(int year)
        {
            Year = year;
        }
    }
    class Phone
    {
        public int Year { get; set; }
        public string Name { get; set; }
        public Phone(int year)
        {
            Year = year;
        }
        public Phone(string name)
        {
            Name = name;
        }
    }
}
