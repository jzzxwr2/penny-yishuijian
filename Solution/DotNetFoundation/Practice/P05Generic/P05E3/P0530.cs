﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E3
{
    /*
     * 5 泛型
     * 5.3 泛型方法
     * 例3 
     */
    class P0530
    {
        static void main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5 };
            classP05E30.ReverseAndPrint(array);
        }
    }
    class classP05E30
    {
        static public void ReverseAndPrint(int[] arr)
        {
            Array.Reverse(arr);
            foreach (int item in arr)
            {
                Console.WriteLine(item);
            }
        }
    }
}
