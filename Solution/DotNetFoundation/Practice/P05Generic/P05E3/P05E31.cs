﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E3
{
    class P05E31
    {
        static void main(string[] args)
        {
            int[] a = new int[] { 1, 2, 3 };
            classP05E31.ReverseAndPrint(a);

            string[] b = new string[] { "a", "b", "c" };
            classP05E31.ReverseAndPrint(b);
        }
    }
    class classP05E31
    {
        public static void ReverseAndPrint<T>(T[] arr)
        {
            Array.Reverse(arr);
            foreach (T item in arr)
            {
                Console.WriteLine(item);
            }
        }
    }
}
