﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E4
{
    class P05E42
    {
        static void main(string[] args)
        {
            classP05E42 c = new classP05E42(1, 2, 3);
            c.Print();
        }
    }
    static class ExtendclassP05E42
    {
        public static void Print(this classP05E42 c)
        {
            int[] nums = c.GetValues();
            Console.WriteLine("{0},{1},{2}",nums[0], nums[1], nums[2]);
            
        }
    }
    class classP05E42
    {
        int[] nums = new int[3];
        public classP05E42(int i1,int i2,int i3)
        {
            nums[0] = i1;
            nums[1] = i2;
            nums[2] = i3;
        }
        public int[] GetValues()
        {
            return nums;
        }
    }
}
