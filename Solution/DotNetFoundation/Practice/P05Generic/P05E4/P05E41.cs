﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E4
{
    class P05E41
    {
        static void main(string[] args)
        {
            classP05E41 c = new classP05E41(1, 2, 3);
            c.Print();
        }
    }
    static class ExtendclassP05e41
    {
        public static void Print(this classP05E41 c)
        {
            int[] vals = c.GetValues();
            Console.WriteLine("{0},{1},{2}",vals[0], vals[1], vals[2]);
        }
    }
    class classP05E41
    {
        int[] nums = new int[3];
        public classP05E41(int i1,int i2,int i3)
        {
            nums[0] = i1;
            nums[1] = i2;
            nums[2] = i3;
        }
        public int[] GetValues()
        {
            return nums;
        }
        
    }
}
