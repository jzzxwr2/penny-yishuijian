﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E4
{
    /*
     * 5 泛型
     * 5.4 扩展方法和泛型类
     * 例4 
     */
    class P05E40
    {
        static void main(string[] args)
        {
            classP05E40 c = new classP05E40(1, 2, 3);
            c.GetValues();
            c.Print();
        }
    }
    static class ExtendclassP05E40
    {
        public static void Print(this classP05E40 c)
        {
            int[] vals = c.GetValues();
            Console.WriteLine("{0},{1},{2}",vals[0], vals[1], vals[2]);
        }
    }
    class classP05E40
    {
        int[] vals = new int[3];
        public classP05E40(int v0,int v1,int v2)
        {
            vals[0] = v0;vals[1] = v1;vals[2] = v2;
        }
        public int[] GetValues()
        {
            return vals;
        }
    }
}
