﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E5
{
    /*
     * 5 泛型
     * 5.5 泛型接口
     * 例5 引题-泛型
     */
    class P05E50
    {
        static void main(string[] args)
        {
            cP05E50 c = new cP05E50();
            int i=c.Return(1);
            Console.WriteLine(i);
        }
    }
    
    class cP05E50
    {
        public int Return(int i)
        {
            return i;
        }
    }
}
