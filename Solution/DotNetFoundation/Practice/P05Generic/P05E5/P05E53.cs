﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E5
{
    class P05E53
    {
        static void main(string[] args)
        {
            CP05E53<int> c=new CP05E53<int>();
            CP05E52<string> s = new CP05E52<string>();
            Console.WriteLine(c.GetValue(1));
            Console.WriteLine(s.GetValue("aaa"));
        }
    }
    interface IInteP05E53<T>
    {
        T GetValue(T value);
    }
    class CP05E53<T> : IInteP05E53<T>
    {
        public T GetValue(T value)
        {
            return value;
        }
    }
}
