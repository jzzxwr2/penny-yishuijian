﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E5
{
    /*
     * 5 泛型
     * 5.5 泛型接口
     * 例5 2-接口
     */
    class P05E52
    {
        static void main(string[] args)
        {
            CP05E52<int> i = new CP05E52<int>();
            CP05E52<string> s = new CP05E52<string>();
            Console.WriteLine(i.GetValue(5));
            Console.WriteLine(s.GetValue("aaa"));
        }
    }
    interface IInteP05E52<T>
    {
        T GetValue(T value);
    }
    class CP05E52<T> : IInteP05E52<T>
    {
        public T GetValue(T value)
        {
            return value;
        }
    }
}
