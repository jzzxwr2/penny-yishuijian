﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E5
{
    /*
     * 5 泛型
     * 5.5 泛型接口
     * 例5 1-泛型
     */
    class P05E51
    {
        static void main(string[] args)
        {
            cP05E51<int> c = new cP05E51<int>();
            int i = c.Return(2);
            Console.WriteLine(i);
            cP05E51<string> s = new cP05E51<string>();
            Console.WriteLine(s.Return("abc"));
        }
    }
    class cP05E51<T>
    {
        public T Return(T Value)
        {
            return Value;
        }
    }
}
