﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E1
{
    class P05E12
    {
        static void main(string[] args)
        {
            classP05E12 c = new classP05E12();
            c.Push(1);
            c.Push(2);
            c.Push(3);
            c.Push(4);
            c.Push(5);

            c.Print();
        }
    }
    class classP05E12
    {
        int[] nums;
        int Pointer = 0;
        const int Max = 3;
        bool IsFull { get { return Pointer >= Max; } }
        bool IsEmpty { get { return Pointer <= 0; } }
        public void Push(int x)
        {
            if (!IsFull) nums[Pointer++] = x;
        }
        public void Print()
        {
            for (int i = Pointer-1; i >=0; i--)
            {
                Console.WriteLine(nums[i]);
            }
        }
        public classP05E12()
        {
            nums = new int[Max];
        }
    }
}
