﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E1
{
    /*
     * 5 泛型
     * 5.1 例1 
     */
    class P05E10
    {
        static void main(string[] args)
        {
            //classP14E10<int> Int = new classP14E10<int>();
            //classP14E10<string> String = new classP14E10<string>();

            //Int.Push(3);
            //Int.Push(5);
            //Int.Push(7);
            //Int.Push(9);
            //Int.Print();

            //String.Push("fun");
            //String.Push("hi");
            //String.Print();


            classP05E10 c = new classP05E10();
            c.Push(1);
            c.Push(2);
            c.Push(3);
            c.Push(4);
            c.Push(5);
            c.Push(6);
            c.Push(7);

            c.Print();
        }
    }
    class classP05E10<T>
    {
        T[] array;
        int Pointer = 0;

        const int Max = 10;
        bool IsFull
        {
            get
            {
                return Pointer >= Max;
            }
        }
        bool IsEmpty
        {
            get
            {
                return Pointer <= 0;
            }
        }
        public void Push(T x)
        {
            if (!IsFull)
            {
                array[Pointer++] = x;
            }
        }
        public T Pop()
        {
            return (!IsEmpty) ? array[--Pointer] : array[0];
        }
        public classP05E10()
        {
            array = new T[Max];
        }
        public void Print()
        {
            for (int i = Pointer-1; i >=0; i--)
            {
                Console.WriteLine(array[i]);
            }
        }
    }
    class classP05E10
    {
        int[] nums;
        int Pointer = 0;

        const int Max = 5;

        bool IsFull
        {
            get
            {
                return Pointer >= Max;
            }
        }
        bool IsEmpty
        {
            get
            {
                return Pointer <= 0;
            }
        }

        public void Push(int x)
        {
            if (!IsFull) nums[Pointer++] = x;
        }
        public int Pop()
        {
            return (!IsEmpty) ? nums[--Pointer] : nums[0];
        }
        public void Print()
        {
            for (int i = Pointer-1; i >=0; i--)
            {
                Console.WriteLine(nums[i]);
            }
        }
        public classP05E10()
        {
            nums = new int[Max];
        }
    }
}
