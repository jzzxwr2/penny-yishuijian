﻿using System;

namespace DotNetFoundation.Practice.P05Generic.P05E1
{
    class P05E13
    {
        static void main(string[] args)
        {
            classP05E13<int> c = new classP05E13<int>();
            c.Push(1);
            c.Push(2);
            //c.Push(3);
            //c.Push(4);
            //c.Push(5);
            c.Print();

            classP05E13<string> c2 = new classP05E13<string>();
            c2.Push("a");
            c2.Push("b");
            c2.Push("c");
            c2.Push("d");
            c2.Push("e");
            c2.Print();
        }
    }
    class classP05E13<T>
    {
        T[] array;
        int Pointer = 0;
        const int Max = 3;
        bool IsFull { get { return Pointer >= Max; } }
        bool IsEmpty { get { return Pointer <= 0; } }
        public void Push(T x)
        {
            if (!IsFull) array[Pointer++] = x;
        }
        public void Print()
        {
            for (int i = Pointer-1; i >=0; i--)
            {
                Console.WriteLine(array[i]);
            }
        }
        public classP05E13()
        {
            array = new T[Max];
        }
    }
}
