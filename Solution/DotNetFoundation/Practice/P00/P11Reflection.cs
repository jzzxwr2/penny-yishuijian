﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习11：反射
    */
    class P11
    {
        static void main(string[] args)
        {
            var type = typeof(Enumerable);
            var methods = type.GetMethods(System.Reflection.BindingFlags.Public
                | System.Reflection.BindingFlags.Static);
            var groups = methods.GroupBy(m => m.Name);
            Console.WriteLine(groups.Count().ToString());
            foreach (var item in groups)
            {
                Console.WriteLine(item.First().Name);
            }
        }
    }
}
