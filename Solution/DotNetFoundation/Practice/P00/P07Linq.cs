﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习7：linq
    */
    class P07
    {
        static void main(string[] args)
        {
            /*
             * linq:语言集成查询，是一种从数据源检索数据的表达式
             * 分为三个步骤：获取数据源、创建、执行
             */


            //1、数据源：数据源是一个数组，支持IEnumerable<T>
            int[] nums = new int[] { 5, 3, 2, 7, 8, 1, 9, 4 };



            //2、创建查询
            //--查询变量本身不执行任何操作，且不返回任何数据
            //--var是IEnumerable<T> 的简单写法

            //*查询语法：调用标准查询运算符，如where,select,groupby,join,max,average等
            var numQuery =
                from num in nums
                where num > 2  //查询大于2的数字
                select num;

            var numQuery2 =
                from num in nums
                where num > 2  //查询大于2的数字
                orderby num descending //将大于2的数字降序排列
                select num;


            //*方法语法：

            //1)lambda表达式
            //-- => lambda运算符
            //-- =>前是输入变量，对应查询表达式中select的变量
            var numQuery3 = nums.Where(n => n > 2); //效果同numQuery
            var numQuery4 = nums.Where(n => n > 2).OrderByDescending(n => n);//效果同numQuery2

            //2)sum,max,min,average,contat：返回单一数值
            numQuery.Average();
            numQuery.Concat(numQuery3);


            //*混合查询语法和方法语法：
            var numCount = nums.Where(n => n > 2).Count();
            Console.WriteLine(numCount);



            //3、执行查询
            //*foreach：延迟执行
            foreach (var item in numQuery)
            {
                Console.WriteLine(item);
            }

            //*count,max,average：强制立即执行，返回单个值
            Console.WriteLine(numQuery.Count());

            //*tolist:强制立即执行，并缓存结果
            var list = numQuery.ToList();
        }
    }
}
