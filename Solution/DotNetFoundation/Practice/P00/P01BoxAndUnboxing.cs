﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
     * 练习1：装箱和拆箱
     */
    class P01
    {
        static void main(string[] args)
        {
            //装箱：将值类型转换为引用类型
            int i = 100;
            object obj = i;
            Console.WriteLine(obj);

            //拆箱：将引用类型转换为值类型。被装过箱才能被拆箱
            int i2 = 200;
            object obj2 = i2;
            int i3 = (int)obj2;
            Console.WriteLine(i3);
        }
    }
}
