﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    class P09
    {
        static void main(string[] args)
        {
            var nums = new int[]{1,3,5 };
            var nums2 = new int[] { 2, 4, 6 };
            var nums3 = new int[0];
            var nums4 = new int[] { 1, 2, 3, 3 };
            var nums5= new int[] { 8,1, 3, 5,6,7,2 };
            var col = new string[] { "小明，20", "小红，女，18","小丽" };
            var perlist = new List<Person>
            {
                new Person{Name="a",Age=20,FruitID=3 },
                new Person{Name="d",Age=3,FruitID=6 },
                new Person{Name="g",Age=50,FruitID=2 },
                new Person{Name="c",Age=3,FruitID=3 }
            };
            var frulist = new List<Fruit>
            {
                new Fruit{Name="apple",ID=1 },
                new Fruit{Name="banana",ID=3 },
                new Fruit{Name="peach",ID=2 },
                new Fruit{Name="orange",ID=3 },
                new Fruit{Name="watermelon",ID=6 },
            };

            ArrayList arrayli = new ArrayList();
            arrayli.Add("汽车");
            arrayli.Add(3);
            arrayli.Add(6.8);
            arrayli.Add(5);

            var days = new ArrayList();
            days.Add("Sun");
            days.Add("Mon");
            days.Add("Wed");
            days.Add("Tue");

            //Console.WriteLine(nums.Aggregate((a,b)=>a+b));
            //Console.WriteLine(nums.Aggregate((a, b) => a*b));
            //Console.WriteLine(col.Aggregate((a,b)=>a+"——"+b));

            //Console.WriteLine(nums.All(n=>n<10));

            //Console.WriteLine(nums.Any(n => n <0));

            //foreach (var item in nums.Append(7)) { Console.WriteLine(item); }

            //Console.WriteLine(nums.Average());

            //foreach (var item in days.Cast<string>()) { Console.WriteLine(item); }

            //foreach (var item in nums.Concat(nums2)) { Console.WriteLine(item); }

            //Console.WriteLine(nums.Contains(2));

            //Console.WriteLine(nums.Count());

            //foreach (var item in nums3.DefaultIfEmpty(0)) { Console.WriteLine(item); }

            //foreach (var item in nums4.Distinct()) { Console.WriteLine(item); }

            //Console.WriteLine(nums.ElementAt(1));

            //Console.WriteLine(nums.ElementAtOrDefault(8));

            //if (nums2.ElementAt(0) == 1) { Console.WriteLine(nums2.ElementAt(1)); }
            //else { Console.WriteLine(Enumerable.Empty<int>()); }

            //foreach (var item in nums4.Except(nums)) { Console.WriteLine(item); }

            //Console.WriteLine(nums2.First());

            //Console.WriteLine(nums3.FirstOrDefault());

            //foreach (var item in perlist.GroupBy(p => p.Age)) { Console.WriteLine(item.Key); }

            //var result = perlist.GroupJoin(frulist, p => p.FruitID, f => f.ID,
            //    (p, fs) => new {personname=p.Name,fruits=fs });
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item.personname);
            //    foreach (var item2 in item.fruits) { Console.WriteLine(item2.Name); }
            //}

            //foreach (var item in nums.Intersect(nums4)) { Console.WriteLine(item); }

            //var result = perlist.Join(frulist, p => p.FruitID, f => f.ID,
            //    (p, f) => new { personname = p.Name, fruitname = f.Name });
            //foreach (var item in result)
            //{
            //    Console.WriteLine("{0},{1}", item.personname,item.fruitname);
            //}

            //Console.WriteLine(nums.Last());

            //Console.WriteLine(nums3.LastOrDefault());

            //Console.WriteLine(nums.Max());
            //Console.WriteLine(nums.Min());

            //foreach (var item in arrayli.OfType<int>()) { Console.WriteLine(item); }

            //foreach (var item in perlist.OrderBy(p=>p.Age)) { Console.WriteLine(item.Age); }
            //foreach (var item in perlist.OrderByDescending(p=>p.Age)) { Console.WriteLine(item.Age); }

            //foreach (var item in nums.Prepend(-1)) { Console.WriteLine(item); }

            //foreach (var item in Enumerable.Range(3,5)) { Console.WriteLine(item); }

            //foreach (var item in Enumerable.Repeat(6,6)) { Console.WriteLine(item); }

            //foreach (var item in nums.Reverse()) { Console.WriteLine(item); }

            //foreach (var item in nums.Select(n => n * n)) { Console.WriteLine(item); }

            //Console.WriteLine(col.Select(p => p.Split()).Count());

            //col.Select(p => p.Split()).ToList().ForEach(pe => Console.WriteLine(pe.Count()));
            //col.SelectMany(p => p.Split()).ToList().ForEach(pe=>Console.WriteLine(pe));

            //Console.WriteLine(nums.SequenceEqual(nums5));

            //Console.WriteLine(nums.Single(n=>n>3));
            //Console.WriteLine(nums.SingleOrDefault(n => n > 5));

            //foreach (var item in nums5.Skip(3)) { Console.WriteLine(item); }

            //foreach (var item in nums5.OrderByDescending(n=>n).SkipWhile(n => n >=2)) { Console.WriteLine(item); }

            //Console.WriteLine(nums.Sum());

            //foreach (var item in nums5.Take(3)) { Console.WriteLine(item); }

            //foreach (var item in nums5.TakeWhile(n => n > 7)) { Console.WriteLine(item); }

            //foreach (var item in col.OrderBy(p=>p.Length).ThenBy(p=>p)){ Console.WriteLine(item); }

            //foreach (var item in perlist.OrderBy(p=>p.Age).ThenByDescending(p=>p.FruitID)) 
            //{ Console.WriteLine("{0},{1}",item.Age,item.FruitID); }

            //foreach (var item in perlist.Select(p => p.Name).ToArray()) Console.WriteLine(item);

            //foreach (var item in perlist.ToDictionary(p => p.Name))
            //{Console.WriteLine("{0},{1},{2}", item.Key, item.Value.Age, item.Value.FruitID);}

            //foreach (var item in perlist.Select(p => p.Name).ToList()) Console.WriteLine(item);

            //foreach (var item in perlist.ToLookup(p => p.Age))
            //{
            //    Console.WriteLine(item.Key);
            //    foreach(var item2 in item)
            //    {
            //        Console.WriteLine("{0},{1}",item2.Name,item2.FruitID);
            //    }
            //}

            //foreach (var item in nums.Union(nums2)) { Console.WriteLine(item); }

            //foreach (var item in perlist.Where(p => p.Age < 30)){ Console.WriteLine(item.Age); }

            //foreach (var item in nums.Zip(col, (n, p) => n+""+p)) { Console.WriteLine(item); }
        }
    }
    class Person
    { 
        public string Name { get; set; }
        public int Age { get; set; }
        public int FruitID { get; set; }
    }
    class Fruit
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
}
