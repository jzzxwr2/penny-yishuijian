﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习2：枚举类型
    */
    class P02
    {
        //枚举类型是值类型，
        //枚举列表中每个符号代表一个值，第一个值默认是0
        enum Colors { R, G, B };

        //也可以自定义每个符号的值
        enum Letters {A=3,B=5,C=7};
        static void main(string[] args)
        {
            Console.WriteLine(Colors.R);
            int numR = (int)Colors.R;
            Console.WriteLine(numR);
            foreach (var item in Enum.GetNames(typeof(Letters)))
            {
                Console.WriteLine(item);
            }

            Console.WriteLine(Letters.B);
            int numL = (int)Letters.B;
            Console.WriteLine(numL);
        }
    }
    
}
