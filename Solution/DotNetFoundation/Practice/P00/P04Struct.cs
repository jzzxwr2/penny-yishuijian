﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习4：结构体
    */
    class P04
    {
        static void main(string[] args)
        {
            /*
             * 结构体相当于我们自己定义的一种复杂类型，它使得单一变量可以存储各种数据类型
             * 结构体要定义在class外或class里，不要定义在main里
             * 结构体特点：
             * --值类型
             * --不能赋初值
             * --不能声明默认的构造函数：必须在实例化前对成员变量进行初始化
             *   所有值类型都隐式声明一个称为默认构造函数的公共无参数实例构造函数
             *   结构类型默认值是通过将所有值类型字段设置为它们的默认值、将所有引用类型字段设置为 null 而产生的值
             *   默认构造函数返回一个零初始化实例，它就是该值类型的默认值。
             *   由于每个值类型都隐式地具有一个公共无参数实例构造函数，因此，
             *   一个结构类型中不可能包含一个关于无参数构造函数的显式声明   ——？。。
             * --不支持继承
             * --构造函数必须为所有字段赋值，不支持无参的构造函数
             */
            Student s1 = new Student();
            s1.SetValues("小红", '女', 20);
            s1.Display();

            Student s2 = new Student("小明",'男',30);
            Console.WriteLine("{0},{1}",s2.Name ,s2.Gender);//不能访问Age，因为它是privite

            Student s3 = new Student();
            Console.WriteLine("{0},{1}", s3.Name, s3.Gender);//——默认初始值是null?
        }
    }
    struct Student
    {
        public string Name;
        public char  Gender;
        private int Age;
        public Student (string name,char gender,int age)
        {
            Name = name;
            Gender = gender;
            Age = age;
        }
        public void SetValues(string name,char gender,int age)
        {
            Name = name;
            Gender = gender;
            Age = age;
        }
        public void Display()
        {
            Console.WriteLine("{0},{1},{2}",Name ,Age,Gender );
        }

    }
}
