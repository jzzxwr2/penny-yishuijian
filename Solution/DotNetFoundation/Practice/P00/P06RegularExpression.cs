﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习6：正则表达式
    */
    class P06
    {
        static void main(string[] args)
        {
            /*
             * 正则表达式是记录文本规则的代码
             * 
             * 1.元字符
             * . 换行符\n以外的任意字符
             * \b 单词的开始或结束
             * \d 数字
             * \w 英文、数字、下划线、汉字
             * \s 空白
             * ^ 字符串开始
             * $ 字符串结束
             * 
             * 2.转义字符\
             * 放在元字符前，查找元字符本身
             * 
             * 3.限定符：又叫重复描述字符，表示一个字符要出现的次数
             * * 0或多
             * + 1或多
             * ？0或1
             * {n} n
             * {n+} n或更多
             * {n,m} n到m
             * 
             * 4.字符类
             * [character_group] 任何单个字符
             * [first-last] 从first到last范围中任何单个字符
             */

            string RegexStr = string.Empty;
            RegexStr = "^[0-9]+$"; //0-9的数字
            Console.WriteLine(Regex.IsMatch("a4",RegexStr));

            RegexStr = "^[0-9]{3}$"; //三个数字
            Console.WriteLine(Regex.IsMatch("156", RegexStr));

            RegexStr = "abc[.]*"; //abc开头
            Console.WriteLine(Regex.IsMatch("abcdef", RegexStr));

            RegexStr = "^[A-Za-z]+$"; //字母
            Console.WriteLine(Regex.IsMatch("A4def", RegexStr));

            RegexStr = "[A-Za-z]{1,2}"; //一个或两个字母
            Console.WriteLine(Regex.IsMatch("[6d#f", RegexStr));

            RegexStr = "^abc[0-9]{3}$"; //abc开头 + 三个数字
            Console.WriteLine(Regex.IsMatch("abc123", RegexStr));

            RegexStr = "^abc[0-9]{3}[A-Za-z]{1,2}$"; //abc开头 + 三个数字 + 一个或两个字母
            Console.WriteLine(Regex.IsMatch("abc123gee", RegexStr));

            RegexStr = "^abc[0-9]{3}[A-Za-z]{1,2}[0-9]{3}$"; //abc开头 + 三个数字 + 一个或两个字母 + 三个数字
            Console.WriteLine(Regex.IsMatch("abc123ge731e", RegexStr));
        }
    }
}
