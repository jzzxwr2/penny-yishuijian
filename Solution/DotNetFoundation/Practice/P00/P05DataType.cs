﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习5：值类型、引用类型
    */
    class P05
    {
        static void main(string[] args)
        {
            /*
             * 值类型只需要一段单独的内存，即栈存储实际数据。如int,bool,char,struct,enum等
             * 引用类型需要两段内存：栈存储引用，堆存储实际数据，引用指向实际数据。如object,string,class等
             */

            //应用？
        }
    }
}
