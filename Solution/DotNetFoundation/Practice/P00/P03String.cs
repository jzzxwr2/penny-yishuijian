﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P00
{
    /*
    * 练习３：字符串
    */
    class P03
    {
        static void main(string[] args)
        {
            /*string str = "Test";
            //字符串截取
            //--提取第i个位置开始、长度为j的字符串   .SubString(i,j)
            Console.WriteLine(str.Substring(1,2));   //得到es

            //--从右边开始取i个字符串  .SubString(str.Length-i)
            Console.WriteLine(str.Substring(str.Length-2));  //得到st

            //--截取最后一个字符
            Console.WriteLine(str.Substring(str.Length-1));  //得到t



            //字符串遍历
            foreach (char item in str)
            {
                Console.WriteLine(item);
            }



            //字符串连接
            //+连接
            string str2 = "a" + "b";
            Console.WriteLine(str2);

            //.Join连接
            string[] strArr = new string[] { "A", "B" };
            string str3 = string.Join("   ",strArr);
            Console.WriteLine(str3);

            //.Concat连接
            string str4= string.Concat(str2, str3);
            Console.WriteLine(str4);

            //StringBuilder连接
            StringBuilder strB = new StringBuilder();
            strB.Append("x");
            strB.Append("y");
            Console.WriteLine(strB);*/

            string ss = "         cbfjkdcsieab a";
            char[] chars=new char[]{'a','b','c'};
            string s5=ss.Trim(chars);
            string s6 = ss.Trim();
            foreach (var item in ss)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\"");
        }
    }
}
