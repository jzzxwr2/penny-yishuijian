﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E1
{
    class A01E13
    {
        static void main(string[] args)
        {
            int[] arr = {2, 9, 1, 3, 12};
            int[] newar = cA01E13.method(arr);
            foreach (var i in newar)
            {
                Console.WriteLine(i);
            }
        }
    }

    class cA01E13
    {
        private int[] nums;

        public static int[] method(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = arr.Length - 1; j >i; j--)
                {
                    if (arr[i]%2!=0&&arr[j]%2==0)
                    {
                        int temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }

            return arr;
        }
    }
}
