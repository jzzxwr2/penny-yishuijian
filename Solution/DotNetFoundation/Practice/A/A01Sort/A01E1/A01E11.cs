﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E1
{
    class A01E11
    {
        static void main(string[] args)
        {
            cA01E11 c = new cA01E11();
            int[] array = {5, 1, 8, 3, 10};
            int[] newarr = c.method(array);
            foreach (var i in newarr)
            {
                Console.WriteLine(i);
            }
        }
    }

    class cA01E11
    {
        private int[] nums;

        public int[] method(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = array.Length-1; j >=i ; j--)
                {
                    if (array[i]%2!=0&&array[j]%2==0)
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }

            return array;
        }
    }
}
