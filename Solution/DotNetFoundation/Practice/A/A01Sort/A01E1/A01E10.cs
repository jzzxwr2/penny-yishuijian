﻿using System;

namespace DotNetFoundation.A.A01Sort.A01E1
{
    /*
     * 1 排序
     * 例1 奇偶
     */
    class A01E1
    {
        static void main(string[] args)
        {
            cA01E1 c = new cA01E1();
            int[] array = { 3, 2, 8, 1 };
            int[] newarray = c.SortArrayByParity(array);
            foreach (int item in newarray)
            {
                Console.WriteLine(item);
            }
        }

    }

    class cA01E1
    {
        private int[] nums;
        public int[] SortArrayByParity(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = array.Length - 1; j >= i; j--)
                {
                    if (array[i] % 2 != 0 && array[j] % 2 == 0)
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
            return array;
        }
    }
}