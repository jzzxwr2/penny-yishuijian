﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E1
{
    class A01E14
    {
        static void main(string[] args)
        {
            int[] nums = {5, 1, 2, 80, 33};
            int[] newarr = cA01E14.method(nums);
            foreach (var i in newarr)
            {
                Console.WriteLine(i);
            }
        }
    }

    class cA01E14
    {
        private int[] arr;

        public static int[] method(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = array.Length - 1; j >= i; j--)
                {
                    if (array[i]%2!=0&&array[j]%2==0)
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }

            return array;
        }
    }
}
