﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E1
{
    class A01E15
    {
        static void main(string[] args)
        {
            int[] ar = {6, 2, 1, 8, 5, 80};
            int[] newar = cA01E15.method(ar);
            foreach (var i in newar)
            {
                Console.WriteLine(i);
            }
        }
    }

    class cA01E15
    {
        private int[] arr;

        public static int[] method(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = array.Length - 1; j >= i; j--)
                {
                    if (array[i]%2!=0&&array[j]%2==0)
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }

            return array;
        }
    }
}
