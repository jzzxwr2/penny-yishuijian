﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E1
{
    class A01E12
    {
        static void main(string[] args)
        {
            int[] arr = {3, 1, 10, 23, 16};
            cA01E12 c = new cA01E12();
            int[] newarr = c.method(arr);
            foreach (var i in newarr)
            {
                Console.WriteLine(i);
            }
        }
    }

    class cA01E12
    {
        private int[] array;

        public int[] method(int[] nums)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = nums.Length-1; j >= i; j--)
                {
                    if (nums[i]%2!=0&&nums[j]%2==0)
                    {
                        int temp = nums[i];
                        nums[i] = nums[j];
                        nums[j] = temp;
                    }
                }
            }

            return nums;
        }
    }
}
