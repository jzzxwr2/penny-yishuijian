﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E2
{
    /*
     * 1 排序
     * 例2 大小
     */
    class A01E20
    {
        static void main(string[] args)
        {
            int[] nums=new int[]{ 3, 7, 1, 9, 2 };
            int[] newnums = cA01E20.method(nums);
            foreach (var i in newnums)
            {
                Console.WriteLine(i);
            }
        }
    }

    class cA01E20
    {
        public static int[] method(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[i] < array[j])
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }

            return array;
        }
    }
}
