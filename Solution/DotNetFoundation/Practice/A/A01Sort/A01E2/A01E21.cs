﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.A.A01Sort.A01E2
{
    class A01E21
    {
        static int[] method(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[i]<array[j])
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }

            return array;
        }
        static void main(string[] args)
        {
            int[] nums = new int[] {5, 1, 3, 0, 2, 8, 10};
            int[] newnums = method(nums);
            foreach (var newnum in newnums)
            {
                Console.WriteLine(newnum);
            }
        }
    }

}
