﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E5
{
    class P07E52
    {
       
        static void main(string[] args)
        {
            deleP07E52 dele = delegate(int x) { return x + 1; };
            Console.WriteLine(dele(2));
        }
    }

    delegate int deleP07E52(int x);
}
