﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E5
{
    class P07E51
    {
        static void main(string[] args)
        {
            deleP07E51 dele = delegate(int x) { return x + 20; };
            Console.WriteLine(dele(1));
            Console.WriteLine(dele(6));
        }
    }

    delegate int deleP07E51(int x);
}
