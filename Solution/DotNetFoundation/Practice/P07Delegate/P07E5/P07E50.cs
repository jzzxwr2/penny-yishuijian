﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E5
{
    /*
     * 7 委托
     * 7.3 匿名方法
     * 例5
     */
    class P07E50
    {
        
        static void main(string[] args)
        {
//            deleP16E50 dele = cP16E50.add2;

            deleP07E50 dele = delegate(int x) { return x + 20;};
            Console.WriteLine(dele(5));
            Console.WriteLine(dele(6));
        }
    }

    delegate int deleP07E50(int x);
//    class cP16E50
//    {
//        public static int add2(int x)
//        {
//            return x + 20;
//        }
//    }
}
