﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E7
{
    /*
     * 7 委托
     * 7.3 匿名方法
     * 7.3.3 变量和参数的作用域
     * 3)捕获变量的生命周期的扩展
     * 例7.3
     */
    class P07E72Capture
    {
        static void main(string[] args)
        {
            deleP07E72 dele;
            {
                int x = 5;
                dele = delegate { Console.WriteLine(x); };
            }
//            Console.WriteLine(x);
            if (null!=dele)
            {
                dele();
            }
        }
    }

    delegate void deleP07E72();
}
