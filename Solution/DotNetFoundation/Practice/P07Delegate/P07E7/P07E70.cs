﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E7
{
    /*
     * 7 委托
     * 7.3 匿名方法
     * 7.3.3 变量和参数的作用域
     * 1)变量的作用域
     * 例7.1
     */
    class P07E70
    {
        static void main(string[] args)
        {
            deleP07E70 dele = delegate(int y)
            {
                int z = 10;
                Console.WriteLine("{0},{1}", y, z);
            };
//            Console.WriteLine("{0},{1}", y, z);
        }
    }

    delegate void deleP07E70(int x);
}
