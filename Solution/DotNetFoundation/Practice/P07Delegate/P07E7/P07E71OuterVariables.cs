﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E7
{
    /*
     * 7 委托
     * 7.3 匿名方法
     * 7.3.3 变量和参数的作用域
     * 2)外部变量
     * 例7.2
     */
    class P07E71OuterVariables
    {
        static void main(string[] args)
        {
            int x = 5;
            deleP07E71 dele = delegate { Console.WriteLine(x); };
            dele(1);
        }
    }

    delegate void deleP07E71(int x);
}
