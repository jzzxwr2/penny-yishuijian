﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E8
{
    /*
     * 7 委托
     * 7.4 Lambda表达式
     * 例8
     */
    class P07E80
    {

        static void main(string[] args)
        {
            deleP07E80 dele = delegate(int x) { return x + 1; };
            deleP07E80 dele2 = x => x + 1;
            Console.WriteLine(dele(2));
            Console.WriteLine(dele2(2));
        }
    }

    delegate double deleP07E80(int x);
}
