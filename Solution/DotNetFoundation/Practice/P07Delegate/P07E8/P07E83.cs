﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E8
{
    class P07E83
    {
        static void main(string[] args)
        {
            deleP07E83 dele = delegate(int x) { return x + 1; };
            deleP07E83 dele2 = x => x + 1;
            Console.WriteLine(dele(1));
            Console.WriteLine(dele2(1));
        }
    }

    delegate double deleP07E83(int x);
}
