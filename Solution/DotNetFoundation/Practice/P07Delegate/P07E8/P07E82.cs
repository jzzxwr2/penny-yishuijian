﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E8
{
    class P07E82
    {
        static void main(string[] args)
        {
            deleP07E82 dele = delegate(int x) { return x + 1; };
            deleP07E82 dele2 = x => x + 1;
            Console.WriteLine(dele(1));
            Console.WriteLine(dele2(1));
        }
    }

    delegate double deleP07E82(int x);
}
