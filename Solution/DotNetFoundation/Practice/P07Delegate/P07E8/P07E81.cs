﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E8
{
    class P07E81
    {
        static void main(string[] args)
        {
            deleP07E81 dele = delegate(int x) { return x + 1; };
            deleP07E81 dele2 = x => x + 1;
            Console.WriteLine(dele(1));
            Console.WriteLine(dele(1));
        }
    }

    delegate double deleP07E81(int x);
}
