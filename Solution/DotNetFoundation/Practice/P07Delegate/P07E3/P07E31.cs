﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E3
{
    class P07E31
    {
        static void main(string[] args)
        {
            cP07E31 c = new cP07E31();
            deleP07E31 dele = c.add10;
            dele += c.add6;
            Console.WriteLine(dele());
        }
    }

    delegate int deleP07E31();

    class cP07E31
    {
        private int num = 5;

        public int add6()
        {
            num += 6;
            return num;
        }

        public int add10()
        {
            num += 10;
            return num;
        }
    }
}
