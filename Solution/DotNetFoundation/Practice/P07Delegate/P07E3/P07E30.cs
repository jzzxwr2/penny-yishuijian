﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E3
{
    /*
     * 7 委托
     * 7.1 调用带返回值的委托
     * 例3
     */
    class P07E30
    {
        static void main(string[] args)
        {
            cP07E30 c = new cP07E30();
            deleP07E30 dele = c.add2;
            dele += c.add3;
            Console.WriteLine(dele());
        }
    }

    delegate int deleP07E30();

    class cP07E30
    {
        private int num = 5;

        public int add2()
        {
            num += 2;
            return num;
        }

        public int add3()
        {
            num += 3;
            return num;
        }
    }
}
