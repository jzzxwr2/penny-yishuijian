﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E1
{
    /*
     * 16 委托
     * 例1
     */
    class P07E10
    {
        static void main(string[] args)
        {
            cP07E10 c = new cP07E10();
            deleP07E10 dele;

            Random rand = new Random();
            int randvalue = rand.Next(99);

            dele = randvalue < 50
                ? new deleP07E10(c.methodA)
                : new deleP07E10(c.methodB);

            dele(randvalue);

        }
    }

    delegate void deleP07E10(int value);

    class cP07E10
    {
        public void methodA(int i)
        {
            Console.WriteLine("{0}-low",i);
        }

        public  void methodB(int i)
        {
            Console.WriteLine("{0}-high",i);
        }
    }
}
