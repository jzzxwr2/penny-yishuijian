﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E1
{
    class P07E11
    {
        static void main(string[] args)
        {
            cP07E11 c = new cP07E11();
            deleP07E11 dele;
            int num = Convert.ToInt32(Console.ReadLine());

            dele = num>10 ? new deleP07E11(c.methodA) : new deleP07E11(c.methodB);

            dele(num);
        }
    }

    delegate void deleP07E11(int i);

    class cP07E11
    {
        public void methodA(int i)
        {
            Console.WriteLine("大");
        }

        public void methodB(int i)
        {
            Console.WriteLine("小");
        }
    }
}
