﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E4
{
    class P07E41
    {
        static void main(string[] args)
        {
            cP07E41 c = new cP07E41();
            deleP07E41 dele = c.add2;
            int i = 1;
            dele += c.add3;
            dele(ref i);
            Console.WriteLine(i);
        }
    }

    delegate void deleP07E41(ref int x);

    class cP07E41
    {
        public void add2(ref int x)
        {
            x += 2;
        }

        public void add3(ref int x)
        {
            x += 3;
        }
    }
}
