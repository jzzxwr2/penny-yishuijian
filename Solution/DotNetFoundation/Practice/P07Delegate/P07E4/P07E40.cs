﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E4
{
    /*
     * 7 委托
     * 7.2 调用带引用参数的委托
     * 例4
     */
    class P07E40
    {
        
        static void main(string[] args)
        {
            cP07E40 c = new cP07E40();
            int num = 1;

            deleP07E40 dele = c.add2;
            dele += c.add3;

            dele(ref num);
            Console.WriteLine(num);
        }
    }

    delegate void deleP07E40(ref int x);

    class cP07E40
    {
        public void add2(ref int x)
        {
            x += 2;
        }
        public void add3(ref int x)
        {
            x += 3;
        }
    }
}
