﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E2
{
    /*
     * 7 委托
     * 例2
     */
    class P07E20
    {
        static void main(string[] args)
        {
            cP07E20 c = new cP07E20();
            deleP07E20 dele;

            dele = c.insMethod;

            dele += cP07E20.staMethod;
            dele += c.insMethod;
            dele += cP07E20.staMethod;

            if (null!=dele)
            {
                dele();
            }
            else
            {
                Console.WriteLine("empty");
            }
        }
    }

    delegate void deleP07E20();

    class cP07E20
    {
        public void insMethod()
        {
            Console.WriteLine("instance");
        }

        public static void staMethod()
        {
            Console.WriteLine("static");
        }
    }
}
