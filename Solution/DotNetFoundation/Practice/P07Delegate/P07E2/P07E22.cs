﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E2
{
    class P07E22
    {
        static void main(string[] args)
        {
            cP07E22 c = new cP07E22();
            deleP07E22 dele = c.insMehtod;
            dele += cP07E22.staMethod;
            if (null!=dele)
            {
                dele();
            }
        }
    }

    delegate void deleP07E22();

    class cP07E22
    {
        public void insMehtod()
        {
            Console.WriteLine("instance");
        }

        public static void staMethod()
        {
            Console.WriteLine("static");
        }
    }
}
