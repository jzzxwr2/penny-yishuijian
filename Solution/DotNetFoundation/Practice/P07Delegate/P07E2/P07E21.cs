﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E2
{
    class P07E21
    {
        static void main(string[] args)
        {
            cP07E21 c = new cP07E21();
            deleP07E21 dele = c.insMethod;
            dele += cP07E21.staMethod;
            dele += c.insMethod;
            dele += cP07E21.staMethod;

            if (null!=dele)
            {
                dele();
            }
            else
            {
                Console.WriteLine("empty");
            }
        }
    }

    delegate void deleP07E21();

    class cP07E21
    {
        public void insMethod()
        {
            Console.WriteLine("instance");
        }

        public static void staMethod()
        {
            Console.WriteLine("static");
        }
    }
}
