﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E6
{
    class P07E61
    {
        static void method()
        {
            Console.WriteLine("test");
        }
        static void main(string[] args)
        {
            deleP07E61 dele = delegate { method(); };
            dele(5);
        }
    }

    delegate void deleP07E61(int x);
}
