﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E6
{
    class P07E62
    {
        static void method()
        {
            Console.WriteLine("test");
        }

        static int method2()
        {
            return 1;
        }

        static int method3(int x)
        {
            return x + 1;
        }
        static void main(string[] args)
        {
//            deleP16E62 dele = delegate { method(); };
//            deleP16E62 dele = delegate { method2(); };
//            deleP16E62 dele = () => method2();
//            deleP16E62 dele = delegate { method3(2); };
//            Console.WriteLine(method2());
//            Console.WriteLine(dele());
              
        }
    }

    delegate int deleP07E62();
}
