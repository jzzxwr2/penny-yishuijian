﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E6
{
    /*
     * 7 委托
     * 7.3 匿名方法
     * 7.3.1 匿名方法的语法
     * 1)参数
     * 例6
     */
    class P07E60
    {
        static void method()
        {
            Console.WriteLine("test");
        }
        static void main(string[] args)
        {
            deleP07E60 dele = delegate{method(); };
            dele();
        }
    }

    delegate void deleP07E60();
}
