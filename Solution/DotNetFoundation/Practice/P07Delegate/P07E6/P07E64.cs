﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E6
{
    class P07E64
    {
        static void main(string[] args)
        {
            dele1P07E64 dele1 = cP07E64.method1;
            dele1();
            dele2P07E64 dele2 = cP07E64.method2;

        }
    }

    delegate void dele1P07E64();

    delegate void dele2P07E64(int x);

    delegate int dele3P07E64(int x);

    class cP07E64
    {
        public static void method1()
        {
            Console.WriteLine("test");
        }

        public static void method2(int x)
        {
            Console.WriteLine(x);
        }

        public static int method3(int x)
        {
            return x *10;
        }
    }
}
