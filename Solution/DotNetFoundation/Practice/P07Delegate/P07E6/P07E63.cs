﻿namespace DotNetFoundation.Practice.P07Delegate.P07E6
{
    class P07E63
    {
        static int method(int i)
        {
            return i + 1;
        }
        static void main(string[] args)
        {
//            deleP16E63 dele = delegate(int x) { method(3); };
        }
    }

    delegate int deleP07E63(int x);
}
