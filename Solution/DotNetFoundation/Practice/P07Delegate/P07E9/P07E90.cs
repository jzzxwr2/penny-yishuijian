﻿using System;

namespace DotNetFoundation.Practice.P07Delegate.P07E9
{
    /*
     * 7 委托
     * 7.4 Lambda表达式
     * 例9
     */
    class P07E90
    {
        static void main(string[] args)
        {
            deleP07E90 dele = nums => nums.Length;
            Console.WriteLine(dele(new int[]{1,2,3}));

            dele2P07E90 dele2 = (int x, string s) => x + s.Length;
            Console.WriteLine(dele2(1,"test"));
        }
    }

    delegate int deleP07E90(int[] nums);

    delegate int dele2P07E90(int x, string s);
}
