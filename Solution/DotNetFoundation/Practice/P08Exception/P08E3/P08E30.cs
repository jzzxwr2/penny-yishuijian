﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E3
{
    /*
     * 8 异常
     * 例3 finally块
     */
    class P08E30
    {
        static void main(string[] args)
        {
            int inVal = Convert.ToInt32(Console.ReadLine());
            try
            {
                if (inVal<10)
                {
                    Console.WriteLine("first branch");
                    return;
                }
                else
                {
                    Console.WriteLine("second branch");
                }
            }
            finally
            {
                Console.WriteLine("finally");
            }
        }
    }
}
