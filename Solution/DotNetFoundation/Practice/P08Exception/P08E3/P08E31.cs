﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E3
{
    class P08E31
    {
        static void main(string[] args)
        {
            int val = Convert.ToInt32(Console.ReadLine());
            try
            {
                if (val<5)
                {
                    Console.WriteLine("small");
                    return;
                }
                else
                {
                    Console.WriteLine("big");
                }
            }
            finally
            {
                Console.WriteLine("finally");
            }

        }
    }
}
