﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E5
{
    class P08E53
    {
        static void main(string[] args)
        {
            string arg = null;
            cP08E53.method(arg);
            cP08E53.method("hi");
        }
    }

    class cP08E53
    {
        public static void method(string str)
        {
            try
            {
                if (str==null)
                {
                    ArgumentNullException ane = new ArgumentNullException("str");
                    throw ane;
                }

                Console.WriteLine(str);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
