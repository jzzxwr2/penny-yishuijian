﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E5
{
    class P08E55
    {
        static void main(string[] args)
        {
            string s = null;
            cP08E55.method(s);
            cP08E55.method("hi");

        }
    }

    class cP08E55
    {
        public static void method(string arg)
        {
            try
            {
                if (arg==null)
                {
                    ArgumentNullException ex = new ArgumentNullException("arg");
                    throw ex;
                }

                Console.WriteLine(arg);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
