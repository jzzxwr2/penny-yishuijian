﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E5
{
    /*
     * 8 异常
     * 例5 抛出异常
     */
    class P08E50
    {
        static void main(string[] args)
        {
            string s = null;
            cP08E50.method(s);
            cP08E50.method("hi");
        }
    }

    class cP08E50
    {
        public static void method(string arg)
        {
            try
            {
                if (arg==null)
                {
                    ArgumentNullException e=new ArgumentNullException("arg");
                    throw e;
                }

                Console.WriteLine(arg);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
