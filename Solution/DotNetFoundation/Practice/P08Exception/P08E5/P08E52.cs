﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E5
{
    class P08E52
    {
        static void main(string[] args)
        {
            string s = null;
            cP08E52.method(s);
            cP08E52.method("hi");
        }
    }

    class cP08E52
    {
        public static void method(string s)
        {
            try
            {
                if (s==null)
                {
                    ArgumentNullException ex = new ArgumentNullException("s");
                    throw ex;
                }

                Console.WriteLine(s);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
