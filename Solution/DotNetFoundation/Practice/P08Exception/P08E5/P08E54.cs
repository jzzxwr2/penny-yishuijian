﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E5
{
    class P08E54
    {
        static void main(string[] args)
        {
            string str = null;
            cP08E54.method(str);
            cP08E53.method("hi");
        }
    }

    class cP08E54
    {
        public static void method(string s)
        {
            try
            {
                if (s==null)
                {
                    ArgumentNullException ex = new ArgumentNullException("s");
                    throw ex;
                }

                Console.WriteLine(s);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
