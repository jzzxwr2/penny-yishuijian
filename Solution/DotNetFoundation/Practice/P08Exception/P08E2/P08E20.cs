﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E2
{
    /*
     * 8 异常
     * 例2 catch子句
     */
    class P08E20
    {
        static void main(string[] args)
        {
            int x = 10, y = 0;
            try
            {
                x /= y;
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.Source);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
