﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E1
{
    /*
     * 8 异常
     * 例1
     */
    class P08E10
    {
        static void main(string[] args)
        {
            int x = 10, y = 0;
            try
            {
                x /= y;
            }
            catch
            {
                Console.WriteLine("keep on running");
            }
        }
    }
}
