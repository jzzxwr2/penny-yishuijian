﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E6
{
    /*
     * 8 异常
     * 例6 不带异常对象的抛出
     */
    class P08E60
    {
        static void main(string[] args)
        {
            string arg = null;
            cP08E60.method(arg);
        }
    }

    class cP08E60
    {
        public static void method(string s)
        {
            try
            {
                try
                {
                    if (s == null)
                    {
                        ArgumentNullException ex = new ArgumentNullException("s");
                        throw ex;
                    }

                    //Console.WriteLine(s);
                }
                catch(ArgumentNullException e)
                {
                    //Console.WriteLine(e.Message);
                    throw e;
                }
            }
            catch (ArgumentNullException e)
            {
                //Console.WriteLine("handle");
            }
        }
    }
}
