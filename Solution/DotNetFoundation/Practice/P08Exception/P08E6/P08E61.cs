﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E6
{
    class P08E61
    {
        static void main(string[] args)
        {
            string str = null;
            cP08E61.method(str);
        }
    }

    class cP08E61
    {
        public static void method(string s)
        {
            try
            {
                try
                {
                    if (s==null)
                    {
                        ArgumentNullException ex = new ArgumentNullException("s");
                        throw ex;
                    }

                    Console.WriteLine(s);
                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("handle");
            }
        }
    }
}
