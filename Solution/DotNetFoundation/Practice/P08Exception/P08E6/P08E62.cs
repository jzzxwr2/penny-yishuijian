﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E6
{
    class P08E62
    {
        static void main(string[] args)
        {
            string str = null;
            cP08E62.method(str);
            cP08E62.method("hi");
        }
    }

    class cP08E62
    {
        public static void method(string s)
        {
            try
            {
                try
                {
                    if (s==null)
                    {
                        ArgumentNullException ex = new ArgumentNullException("s");
                        throw ex;
                    }

                    Console.WriteLine(s);
                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("handle");
            }
        }
    }
}
