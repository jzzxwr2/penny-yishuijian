﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E4
{
    class P08E41
    {
        static void main(string[] args)
        {
            cP08E41 c = new cP08E41();
            try
            {
                c.A();
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("main");
            }
            finally{
                Console.WriteLine("fi main");
            }

            Console.WriteLine("after try");
            Console.WriteLine("keep running");
        }
    }

    class cP08E41
    {
        public void A()
        {
            try
            {
                B();
            }
            catch (System.NullReferenceException )
            {
                Console.WriteLine("A");
            }
            finally{
                Console.WriteLine("fi A");
            }
        }

        void B()
        {
            int x = 10, y = 0;
            try
            {
                x /= y;
            }
            catch (System.IndexOutOfRangeException )
            {
                Console.WriteLine("B");
            }
            finally{
                Console.WriteLine("fi B");
            }
        }
    }
}
