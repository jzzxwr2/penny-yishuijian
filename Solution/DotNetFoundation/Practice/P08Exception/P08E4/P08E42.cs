﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E4
{
    class P08E42
    {
        static void main(string[] args)
        {
            cP08E42 c = new cP08E42();
            try
            {
                c.A();
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("main");
            }
            finally{
                Console.WriteLine("fi main");
            }
        }
    }

    class cP08E42
    {
        public void A()
        {
            try
            {
                B();
            }
            catch (System.NullReferenceException )
            {
                Console.WriteLine("A");
            }
            finally{
                Console.WriteLine("fi A");
            }
        }
        void B()
        {
            int x = 10, y = 0;
            try
            {
                x /= y;
            }
            catch (System.IndexOutOfRangeException )
            {
                Console.WriteLine("B");
            }
            finally{
                Console.WriteLine("fi B");
            }
        }
    }
}
