﻿using System;

namespace DotNetFoundation.Practice.P08Exception.P08E4
{
    /*
     * 8 异常
     * 例4 搜索寻找异常处理程序
     */
    class P08E40
    {
        static void main(string[] args)
        {
            cP08E40 c = new cP08E40();
            try
            {
                c.A();
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("catch main");
            }
            finally
            {
                Console.WriteLine("finally main");
            }

            Console.WriteLine("after main");
            Console.WriteLine("keep running");

        }
    }

    class cP08E40
    {
        public void A()
        {
            try
            {
                B();
            }
            catch (System.NullReferenceException )
            {
                Console.WriteLine("catch A");
            }
            finally
            {
                Console.WriteLine("finally A");
            }

        }

        void B()
        {
            int x = 10, y = 0;
            try
            {
                x /= y;
            }
            catch (System.IndexOutOfRangeException )
            {
                Console.WriteLine("catch B");
            }
            finally
            {
                Console.WriteLine("finally B");
            }

        }
    }
}
