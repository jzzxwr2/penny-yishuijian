﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E63
    {
        static void main(string[] args)
        {
            classP04E63 c = new classP04E63();
            foreach (var item in c)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
    class classP04E63
    { 
        public IEnumerator<People> home()
        {
            yield return new People("a");
            yield return new People("b");
            yield return new People("c");
        }
        public IEnumerator<People> GetEnumerator()
        {
            return home();
        }
    }
    class People
    {
        public string Name { get; set; }
        public People(string name)
        {
            Name = name;
        }
    }
}
