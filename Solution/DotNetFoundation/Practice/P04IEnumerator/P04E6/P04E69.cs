﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E69
    {
        static void main(string[] args)
        {
            classP04E69 c = new classP04E69();
            foreach (Dog item in c)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
    class classP04E69
    {
        public IEnumerator<Dog> dogHome()
        {
            Dog[] dogarray = new Dog[]
            {
                new Dog("a"),
                new Dog("w"),
                new Dog("y"),

            };

            //Dog[] dogarray = new Dog[3];
            //dogarray[0] = new Dog("a");
            //dogarray[1] = new Dog("w");
            //dogarray[2] = new Dog("y");
            for (int i = 0; i < dogarray.Length; i++)
            {
                yield return dogarray[i];
            }
        }
        public IEnumerator<Dog> GetEnumerator()
        {
            return dogHome();
        }
    }
    class Dog
    {
        public string Name { get; set; }
        public Dog(string name)
        {
            Name = name;
        }
    }
}
