﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    /*
     * 4 枚举器、迭代器
     * 4.3 iterator迭代器  
     * 例5 使用迭代器创建枚举器
     */
    class P04E61
    {
        
        static void main(string[] args)
        {
            Class0461 c = new Class0461();
            foreach (string item in c)
            {
                Console.WriteLine(item);
            }

        }
    }
    class Class0461
    {
        public IEnumerator<string> BlackAndWhite()
        {
            yield return "black";
            yield return "gray";
            yield return "white";
        }
        public IEnumerator<string> GetEnumerator()
        {
            return BlackAndWhite();
        }
    }
}
