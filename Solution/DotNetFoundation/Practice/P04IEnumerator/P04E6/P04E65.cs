﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E65
    {
        static void main(string[] args)
        {
            classP04E65 c = new classP04E65();
            foreach (var item in c)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
    class classP04E65
    {
        public IEnumerator<Book> library()
        {
            Book[] bookArray =
            {
                new Book("a"),
                new Book("d"),
                new Book("e"),

            };
            for (int i= 0; i < bookArray.Length; i++)
            {
                yield return bookArray[i];
            }
        }
        public IEnumerator<Book> GetEnumerator()
        {
            return library();
        }
    }
    class Book
    {
        public string  Name { get; set; }
        public Book(string name)
        {
            Name = name;
        }
    }
}
