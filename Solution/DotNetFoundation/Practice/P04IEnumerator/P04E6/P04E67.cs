﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E67
    {
        static void main(string[] args)
        {
            classP04E67 c = new classP04E67();
            foreach (Car item in c)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
    class classP04E67
    {
        public IEnumerator<Car> garage()
        {
            Car[] carArray =
            {
                new Car("a"),
                new Car("x"),
                new Car("u"),
            };
            for (int i = 0; i < carArray.Length; i++)
            {
                yield return carArray[i];
            }
        }
        public IEnumerator<Car> GetEnumerator()
        {
            return garage();
        }
    }
    class Car
    {
        public string  Name { get; set; }
        public Car(string name)
        {
            Name = name;
        }
    }
}
