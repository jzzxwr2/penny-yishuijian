﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E68
    {
        static void main(string[] args)
        {
            classP04E68 c = new classP04E68();
            foreach (Cat item in c)
            {
                Console.WriteLine(item.Year);
            }
        }
    }
    class classP04E68
    { 
        public IEnumerator<Cat> catHome()
        {
            Cat[] catarray = new Cat[3];
            catarray[0] = new Cat(1);
            catarray[1] = new Cat(3);
            catarray[2] = new Cat(2);
            for (int i = 0; i < catarray.Length; i++)
            {
                yield return catarray[i];
            }
        }
        public IEnumerator<Cat> GetEnumerator()
        {
            return catHome();
        }
    }
    class Cat
    {
        public int Year { get; set; }
        public Cat(int year)
        {
            Year = year;
        }
    }
    
}
