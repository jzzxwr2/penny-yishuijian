﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    
    class P04E60
    {
        static void main(string[] args)
        {
            classp04E60 c = new classp04E60();
            foreach (string  item in c)
            {
                Console.WriteLine(item);
            }
        }
    }
    class classp04E60
    {
        public IEnumerator<string> colors()
        {
            yield return "r";
            yield return "g";
            yield return "b";
        }
        public IEnumerator<string> GetEnumerator()
        {
            return colors();
        }
    }
}
