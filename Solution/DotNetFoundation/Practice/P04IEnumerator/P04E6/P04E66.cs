﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E66
    {
        static void main(string[] args)
        {
            classP04E66 c = new classP04E66();
            foreach (var item in c)
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E66
    {
        public IEnumerator<int> nums()
        {
            int[] numArray = { 1, 2, 3 };
            for (int i = 0; i < numArray.Length; i++)
            {
                yield return numArray[i];
            }
        }
        public IEnumerator<int> GetEnumerator()
        {
            return nums();
        }
    }
}
