﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E64
    {
        static void main(string[] args)
        {
            classP04E64 c = new classP04E64();
            foreach (var item in c)
            {
                Console.WriteLine(item.Year) ;
            }
        }
    }
    class classP04E64
    {
        public IEnumerator<Phone> shop()
        {
            yield return new Phone(10);
            yield return new Phone(2);
            yield return new Phone(5);

        }
        public IEnumerator<Phone> GetEnumerator()
        {
            return shop();
        }
    }
    class Phone
    {
        public int Year { get; set; }
        public Phone(int year)
        {
            Year = year;
        }
    }
}
