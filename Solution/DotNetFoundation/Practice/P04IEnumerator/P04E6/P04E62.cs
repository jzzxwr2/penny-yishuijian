﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E6
{
    class P04E62
    {
        static void main(string[] args)
        {
            classP04E62 c = new classP04E62();
            foreach (var item in c)
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E62
    {
        public IEnumerator<int> nums()
        {
            yield return 1;
            yield return 2;
            yield return 3;
        }
        public IEnumerator<int> GetEnumerator()
        {
            return nums();
        }

    }
}
