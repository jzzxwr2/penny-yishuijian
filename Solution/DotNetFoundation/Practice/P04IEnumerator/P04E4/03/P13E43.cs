﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E4._03
{
    internal class P13E43
    {
        static void main(string[] args)
        {
            Spec spec=new Spec();
            foreach (var color in spec)
            {
                Console.WriteLine(color);
            }
        }
    }

    internal class ColorEnumerator : IEnumerator
    {
        private string[] _colors;
        private int _positon = -1;

        public ColorEnumerator(string[] theColors)
        {
            _colors = new string[theColors.Length];
            for (int i = 0; i < theColors.Length; i++)
            {
                _colors[i] = theColors[i];
            }
        }

        public object Current
        {
            get
            {
                if(_positon==-1) throw new InvalidOperationException();
                if(_positon>=_colors.Length) throw new InvalidOperationException();
                return _colors[_positon];
            }
        }

        public bool MoveNext()
        {
            if (_positon<_colors.Length-1)
            {
                _positon++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _positon = -1;
        }
    }

    internal class Spec : IEnumerable
    {
        private string[] Colors = {"r", "g", "b"};

        public IEnumerator GetEnumerator()
        {
            return new ColorEnumerator(Colors);
        }


    }
}
