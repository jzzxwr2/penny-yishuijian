﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E4._05
{
    class P13E45
    {
        static void main(string[] args)
        {
            Spec spec = new Spec();
            foreach (var color in spec)
            {
                Console.WriteLine(color);
            }

        }
    }

    class ColorEnumerator : IEnumerator
    {
        private string[] _colors;
        private int _position = -1;

        public ColorEnumerator(string[] thecColors)
        {
            _colors = new string[thecColors.Length];
            for (int i = 0; i < thecColors.Length; i++)
            {
                _colors[i] = thecColors[i];
            }
        }

        public object Current
        {
            get
            {
                if(_position==-1) throw new InvalidOperationException();
                if (_position >= _colors.Length) throw new InvalidOperationException();
                return _colors[_position];
            }
        }

        public bool MoveNext()
        {
            if (_position<_colors.Length-1)
            {
                _position++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _position = -1;
        }
    }

    class Spec : IEnumerable
    {
        private string[] Colors = {"r", "g"};

        public IEnumerator GetEnumerator()
        {
            return new ColorEnumerator(Colors);
        }
    }
}
