﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E4._06
{
    internal class P13E46
    {
        static void main(string[] args)
        {
            Spec6 spec6 = new Spec6();
            foreach (string  color in spec6)
            {
                Console.WriteLine(color);
            }
        }
    }

    class ColorEnumerator : IEnumerator
    {
        private string[] _colors;
        private int _position = -1;

        public ColorEnumerator(string[] theColors)
        {
            _colors = new string[theColors.Length];
            for (var i = 0; i < theColors.Length; i++) _colors[i] = theColors[i];
        }

        

        
        public object Current
        {
            get
            {
                if (_position == -1) throw new InvalidOperationException();
                if (_position >= _colors.Length) 
                    throw new InvalidOperationException();
                return _colors[_position];
            }
        }

        public bool MoveNext()
        {
            if (_position <_colors.Length-1)
            {
                _position++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _position = -1;
        }
    }

    class Spec6 : IEnumerable
    {
        private string[] Colors = {"r", "b"};

        public IEnumerator GetEnumerator()
        {
            return new ColorEnumerator(Colors);
        }
    }
}