﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E4._12
{
    class P13E412
    {
        static void main(string[] args)
        {
            Library library=new Library();

            IEnumerator enumerator = library.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Book book = enumerator.Current as Book;
                Console.WriteLine(book.age);
            }
            foreach (Book book in library)
            {
                Console.WriteLine(book.age);
            }
            
            

        }
    }
    class BookEnumerator : IEnumerator
    {
        private Book[] books;
        private int po = -1;

        public BookEnumerator(Book[] books)
        {
            this.books = books;
        }

        public object Current
        {
            get
            {
                if(po==-1||po>books.Length)throw new InvalidOperationException();
                return books[po];
            }
        }

        public bool MoveNext()
        {
            if(po<books.Length-1)
            {
                po++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            po = -1;
        }
    }
    class Library : IEnumerable
    {
        Book[] books=new Book[3];

        public Library()
        {
            books[0]=new Book(10);
            books[1]=new Book(20);
            books[2]=new Book(30);
        }

        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();
        }
    }

    class Book
    {
        public int age;

        public Book(int age)
        {
            this.age = age;
        }
    }
}
