﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E4._11
{
    class P13E411
    {
        static void main(string[] args)
        {
            ColorGarage colors=new ColorGarage();

            /*IEnumerator enumerator = colors.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string color = enumerator.Current as string;
                Console.WriteLine(color);
            }*/
            foreach (string color in colors)
            {
                Console.WriteLine(color);
            }


        }
    }
    class ColorEnumerator : IEnumerator
    {
        private string[] _colors;
        private int po = -1;

        public ColorEnumerator(string[] theColors)
        {
            _colors = new string[theColors.Length];
            for (int i = 0; i < theColors.Length; i++)
            {
                _colors[i] = theColors[i];

            }
        }

        public object Current
        {
            get
            {
                if (po == -1) throw new InvalidOperationException();
                if(po >= _colors.Length) throw new InvalidOperationException();
                return _colors[po];
            }
        }

        public bool MoveNext()
        {
            if (po < _colors.Length - 1)
            {
                po++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            po = -1;
        }
    }

    class ColorGarage : IEnumerable
    {
        private string[] colors = {"r", "g", "b"};
        public IEnumerator GetEnumerator()
        {
            return new ColorEnumerator(colors);
        }
    }
}
