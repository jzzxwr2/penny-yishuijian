﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E4._02
{
    internal class P13E42
    {
        static void main(string[] args)
        {
            Spec spec=new Spec();
            foreach (string color in spec)
            {
                Console.WriteLine(color);
            }
        }
    }

    internal class ColorEnumerator : IEnumerator
    {
        string[] _colors;
        private int _position = -1;

        public ColorEnumerator(string[] theColors)
        {
            _colors = new string[theColors.Length];
            for (int i = 0; i < theColors.Length; i++)
            {
                _colors[i] = theColors[i];
            }
        }

        public object Current
        {
            get
            {
                if(_position==-1) throw new InvalidOperationException();
                if (_position >= _colors.Length) throw new InvalidOperationException();
                return _colors[_position];
            }
        }

        public bool MoveNext()
        {
            if (_position<_colors.Length-1)
            {
                _position++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _position = -1;
        }

    }

    internal class Spec : IEnumerable
    {
        private string[] Colors = {"r", "g", "b", "y", "o", "p"};

        public IEnumerator GetEnumerator()
        {
            return new ColorEnumerator(Colors);
        }
    }
}

namespace dotnetTest
{

}