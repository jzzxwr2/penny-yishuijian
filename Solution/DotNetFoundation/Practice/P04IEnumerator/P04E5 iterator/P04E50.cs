﻿using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E5_iterator
{
    /*
     * 4 枚举器、迭代器
     * 4.3 iterator迭代器  
     * 相同结果展示
     */
    class P04E50
    {
        public IEnumerator<string> BlackAndWhite()
        {
            yield return "black";
            yield return "gray";
            yield return "white";
        }
        public IEnumerator<string> BlackAndWhite2()
        {
            string[] theColors = { "black", "grey", "white" };
            for (int i = 0; i < theColors.Length; i++)
            {
                yield return theColors[i];
            }
        }
        static void main(string[] args)
        {

        }
    }
    

}
