﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E3._09
{
    class P13E39
    {
        static void main(string[] args)
        {
            Phone[] phones =
            {
                new Phone(9),
                new Phone(1),
                new Phone(2),
            };
            PhoneEnumerator enumerator = new PhoneEnumerator(phones);
            while (enumerator.MoveNext())
            {
                Phone phone = enumerator.Current as Phone;
                Console.WriteLine(phone.age);
            }
        }
    }
    class PhoneEnumerator : IEnumerator
    {
        private Phone[] phones;
        private int position = -1;

        public PhoneEnumerator(Phone[] phones)
        {
            this.phones = phones;
        }

        public object Current
        {
            get
            {
                if(position==-1) return new IndexOutOfRangeException();
                return phones[position];
            }
        }

        public bool MoveNext()
        {
            position++;
            return position < phones.Length;
        }

        public void Reset()
        {
            position = -1;
        }
    }

    class Phone
    {
        public int age;

        public Phone(int age)
        {
            this.age = age;
        }
    }
}
