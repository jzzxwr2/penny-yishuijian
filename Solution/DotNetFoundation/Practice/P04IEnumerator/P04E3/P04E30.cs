﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E3
{
    /*
     * 4 枚举器、迭代器
     * 4.2 例3 IEnumerator接口
     */
    class P04E30
    {
        private static void main(string[] args)
        {
            Phone[] phones =
            {
                new Phone("iphone"),
                new Phone("huawei"),
                new Phone("xiaomi")
            };
            PhoneEnumerator enumerator = new PhoneEnumerator(phones);
            while (enumerator.MoveNext())
            {
                var phone = enumerator.Current as Phone;
                Console.WriteLine(phone._name);
            }

            
        }
    }

    public class PhoneEnumerator : IEnumerator
    {
        private Phone[] _phones;
        private int _position = -1;

        public PhoneEnumerator(Phone[] phones)
        {
            _phones = phones;
        }

        public object Current
        {
            get
            {
                if (_position == -1) return new IndexOutOfRangeException();
                return _phones[_position];
            }
        }

        public bool MoveNext()
        {
            _position++;
            return _position < _phones.Length;
        }

        public void Reset()
        {
            _position = -1;
        }
    }

    public class Phone
    {
        public string _name;

        public Phone(string name)
        {
            _name = name;
        }
    }
}