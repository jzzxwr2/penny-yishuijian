﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P04IEnumerator.P13E3._02
{
    class P13E32
    {
        static void main(string[] args)
        {
            Phone[] phones =
            {
                new Phone("p1"),
                new Phone("p2"),
                new Phone("p3"),
            };
            PhoneEnumerator enumerator = new PhoneEnumerator(phones);
            while (enumerator.MoveNext())
            {
                Phone phone = enumerator.Current as Phone;
                Console.WriteLine(phone._name);
            }
        }
    }
    class PhoneEnumerator : IEnumerator
    {
        private Phone[] phones;
        private int position = -1;

        public PhoneEnumerator(Phone[] phones)
        {
            this.phones = phones;
        }

        public object Current
        {
            get
            {
                if (position == -1)
                {

                    return new IndexOutOfRangeException();
                }

                return phones[position];
            }
        }

        public bool MoveNext()
        {
            position++;
            return position < phones.Length;
        }

        public void Reset()
        {
            position = -1;
        }
    }

    class Phone
    {
        public string _name;

        public Phone(string name)
        {
            _name = name;
        }
    }
}
