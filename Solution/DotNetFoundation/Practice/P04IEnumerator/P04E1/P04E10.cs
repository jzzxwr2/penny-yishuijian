﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E1
{
    /*
     * 4 枚举器、迭代器
     * 4.1 例1 手动实现foreach语句自动做的事：
     *          可枚举类型——数组
     */
    class P04E10
    {
        static void main(string[] args)
        {
            int[] MyArray = {1, 2, 3, 4, 5};

            IEnumerator ie = MyArray.GetEnumerator();
            while (ie.MoveNext())
            {
                int i = (int) ie.Current;
                //var i = ie.Current;
                Console.WriteLine(i);
            }

            foreach (var i in MyArray)
            {
                Console.WriteLine(i);
            }
        }
    }
}
