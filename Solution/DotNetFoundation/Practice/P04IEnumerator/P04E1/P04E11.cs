﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E1
{
    class P04E11
    {
        static void main(string[] args)
        {
            int[] array = {1, 2, 3};

            IEnumerator ie = array.GetEnumerator();
            while (ie.MoveNext())
            {
                int i = (int) ie.Current;
                Console.WriteLine(i);
            }

            foreach (int i in array)
            {
                Console.WriteLine(i);
            }
        }
    }
}
