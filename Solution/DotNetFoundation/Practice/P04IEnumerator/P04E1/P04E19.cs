﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E1
{
    class P04E19
    {
        static void main(string[] args)
        {
            int[] array = {1, 2, 3};
            IEnumerator iEnumerator = array.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                int i = (int) iEnumerator.Current;
                Console.WriteLine(i);
            }
        }
    }
}
