﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E1
{
    class P04E12
    {
        static void main(string[] args)
        {
            int[] array = {1, 2, 3};

            IEnumerator ieEnumerator = array.GetEnumerator();

            while (ieEnumerator.MoveNext())
            {
                int i = (int) ieEnumerator.Current;
                Console.WriteLine(i);
            }
        }
    }
}
