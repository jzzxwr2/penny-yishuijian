﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E8
{
    /*
     * 4 枚举器、迭代器
     * 4.4  产生多个可枚举类型
     * 例7 
     */
    class P04E8
    {
        static void main(string[] args)
        {
            classP04E8 c = new classP04E8();
            foreach (int item in c.oTof())
            {
                Console.WriteLine(item);
            }
            foreach (int item in c.fToo())
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E8
    {
        int[] nums = { 1, 2, 3, 4, 5 };

        public IEnumerable<int> oTof()
        {
            for (int i = 0; i < nums.Length; i++)
            {
                yield return nums[i];
            }
        }
        public IEnumerable<int> fToo()
        {
            for (int i = nums.Length-1; i >=0; i--)
            {
                yield return nums[i];
            }
        }
    }
}
