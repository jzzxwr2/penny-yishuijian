﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E22
    {
        static void main(string[] args)
        {
            CarGarage carGarage=new CarGarage();

            IEnumerator iEnumerator = carGarage.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Car car = (Car) iEnumerator.Current;
                Console.WriteLine("{0},{1}",car.Color,car.Year);
            }
        }
    }
    public class CarGarage : IEnumerable
    {
        public Car[] carArray = new Car[3];

        public CarGarage()
        {
            carArray[0]=new Car("g",30);
            carArray[1]=new Car("b",10);
            carArray[2]=new Car("o",20);
        }

        public IEnumerator GetEnumerator()
        {
            return carArray.GetEnumerator();
        }
    }

    public class Car
    {
        public string Color { get; set; }
        public int Year { get; set; }

        public Car(string color, int year)
        {
            Color = color;
            Year = year;
        }
    }
}
