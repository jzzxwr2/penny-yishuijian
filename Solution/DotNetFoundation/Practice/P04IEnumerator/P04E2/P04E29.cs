﻿/*
using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E29
    {
        static void main(string[] args)
        {
            Libraty libraty=new Libraty();
            IEnumerator iEnumerator = libraty.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Book book = (Book) iEnumerator.Current;
                Console.WriteLine("{0},{1}",book.Name,book.Year);
            }
        }
    }
    class Libraty : IEnumerable
    {
        Book[] books = new Book[3];

        public Libraty()
        {
            books[0]=new Book("g",30);
            books[1]=new Book("g",10);
            books[2]=new Book("g",90);
        }

        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();
        }
    }

    class Book
    {
        public string Name { get; set; }
        public int Year { get; set; }

        public Book(string name, int year)
        {
            Name = name;
            Year = year;
        }
    }
}
*/
