﻿/*
using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E27
    {
        static void main(string[] args)
        {
            Library library=new Library();
            IEnumerator iEnumerator = library.GetEnumerator();

            while (iEnumerator.MoveNext())
            {
                Book book = (Book) iEnumerator.Current;
                Console.WriteLine("{0},{1}",book.Name,book.Year);
            }
        }
    }
    class Library : IEnumerable
    {
        Book[] books = new Book[3];

        public Library()
        {
            books[0]=new Book("b1",30);
            books[1]=new Book("b2",10);
            books[2]=new Book("b3",50);
        }

        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();
        }
    }

    class Book
    {
        public Book(string name, int year)
        {
            Name = name;
            Year = year;
        }

        public string Name { get; set; }
        public int Year { get; set; }
    }
}
*/
