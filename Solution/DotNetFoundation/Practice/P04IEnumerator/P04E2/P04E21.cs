﻿/*
using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E21
    {
        static void main(string[] args)
        {
            Garage carGarage = new Garage();
            IEnumerator iEnumerator = carGarage.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Car car = (Car) iEnumerator.Current;
                Console.WriteLine("{0},{1}",car.Color,car.Year);
            }
            /*foreach (Car car in carGarage)
            {
                Console.WriteLine("{0},{1}",car.Color,car.Year);
            }#1#

        }
    }

    public class cGarage : IEnumerable
    {
        public Car[] carArray = new Car[2];

        public cGarage()
        {
            carArray[0]=new Car("r",20);
            carArray[1]=new Car("g",10);
        }

        public IEnumerator GetEnumerator()
        {
            return carArray.GetEnumerator();
        }
    }
    public class cCar
    {
        public string  Color { get; set; }
        public int Year { get; set; }

        public cCar(string color, int year)
        {
            Color = color;
            Year = year;
        }
    }
}
*/
