﻿/*
using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E23
    {
        static void main(string[] args)
        {
            CarGarage carGarage=new CarGarage();

            IEnumerator iEnumerator = carGarage.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Car car = (Car)iEnumerator.Current;
                Console.WriteLine("{0},{1}", car.Color, car.Year);
            }
        }
    }

    public class CarGarage : IEnumerable
    {
        public Car[] carArray = new Car[5];

        public CarGarage()
        {
            carArray[0]=new Car("y",50);
            carArray[1]=new Car("o",60);
            carArray[2]=new Car("g",10);
            carArray[3]=new Car("r",90);
            carArray[4]=new Car("b",30);
        }

        public IEnumerator GetEnumerator()
        {
            return carArray.GetEnumerator();
        }
    }
    public class Car
    {
        public string  Color { get; set; }
        public int Year { get; set; }

        public Car(string color, int year)
        {
            Color = color;
            Year = year;
        }
    }
}
*/
