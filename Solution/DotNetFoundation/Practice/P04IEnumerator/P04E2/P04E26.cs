﻿/*
using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E26
    {
        static void main(string[] args)
        {
            Libraty libraty = new Libraty();
            IEnumerator iEnumerator = libraty.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Book book = (Book)iEnumerator.Current;
                Console.WriteLine("{0},{1}",book.Name,book.Year);
            }
        }
    }
    class Libraty : IEnumerable
    {
        Book[] bookArray = new Book[5];

        public Libraty()
        {
            bookArray[0]=new Book("b1",50);
            bookArray[1]=new Book("b2",520);
            bookArray[2]=new Book("b3",80);
            bookArray[3]=new Book("b4",510);
            bookArray[4]=new Book("b5",70);
        }

        public IEnumerator GetEnumerator()
        {
            return bookArray.GetEnumerator();
        }
    }

    class Book
    {
        public string  Name { get; set; }
        public int Year { get; set; }

        public Book(string name, int year)
        {
            Name = name;
            Year = year;
        }
    }
}
*/
