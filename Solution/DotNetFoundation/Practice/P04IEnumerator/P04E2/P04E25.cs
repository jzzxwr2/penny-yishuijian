﻿using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E25
    {
        static void main(string[] args)
        {
            Library library=new Library();
            IEnumerator iEnumerator = library.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Book book = (Book) iEnumerator.Current;
                Console.WriteLine("{0},{1}",book.Name,book.Year);
            }
        }
    }
    public class Library : IEnumerable
    {
        public Book[] bookArray = new Book[3];

        public Library()
        {
            bookArray[0]=new Book("book1",50);
            bookArray[1]=new Book("book2",750);
            bookArray[2]=new Book("book3",10);
        }

        public IEnumerator GetEnumerator()
        {
            return bookArray.GetEnumerator();
        }
    }

    public class Book
    {
        public Book(string name, int year)
        {
            Name = name;
            Year = year;
        }

        public string  Name { get; set; }
        public int Year { get; set; }
    }
}
