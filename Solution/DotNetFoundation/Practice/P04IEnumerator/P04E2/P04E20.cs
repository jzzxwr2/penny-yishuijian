﻿/*using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    /*
     * 4 枚举器、迭代器
     * 4.1 例2 IEnumerable接口
     *          可枚举类的实现
     #1#
    class P04E20
    {
        private static void main(string[] args)
        {
            var carLot = new Garage();

            var i = carLot.GetEnumerator();
            while (i.MoveNext())
            {
                var myCar = (Car) i.Current;
                Console.WriteLine("{0},{1}", myCar.Color, myCar.Year);
            }


            foreach (Car item in carLot) Console.WriteLine("{0},{1}", item.Color, item.Year);
        }
    }


    public class Garage : IEnumerable
    {
        public Car[] carArray = new Car[3];

        public Garage()
        {
            carArray[0] = new Car("red", 10);
            carArray[1] = new Car("green", 50);
            carArray[2] = new Car("orange", 20);
        }

        public IEnumerator GetEnumerator()
        {
            return carArray.GetEnumerator();
        }
    }

    public class cCar
    {
        public cCar(string color, int year)
        {
            Color = color;
            Year = year;
        }

        public string Color { get; set; }
        public int Year { get; set; }
    }
}*/