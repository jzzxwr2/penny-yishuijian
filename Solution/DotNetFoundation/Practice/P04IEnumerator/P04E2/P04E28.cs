﻿/*
using System;
using System.Collections;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E2
{
    class P04E28
    {
        static void main(string[] args)
        {
            Library library=new Library();
            IEnumerator iEnumerator = library.GetEnumerator();
            while (iEnumerator.MoveNext())
            {
                Book book = (Book) iEnumerator.Current;
                Console.WriteLine("{0},{1}",book.Year,book.Name);
            }
        }
    }
    class Library : IEnumerable
    {
        Book[] books = new Book[2];

        public Library()
        {
            books[0]=new Book("b1",60);
            books[1]=new Book("b2",20);
        }

        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();
        }
    }

    class Book
    {
        public string Name { get; set; }
        public int Year { get; set; }

        public Book(string name, int year)
        {
            Name = name;
            Year = year;
        }
    }
}
*/
