﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E9
{
    class P04E92
    {
        static void main(string[] args)
        {
            classP04E92 c = new classP04E92(true);
            classP04E92 c2 = new classP04E92(false);
            foreach (int item in c)
            {
                Console.WriteLine(item);
            }
            foreach (int item in c2)
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E92
    {
        bool _list5to1;
        int[] nums = { 1, 2, 3, 4, 5 };
        public classP04E92(bool list5to1)
        {
            _list5to1 = list5to1;
        }
        public IEnumerator<int> oTof
        {
            get
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> fToo
        { 
            get
            {
                for (int i = nums.Length-1; i >= 0; i--)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> GetEnumerator()
        {
            return _list5to1 ? fToo : oTof;
        }
    }
}
