﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E9
{
    /*
     * 4 枚举器、迭代器
     * 4.5  将迭代器作为属性
     * 例8 
     */
    class P04E91
    {
        static void main(string[] args)
        {
            classP04E91 c15 = new classP04E91(true);
            classP04E91 c51 = new classP04E91(false);
            foreach (int item in c15)
            {
                Console.WriteLine(item);
            }
            foreach (int item in c51)
            {
                Console.WriteLine(item);
            }

        }
    }
    class classP04E91
    {
        bool _listoTof;
        int[] nums = { 1, 2, 3, 4, 5 };
        public classP04E91(bool listoTof)
        {
            _listoTof = listoTof;
        }
        public IEnumerator<int> OToF
        {
            get
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> FToO
        {
            get
            {
                for (int i = nums.Length-1; i>=0; i--)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> GetEnumerator()
        {
            return _listoTof ? OToF : FToO;
        }

    }
}
