﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E9
{
    class P04E95
    {
        static void main(string[] args)
        {
            classP04E95 c = new classP04E95(false);
            classP04E95 c2 = new classP04E95(true);
            foreach (int item in c)
            {
                Console.WriteLine(item);
            }
            foreach (int item in c2)
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E95
    {
        bool _list;
        int[] nums = { 1, 2, 3, 4, 5 };
        public classP04E95(bool list)
        {
            _list = list;
        }
        public IEnumerator<int> OToF
        {
            get
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> FToO
        {
            get
            {
                for (int i = nums.Length-1; i >=0; i--)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> GetEnumerator()
        {
            return _list ? OToF : FToO;
        }
    }
}
