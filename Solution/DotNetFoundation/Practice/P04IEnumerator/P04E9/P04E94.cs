﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E9
{
    class P04E94
    {
        static void main(string[] args)
        {
            classP04E94 c = new classP04E94(true);
            classP04E94 c2 = new classP04E94(false);
            foreach (int item in c2)
            {
                Console.WriteLine(item);
            }
            foreach (int item in c)
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E94
    {
        bool _list;
        int[] nums = { 1, 2, 3, 4, 5 };
        public classP04E94(bool list)
        {
            _list = list;
        }
        public IEnumerator<int> OToF
        {
            get
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> FToO
        {
            get
            {
                for (int i = nums.Length-1; i >=0; i--)
                {
                    yield return nums[i];
                }
            }
        }
        public IEnumerator<int> GetEnumerator()
        {
            return _list ? OToF : FToO;
        }
    }
}
