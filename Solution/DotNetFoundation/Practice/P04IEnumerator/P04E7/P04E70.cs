﻿using System;
using System.Collections.Generic;

namespace DotNetFoundation.Practice.P04IEnumerator.P04E7
{
    /*
     * 4 枚举器、迭代器
     * 4.3 iterator迭代器  
     * 例6 使用迭代器创建可枚举类型
     */
    class P04E70
    {
        static void main(string[] args)
        {
            classP04E70 c = new classP04E70();

            foreach (string  item in c)
            {
                Console.WriteLine(item);
            }

            foreach (string item in c.BlackAndWhite())
            {
                Console.WriteLine(item);
            }
        }
    }
    class classP04E70
    {
        public IEnumerable<string> BlackAndWhite()
        {
            yield return "b";
            yield return "g";
            yield return "w";
        }
        public IEnumerator<string> GetEnumerator()
        {
            IEnumerable<string> myenu = BlackAndWhite();
            return myenu.GetEnumerator();
        }
    }
}
