﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02E1
{
    /*
    * 2 类
    * 2.1访问修饰符
    */
    class P02E10
    {
        static void main(string[] args)
        {
            test3cP02E10 t = new test3cP02E10();
            t.publicmethod();
        }
    }

    public class cP02E10
    {
        public string GetPublicString()
        {
            return "public string";
        }

        private string GetPrivateString()
        {
            return "private string";
        }

        protected string GetProtectedString()
        {
            return "protected string";
        }

        internal string GetInternalString()
        {
            return "internal string";
        }

        protected internal string GetProtectedInternalString()
        {
            return "protected internal string";
        }

        void AvailableAccessModifier()
        {
            this.GetPublicString();
            this.GetPrivateString();
            this.GetInternalString();
            this.GetProtectedInternalString();
            this.GetProtectedString();

        }

        
    }
    class test1cP02E10
    {
        void method()
        {
            cP02E10 c = new cP02E10();
            c.GetPublicString();
            c.GetInternalString();
            c.GetProtectedInternalString();
        }
    }

    public class test2cP02E10 : cP02E10
    {
        void method()
        {
            cP02E10 c = new cP02E10();
            c.GetPublicString();
            c.GetInternalString();
            c.GetProtectedInternalString();
            base.GetProtectedString();
            

        }
    }

    class test3cP02E10
    {
        private void privatemethod()
        {
            Console.WriteLine("private");
        }
        public void publicmethod()
        {
            Console.WriteLine("public");
        }
    }
}
