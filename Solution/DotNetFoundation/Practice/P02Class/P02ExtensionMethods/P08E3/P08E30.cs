﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02ExtensionMethods.P08E3
{
    /*
     * 8 扩展方法
     * 例3
     */
    class P08E30
    {
        static void main(string[] args)
        {
            string str = "abc";
            str = str.AddNewLine();
            Console.WriteLine(str+"efg");
        }
    }
    public static class ExtString
    {
        public static string AddNewLine(this string str)
        {
            return str + Environment.NewLine;
        }
    }
}
