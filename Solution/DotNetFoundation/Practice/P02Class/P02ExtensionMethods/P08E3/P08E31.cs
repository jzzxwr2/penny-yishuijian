﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02ExtensionMethods.P08E3
{
    class P08E31
    {
        static void main(string[] args)
        {
            string str = "abc";
            str = str.AddNewLine2();
            Console.WriteLine(str+1);
        }
    }
    public static class ExString0831
    {
        public static string AddNewLine2(this string str)
        {
            return str + Environment.NewLine;
        }
    }
}
