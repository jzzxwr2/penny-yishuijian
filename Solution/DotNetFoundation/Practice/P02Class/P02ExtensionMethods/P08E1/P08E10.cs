﻿using System;

namespace DotNetFoundation.Practice.P02Class.P02ExtensionMethods.P08E1
{
    /*
    * 8 扩展方法
    * 例1
    */

    /*
     * 扩展的意义：为某一类型做某个功能的扩展，即补丁
     * 位置：程序集的全局区、顶级类中。不能写在嵌套类中
     * 三要素：静态类，静态方法，this修饰符
     * this后面是需要调用扩展方法的类型，需要参数则在后面继续添加参数
     */
    class P08
    {
        /*public*/ static void main(string[] args)
        {
            string str = "a";
            Console.WriteLine(str.AddLetterA());//扩展方法可由定义扩展那个类的对象直接调用

            var testInstance = new MyClassP82();
            Console.WriteLine(testInstance.AddLetterB("a"));//普通方法只能在本类的对象调用

            testInstance.SayHello(); //扩展方法可由定义扩展那个类的对象直接调用


        }
    }
    public static class MyClassP8
    {
        public static string AddLetterA(this string str)
        {
            return str + "A";
        }
        public static void SayHello(this MyClassP82 c)
        {
            Console.WriteLine("hello");
        }
    }
    
    public class MyClassP82
    {
        public string AddLetterB(string str)
        {
            return str + "B";
        }
    }

}
