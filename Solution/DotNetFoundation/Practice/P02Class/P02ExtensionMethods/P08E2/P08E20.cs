﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02ExtensionMethods.P08E2
{
    /*
     * 8 扩展方法
     * 例2
     */
    class P08E20
    {
        static void main(string[] args)
        {
            classP08E20 c = new classP08E20(3, 4, 5);
            Console.WriteLine(ExtendclassP08E20.Average(c));

            Console.WriteLine(c.Sum());
            Console.WriteLine(c.Average());
        }
    }
    static class ExtendclassP08E20
    {
        public static double Average(classP08E20 c)
        {
            return c.Sum() / 3;
        }
    }
    static class Extend2classP08E20
    {
        public static double Average(this classP08E20 c)
        {
            return c.Sum() / 3;
        }
    }
    class classP08E20
    {
        double D1;
        double D2;
        double D3;
        public classP08E20(double d1,double d2,double d3)
        {
            D1 = d1;D2 = d2;D3 = d3;
        }
        public double Sum()
        {
            return D1 + D2 + D3;
        }
    }
}
