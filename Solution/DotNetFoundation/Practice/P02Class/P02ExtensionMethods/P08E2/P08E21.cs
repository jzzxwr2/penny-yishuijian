﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P03Interface.P02Class.P02ExtensionMethods.P08E2
{
    class P08E21
    {
        static void main(string[] args)
        {
            classP08E21 c = new classP08E21(2, 4, 6);
            Console.WriteLine(c.Sum());
            Console.WriteLine(c.Average());
        }
    }
    static class ExtendClassP08E21
    {
        public static double Average(this classP08E21 c)
        {
            return c.Sum() / 3;
        }
    }
    class classP08E21
    {
        double D1, D2, D3;
        public classP08E21(double d1,double d2,double d3)
        {
            D1 = d1;D2 = d2;D3 = d3;
        }
        public double Sum()
        {
            return D1 + D2 + D3;
        }
    }
}
