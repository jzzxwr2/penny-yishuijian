﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02ExtensionMethods.P08E4
{
    class P08E40
    {
        static void main(string[] args)
        {
            Student student = new Student();
            string info = student.ExStuInfo("a", 1);
            string info2 = student.GetStuInfo("b", 2);
            Console.WriteLine(info);
            Console.WriteLine(info2);
        }
    }
    public static class ExtStu
    {
        public static string ExStuInfo(this Student student, string name,int num)
        {
            return student.GetStuInfo(name, num);
        }
    }
    public class Student
    {
        public string StuInfo()
        {
            return "学生基本信息";
        }
        public string GetStuInfo(string stuName,int stuNum)
        {
            return string.Format("{0},{1}", stuName, stuNum);
        }
    }
}
