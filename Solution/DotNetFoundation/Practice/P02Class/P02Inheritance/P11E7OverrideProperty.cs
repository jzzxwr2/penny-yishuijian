﻿using System;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.4 使用基类的引用
    * 11.4.4 覆盖属性
    */
    internal class P11E7
    {
        private static void main(string[] args)
        {
            var derived = new MDC();
            var mbc = (MBC) derived;

            Console.WriteLine(derived.Age);
            Console.WriteLine(mbc.Age);
        }
    }

    internal class MBC
    {
        public virtual int Age { get; } = 6;
    }

    internal class MDC : MBC
    {
        public override int Age { get; } = 10;
    }
}