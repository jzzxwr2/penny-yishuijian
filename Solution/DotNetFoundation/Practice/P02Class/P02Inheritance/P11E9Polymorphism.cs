﻿using System;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.5 虚方法实现多态性
    * 11.5.2 多态性实例
    */
    internal class P11E9
    {
        private static void main(string[] args)
        {
            var person = new Person("小红", 18);
            var employee = new Employee("Jim Green", 30, "财务部", 6000);
            Person.DisplayData(person);
//            Person.DisplayData(employee);
        }
    }

    public class Person
    {
        private int _age = 20;
        private string _name = "小明";

        public Person(string Name, int Age)
        {
            _name = Name;
            _age = Age;
        }

        protected virtual void Display()
        {
            Console.WriteLine("{0},{1}", _name, _age);
        }

        public static void DisplayData(Person p)
        {
            p.Display();
        }
    }

    public class Employee : Person
    {
        private string _department;
        private decimal _salary;

        public Employee(string Name, int Age, string Department, decimal Salary) : base(Name, Age)
        {
            _department = Department;
            _salary = Salary;
        }

        protected override void Display()
        {
            base.Display();
            Console.WriteLine("{0},{1}", _department, _salary);
        }
    }
}