﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.4 使用基类的引用
    * 11.4.1 
    */
    class P11E4
    {
        static void main(string[] args)
        {
            var derived = new MyDerivedClass();
            var mybc = (MyBaseClass)derived;

            derived.Print();
            mybc.Print();
        }
    }
    class MyBaseClass
    {
        public void Print()
        {
            Console.WriteLine("base class");
        }
    }
    class MyDerivedClass:MyBaseClass
    {
        new public void Print()
        {
            Console.WriteLine("derived class");
        }
    }
}
