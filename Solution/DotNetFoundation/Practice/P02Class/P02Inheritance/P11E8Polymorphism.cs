﻿using System;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.5 虚方法实现多态性
    * 11.5.1 
    */
    internal class P11E8
    {
        private static void Display2(Base b)
        {
            b.Display();
        }

        private static void main(string[] args)
        {
            var derived = new Deri();
            var mybc = new Base();
            Base mybc2 = derived;
            mybc.Print();
            mybc2.Print();
            derived.Print();
            mybc.Display();
            mybc2.Display();
            Display2(derived);
            Display2(mybc);
        }
    }

    internal class Base
    {
        public void Print()
        {
            Console.WriteLine("base not virtual");
        }

        public virtual void Display()
        {
            Console.WriteLine("base virtual");
        }
    }

    internal class Deri : Base
    {
        public new void Print()
        {
            Console.WriteLine("derived not virtual");
        }

        public override void Display()
        {
            Console.WriteLine("derived override");
        }
    }
}