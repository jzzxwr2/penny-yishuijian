﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 11 继承
    * 11.1 访问继承的成员
    */
    class P11E1
    {
        static void main(string[] args)
        {
            var te = new DerivedClass();
            te.Method1(te.Field1);
            te.Method1(te.Field2);
            te.Method2(te.Field1);
            te.Method2(te.Field2);
        }
    }
    class BaseClass
    {
        public string Field1 = "base class field1";
        public void Method1(string s) { Console.WriteLine("basic class Method1 :{0}",s); }
    }
    class DerivedClass:BaseClass
    {
        public string Field2 = "derived class field2";
        public void Method2(string s) { Console.WriteLine("derived class Method2 :{0}", s); }
    }
}
