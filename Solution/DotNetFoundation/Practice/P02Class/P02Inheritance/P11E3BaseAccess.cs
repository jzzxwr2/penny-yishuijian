﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.3 基类访问
    */
    class P11E3
    {
        static void main(string[] args)
        {
            var te = new DeClass();
            te.PrintField1();
        }
    }
    class BaClass
    {
        public string Field1 = "Field1 in the BaClass";
    }
    class DeClass:BaClass
    {
        public new string Field1 = "Field1 in the DeClass";
        public void PrintField1() 
        { 
            Console.WriteLine(Field1);
            Console.WriteLine(base.Field1);
        }
    }
}
