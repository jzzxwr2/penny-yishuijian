﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.2 屏蔽基类成员
    */
    class P11E2
    {
        static void main(string[] args)
        {
            var te = new Class2();
            te.Method1(te.Field1);
        }
    }
    class Class1
    {
        public string Field1 = "class1 field1";
        public void Method1(string s) 
        { 
            Console.WriteLine("class1 Method1 :{0}", s); 
        }
    }
    class Class2 : Class1
    {
        new public string Field1 = "class2 field1";
        new public void Method1(string s) 
        { 
            Console.WriteLine("class2 Method1 :{0}", s);
        }
    }
}
