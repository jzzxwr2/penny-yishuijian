﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable All

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.4 使用基类的引用
    * 11.4.3 虚方法、覆写方法
    */
    class P11E6
    {
        static void main(string[] args)
        {
            //var secderived = new SecondDerived();
            var secderived = new SecondDerived2();
            var pbc = (PBaseClass)secderived;

            secderived.Print();
            pbc.Print();
        }
    }
    class PBaseClass
    {
        public virtual void Print()
        {
            Console.WriteLine("P base class");
        }
    }
    class PDerivedClass:PBaseClass
    {
        public override void Print()
        {
            Console.WriteLine("P derived class");
        }
    }
    class SecondDerived:PDerivedClass
    {
        public override void Print()
        {
            Console.WriteLine("second derived class");
        }
    }
    class SecondDerived2 : PDerivedClass
    {
        public new void Print()
        {
            Console.WriteLine("second derived class 2");
        }
    }
}
