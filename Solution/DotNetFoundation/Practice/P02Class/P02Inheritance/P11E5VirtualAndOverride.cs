﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P02Class.P02Inheritance
{
    /*
    * 练习11：继承
    * 11.4 使用基类的引用
    * 11.4.2 虚方法、覆写方法
    */
    class P11E5
    {
        static void main(string[] args)
        {
            var derived = new MDerivedClass();
            var mybc = (MBaseClass)derived;

            derived.Print();
            mybc.Print();
        }
    }
    class MBaseClass
    { 
        virtual public void Print()
        {
            Console.WriteLine("base class");
        }
    }
    class MDerivedClass:MBaseClass
    {
        override public void Print()
        {
            Console.WriteLine("derived class");
        }
    }
}
