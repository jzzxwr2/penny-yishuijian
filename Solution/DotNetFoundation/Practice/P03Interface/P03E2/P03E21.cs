﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E2
{
    /*
     * 3 接口
     * 例1
     */
    class P03E21
    {
        static void main(string[] args)
        {
            C1P03E21 c1 = new C1P03E21() {Name = "a", Age = 12};
            C2P03E21 c2 = new C2P03E21() {First = "f", Last = "l", PersonAge = 10.9};
            Console.WriteLine(c1.GetAge());
            Console.WriteLine(c2.GetName());
        }
    }

    interface IInterP03E21
    {
        string GetName();
        int GetAge();
    }
    class C1P03E21:IInterP03E21
    {
        public string Name;
        public int Age;

        public int GetAge()
        {
            return Age;
        }

        public string GetName()
        {
            return Name;
        }
    }

    class C2P03E21:IInterP03E21
    {
        public string First;
        public string Last;
        public double PersonAge;

        public int GetAge()
        {
            return (int)PersonAge;
        }

        public string GetName()
        {
            return First + Last;
        }
    }
}
