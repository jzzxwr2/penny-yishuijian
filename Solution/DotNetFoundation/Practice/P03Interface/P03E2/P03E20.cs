﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E2
{
    /*
     * 3 接口
     * 引入：不管类的结构，都可以正常处理
     */
    class P03E20
    {
        static void method(c1P03E20 c)
        {
            Console.WriteLine("{0},{1}",c.Name,c.Age);
        }
        static void main(string[] args)
        {

        }
    }

    class c1P03E20
    {
        public string Name;
        public int Age;
    }

    class c2P03E20
    {
        public string First;
        public string Last;
        public double PersonAge;
    }
}
