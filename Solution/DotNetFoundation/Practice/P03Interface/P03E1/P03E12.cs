﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E1
{
    class P03E12
    {
        static void Print(IInterP03E12 item)
        {
            Console.WriteLine(item.GetAge());
        }
        static void main(string[] args)
        {
            CAP03E12 a = new CAP03E12() {age = 12};
            CBP03E12 b = new CBP03E12() {personAge = 10};
            Print(a);
            Print(b);
        }
    }
    interface IInterP03E12
    {
        string GetAge();
    }
    class CAP03E12:IInterP03E12
    {
        public int age;
        public string GetAge()
        {
            return age.ToString();
        }
    }
    class CBP03E12:IInterP03E12
    {
        public double personAge;
        public string GetAge()
        {
            return personAge.ToString();
        }
    }
}
