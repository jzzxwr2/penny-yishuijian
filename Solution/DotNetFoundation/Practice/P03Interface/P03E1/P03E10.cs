﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E1
{
    /*
     * 3 接口
     * 3.0 引题
     */
    class P03E10
    {
        static void PrintInfo(CAP03E10 item)
        {
            Console.WriteLine("{0},{1}", item.Name, item.Age);
        }
        static void main(string[] args)
        {
            CAP03E10 a = new CAP03E10() {Name = "John Doe", Age = 35};
            PrintInfo(a);
        }
    }
    
    class CAP03E10
    {
        public string Name;
        public int Age;
    }
    
    class CBP03E10
    {
        public string First;
        public string Last;
        public double PersonAge;
    }
}
