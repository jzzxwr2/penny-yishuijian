﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E1
{
    /*
     * 3 接口
     * 3.1 例1
     */
    class P03E11
    {
        static void PrintInfo(IInfoP03E11 item)
        {
            Console.WriteLine("{0},{1}", item.GetName(),item.GetAge());
        }
        static void main(string[] args)
        {
            CAP03E11 a = new CAP03E11(){Name="John Doe",Age=35};
            CBP03E11 b = new CBP03E11(){First="Jane",Last="Doe",PersonAge=33};
            PrintInfo(a);
            PrintInfo(b);
        }
    }
    interface IInfoP03E11
    {
        string GetName();
        string GetAge();
    }
    class CAP03E11 : IInfoP03E11
    {
        public string Name;
        public int Age;
        public string GetAge()
        {
            return Age.ToString();
        }
        public string GetName()
        {
            return Name;
        }
    }
    class CBP03E11 : IInfoP03E11
    {
        public string First;
        public string Last;
        public double PersonAge;
        public string GetAge()
        {
            return PersonAge.ToString();
        }
        public string GetName()
        {
            return First + " " + Last;
        }
    }
}
