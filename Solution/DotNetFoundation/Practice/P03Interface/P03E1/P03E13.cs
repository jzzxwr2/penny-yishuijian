﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E1
{
    class P03E13
    {
        static void Print(IInterP03E13 item)
        {
            Console.WriteLine(item.GetAge());
        }
        static void main(string[] args)
        {
            CAP03E13 a = new CAP03E13() {age = 10};
            CBP03E13 b = new CBP03E13() {personage = 20};
            Print(a);
            Print(b);
        }
    }
    interface IInterP03E13
    {
        string GetAge();
    }
    class CAP03E13:IInterP03E13
    {
        public int age;
        public string GetAge()
        {
            return age.ToString();
        }
    }
    class CBP03E13:IInterP03E13
    {
        public double personage;
        public string GetAge()
        {
            return personage.ToString();
        }
    }
}
