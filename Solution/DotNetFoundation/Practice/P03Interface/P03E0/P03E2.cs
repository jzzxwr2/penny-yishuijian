﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E0
{
    /*
    * 3 接口
    * 3.1 概念
    */
    class P03E2
    {
        static void Print(myC[] myClass)
        {
            foreach (var item in myClass)
            {
                Console.WriteLine(item.theValue);
            }
        }
        static void main(string[] args)
        {
            var nums = new int[] {6,2,9,1,3 };
            var myCArray = new myC[5];
            for (int i = 0; i < 5; i++)
            {
                myCArray[i] = new myC();
                myCArray[i].theValue = nums[i];
            }
            Print(myCArray);
            Array.Sort(myCArray);
            Print(myCArray);


        }
        class myC : IComparable
        {
            public int theValue;
            public int CompareTo(object obj)
            {
                myC myC = (myC)obj;
                if (theValue<myC.theValue)
                {
                    return -1;
                }
                if (theValue > myC.theValue)
                {
                    return 1;
                }
                return 0;
            }
        }
    }
}
