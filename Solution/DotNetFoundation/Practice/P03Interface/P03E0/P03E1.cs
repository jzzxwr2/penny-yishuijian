﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E0
{
    /*
    * 3 接口
    * 3.1 概念
    */
    class P03E1
    {
        static void Print(IInfo item)
        {
            Console.WriteLine(item.GetName(),item.GetAge());//传入接口的引用
        }
        static void main(string[] args)
        {
            var a = new CA() { Name = "小红", Age = 18 };
            var b = new CB() { FirstName = "Jim", LastName = "Green", PersonAge = 20 };

            Print(a);//对象的引用能自动转换为它们实现的接口的引用
            Print(b);
        }
    }
    public class CA:IInfo //声明实现了接口的CA类
    {
        public string Name;
        public int Age;

        public string GetName()
        {
            return Name; //在CA中实现IInfo接口的方法GetName()
        }
        public string GetAge()
        {
            return Age.ToString();
        }
    }
    public class CB:IInfo
    {
        public string FirstName;
        public string LastName;
        public double PersonAge;

        public string GetName()
        {
            return FirstName + " " + LastName;
        }
        public string GetAge()
        {
            return PersonAge.ToString();
        }
    }
    interface IInfo //声明接口
    {
        string GetName();
        string GetAge();
    }
}
