﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P03E0
{
    /*
    * 3 接口
    * 3.4 接口是引用类型
    */
    class P03E3
    {
        static void main(string[] args)
        {
            var a = new MyCl();
            a.Print("hello");

            var b = (IInterface)a;
            b.Print("hi");
        }
    }
    interface IInterface
    {
        void Print(string s);
    }
    class MyCl : IInterface
    {
        public void Print(string s)
        {
            Console.WriteLine(s);
        }
    }
}
