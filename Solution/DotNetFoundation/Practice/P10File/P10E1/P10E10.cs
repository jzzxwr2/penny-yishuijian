﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P10File.P10E1
{
    /*
     * 10 File
     * 10.1 File
     */
    class P10E10
    {
        static void main(string[] args)
        {
//            string[] st=new string[]{"aaa","bbb"};
//            File.AppendAllLines("c:\\Users\\PC\\Desktop\\6081.txt", st);
//            var s=File.OpenRead("c:\\Users\\PC\\Desktop\\6081.txt");
//            File.CreateText("c:\\Users\\PC\\Desktop\\6081.txt");
//            StreamWriter sw=new StreamWriter("c:\\Users\\PC\\Desktop\\6081.txt");
//            sw.Write("hello");
//            sw.Flush();
//            sw.Close();
            StreamReader sr=new StreamReader("c:\\Users\\PC\\Desktop\\6081.txt");
//            string str=sr.ReadLine();
//            string s2 = sr.ReadToEnd();
            var s3 = sr.Read();
//            var s4 = sr.ReadBlockAsync();
            Console.WriteLine(s3);
            sr.Close();
        }
    }
}
