﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P10File.P10E1
{
    /*
     * 10 File
     * 10.2 FileInfo
     */
    class P10E11
    {
        static void main(string[] args)
        {
            FileInfo finfo = new FileInfo("c:\\Users\\PC\\Desktop\\6081.txt");
//            Console.WriteLine(finfo.Exists);
//            Console.WriteLine(finfo.CreationTime);
//            Console.WriteLine(finfo.Directory);
//            Console.WriteLine(finfo.DirectoryName);
            //            FileSystemInfo fsinfo = new FileInfo();
            Directory.CreateDirectory("c:\\Users\\PC\\Desktop\\1");
            //            Directory.Delete("c:\\Users\\PC\\Desktop\\1");
            DirectoryInfo dinfo = new DirectoryInfo("c:\\Users\\PC\\Desktop\\1");
            Console.WriteLine(dinfo.FullName);
            

        }
    }
}
