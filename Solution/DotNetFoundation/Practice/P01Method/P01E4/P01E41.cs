﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E4
{
    class P01E41
    {
        static void main(string[] args)
        {
            int[] nums = new int[] {1, 2, 3};
            cP01E41 c = new cP01E41();
            c.method(nums);
            foreach (int num in nums)
            {
                Console.WriteLine(num);
            }
        }
    }

    class cP01E41
    {
        public void method(params int[] nums)
        {
            if (nums!=null&&nums.Length!=0)
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    nums[i] = nums[i] * 10;
                    Console.WriteLine(nums[i]);
                }
            }
        }
    }
}
