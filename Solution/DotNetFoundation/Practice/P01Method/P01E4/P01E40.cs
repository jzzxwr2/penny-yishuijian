﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E4
{
    /*
     * 1 方法
     * 1.3 参数数组
     * 1.3.2 数组参数是引用类型
     * 例4
     */
    class P01E40
    {
        static void main(string[] args)
        {
            int[] nums = new int[] {1, 2, 3};
            cP01E40 c = new cP01E40();
            c.method(nums);
            foreach (int item in nums)
            {
                Console.WriteLine(item);
            }
        }
    }

    class cP01E40
    {
        public void method(params int[] nums)
        {
            if (nums!=null&&nums.Length!=0)
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    nums[i] = nums[i] * 10;
                    Console.WriteLine(nums[i]);
                }
            }
        }
    }
}
