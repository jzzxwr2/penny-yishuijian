﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E1
{
    class P01E11
    {
        static void method(ref cP01E11 c, ref int numvalue)
        {
            c.num += 1;
            numvalue += 2;
            Console.WriteLine("{0},{1}",c.num,numvalue);
        }
        static void main(string[] args)
        {
            cP01E11 c = new cP01E11();
            int i = 10;

            method(ref c, ref i);
            Console.WriteLine("{0},{1}",c.num,i);
        }
    }

    class cP01E11
    {
        public int num = 6;
    }
}
