﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E1
{
    /*
     * 1 方法
     * 1.1 引用参数
     * 例1
     */
    class P01E10
    {
        static void method(ref cP01E10 instance, ref int numvalue)
        {
            instance.num = instance.num + 5;
            numvalue = numvalue + 5;
            Console.WriteLine("{0},{1}",instance.num,numvalue);
        }
        static void main(string[] args)
        {
            cP01E10 c=new cP01E10();
            int i = 6;

            method(ref c, ref i);
            Console.WriteLine("{0},{1}",c.num,i);
        }
    }

    class cP01E10
    {
        public int num = 20;

        
    }
}
