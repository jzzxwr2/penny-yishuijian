﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E3
{
    class P01E31
    {
        static void main(string[] args)
        {
            cP01E31 c = new cP01E31();
            int i1 = 1, i2 = 2, i3 = 3;
            c.method(i1, i2, i3);
            Console.WriteLine("{0},{1},{2}",i1,i2,i3);
        }
    }

    class cP01E31
    {
        public void method(params int[] nums)
        {
            if (nums!=null&&nums.Length!=0)
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    nums[i] = nums[i] * 10;
                    Console.WriteLine(nums[i]);
                }
            }
        }
    }
}
