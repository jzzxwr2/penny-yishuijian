﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E3
{
    /*
     * 1 方法
     * 1.3 参数数组
     * 1.3.1 数组参数是值类型
     * 例3
     */
    class P01E30
    {
        static void main(string[] args)
        {
            int i1 = 5, i2 = 6, i3 = 7;
            cP01E30 c = new cP01E30();
            c.method(i1, i2, i3);
            Console.WriteLine("{0},{1},{2}",i1,i2,i3);
        }
    }

    class cP01E30
    {
        public void method(params int[] nums)
        {
            if (nums!=null&&nums.Length!=0)
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    nums[i] = nums[i] * 10;
                    Console.WriteLine(nums[i]);
                }
            }
        }
    }
}
