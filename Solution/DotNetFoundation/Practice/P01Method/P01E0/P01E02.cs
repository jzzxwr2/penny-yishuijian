﻿using System;

namespace DotNetFoundation.Practice.P01Method.P01E0
{
    /*
     * 0 参数
     * 0.2 引用类型作为值参数、引用参数
     */
    class P01E02
    {
        static void RefAsParameter(P122MyClass f1)
        {
            f1.Val = 60;
            Console.WriteLine(f1.Val);
            f1=new P122MyClass();
            Console.WriteLine(f1.Val);
        }
        static void RefAsParameter2(ref P122MyClass f1)
        {
            f1.Val = 60;
            Console.WriteLine(f1.Val);
            f1=new P122MyClass();
            Console.WriteLine(f1.Val);
        }
        static void main(string[] args)
        {
            var a1=new P122MyClass();

            Console.WriteLine(a1.Val);
//            RefAsParameter(a1);
            RefAsParameter2(ref a1);
            Console.WriteLine(a1.Val);
        }
    }
    class P122MyClass
    {
        public int Val = 50;
    }
}
