﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E0
{
    /*
     * 0 参数
     * 0.3 out参数
     */
    class P01E03
    {
        private static void Method(out P123MyClass f1, out int f2)
        {
            f1 = new P123MyClass
            {
                Val = 25
            };
            f2 = 15;
            Console.WriteLine("{0},{1}", f1.Val, f2);
        }
        private static void main(string[] args)
        {
            P123MyClass a1;
            int a2;

            Method(out a1, out a2);
            Console.WriteLine("{0},{1}", a1.Val, a2);
        }
    }
    class P123MyClass
    {
        public int Val = 20;
    }
}
