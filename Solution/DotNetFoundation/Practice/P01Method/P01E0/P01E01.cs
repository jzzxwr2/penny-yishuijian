﻿using System;

namespace DotNetFoundation.Practice.P03Interface.P01Method.P01E0
{
    /*
     * 0 参数
     * 0.1 值参数、引用参数
     */
    internal class P01E01
    {
        private static void ValMethod(P121MyClass f1, int f2)
        {
            f1.Val = f1.Val + 5;
            f2 = f2 + 5;
            Console.WriteLine("{0},{1}", f1.Val, f2);
        }

        private static void RefMethod(ref P121MyClass f1, ref int f2)
        {
            f1.Val = f1.Val + 5;
            f2 = f2 + 5;
            Console.WriteLine("{0},{1}", f1.Val, f2);
        }

        private static void main(string[] args)
        {
            var a1 = new P121MyClass();
            var a2 = 10;

            ValMethod(a1, a2);
            RefMethod(ref a1,ref a2);
            Console.WriteLine("{0},{1}", a1.Val, a2);
        }
    }

    internal class P121MyClass
    {
        public int Val = 20;
    }
}