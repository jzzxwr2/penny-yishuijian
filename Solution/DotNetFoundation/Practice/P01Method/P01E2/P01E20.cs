﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E2
{
    /*
     * 1 方法
     * 1.2 引用类型作为引用参数
     * 例2
     */
    class P01E20
    {
        static void method(ref cP01E20 item)
        {
            item.num = 6;
            Console.WriteLine(item.num);

            item = new cP01E20();
            Console.WriteLine(item.num);
        }
        static void main(string[] args)
        {
            cP01E20 c = new cP01E20();

            Console.WriteLine(c.num);
            method(ref c);
            Console.WriteLine(c.num);
        }
    }

    class cP01E20
    {
        public int num = 10;
    }
}
