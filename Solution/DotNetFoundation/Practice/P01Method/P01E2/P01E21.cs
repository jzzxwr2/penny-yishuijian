﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P01Method.P01E2
{
    class P01E21
    {
        static void method(ref cP01E21 item)
        {
            item.num = 1;
            Console.WriteLine(item.num);

            item = new cP01E21();
            Console.WriteLine(item.num);
        }
        static void main(string[] args)
        {
            cP01E21 c = new cP01E21();
            //Console.WriteLine(c.num);
            method(ref c);
            Console.WriteLine(c.num);
        }
    }

    class cP01E21
    {
        public int num = 8;
    }
}
