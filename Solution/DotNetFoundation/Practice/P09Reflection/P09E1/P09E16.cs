﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E1
{
    class P09E16
    {
        static void main(string[] args)
        {
            cdP09E16 d1 = new cdP09E16();
            cdP09E16 d2 = new cdP09E16();
            cdP09E16[] ds = new cdP09E16[] {d1, d2};
            foreach (var item in ds)
            {
                Type t = item.GetType();
                Console.WriteLine(t.Name);
                FieldInfo[] fis = t.GetFields();
                foreach (var fi in fis)
                {
                    Console.WriteLine(fi.Name);
                }

            }
        }
    }

    class cbP09E16
    {
        public int bf = 0;
    }

    class cdP09E16:cbP09E16
    {
        public int df = 0;
    }
}
