﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E1
{
    class P09E13
    {
        static void main(string[] args)
        {
            cbP09E13 b = new cbP09E13();
            cdP09E13 d = new cdP09E13();
            cbP09E13[] bs = new cbP09E13[] {b, d};
            foreach (var item in bs)
            {
                Type t = item.GetType();
                Console.WriteLine(t.Name);
                FieldInfo[] fis = t.GetFields();
                foreach (var fi in fis)
                {
                    Console.WriteLine(fi.Name);
                }

            }
        }
    }

    class cbP09E13
    {
        public int bf = 0;
    }

    class cdP09E13 : cbP09E13
    {
        public int df = 0;
    }
}
