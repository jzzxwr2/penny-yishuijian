﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E1
{
    class P09E15
    {
        static void main(string[] args)
        {
            cbP09E15 b = new cbP09E15();
            cdP09E15 d = new cdP09E15();
            cbP09E15[] bs = new cbP09E15[] {b, d};
            foreach (var item in bs)
            {
                Type t = item.GetType();
                Console.WriteLine(t.Name);
                FieldInfo[] fis = t.GetFields();
                foreach (var f in fis)
                {
                    Console.WriteLine(f.Name);
                }
            }
        }
    }

    class cbP09E15
    {
        public int bf = 0;
    }

    class cdP09E15 : cbP09E15
    {
        public int df = 0;
    }
}
