﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E1
{
    class P09E11
    {
        static void main(string[] args)
        {
            cbP09E11 b = new cbP09E11();
            cdP09E11 d = new cdP09E11();
            cbP09E11[] bs = new cbP09E11[] {b, d};
            foreach (var item in bs)
            {
                Type t = item.GetType();
                Console.WriteLine(t.Name);

                FieldInfo[] fi = t.GetFields();
                foreach (var f in fi)
                {
                    Console.WriteLine(f.Name);
                }

            }
        }
    }

    class cbP09E11
    {
        public int bf = 0;
    }

    class cdP09E11:cbP09E11
    {
        public int df = 0;
    }
}
