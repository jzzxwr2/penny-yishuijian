﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DotNetFoundation.Practice.P02Class.P02Inheritance;

namespace DotNetFoundation.Practice.P09Reflection.P09E1
{
    /*
     * 9 反射
     * 9.1获取Type对象
     * 例1 通过GetType()
     */
    class P09E10
    {
        static void main(string[] args)
        {
            cbP09E10 b = new cbP09E10();
            cdP09E10 d = new cdP09E10();

            cbP09E10[] bc = new cbP09E10[] {b, d};

            foreach (var item in bc)
            {
                Type t = item.GetType();
                Console.WriteLine(t.Name);

                FieldInfo[] fi = t.GetFields();
                foreach (var f in fi)
                {
                    Console.WriteLine(f.Name);
                }
            }
            
        }
    }

    class cbP09E10
    {
        public int bf = 0;
    }

    class cdP09E10:cbP09E10
    {
        public int df = 0;
    }
}
