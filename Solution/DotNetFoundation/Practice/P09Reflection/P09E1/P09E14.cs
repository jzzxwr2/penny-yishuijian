﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E1
{
    class P09E14
    {
        static void main(string[] args)
        {
            cbP09E14 b = new cbP09E14();
            cdP09E14 d = new cdP09E14();
            cbP09E14[] bs = new cbP09E14[] {b, d};
            foreach (var item in bs)
            {
                Type t = item.GetType();
                Console.WriteLine(t.Name);
                FieldInfo[] fis = t.GetFields();
                foreach (var f in fis)
                {
                    Console.WriteLine(f.Name);
                }
            }

        }
    }

    class cbP09E14
    {
        public int bf = 0;
    }

    class cdP09E14 : cbP09E14
    {
        public int df = 0;
    }
}
