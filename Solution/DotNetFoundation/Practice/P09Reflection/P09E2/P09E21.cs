﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E2
{
    class P09E21
    {
        static void main(string[] args)
        {
            Type t = typeof(cdP09E21);
            Console.WriteLine(t.Name);
            FieldInfo[] fis = t.GetFields();
            foreach (var fi in fis)
            {
                Console.WriteLine(fi.Name);
            }
        }
    }

    class cbP09E21
    {
        public int bf;
    }

    class cdP09E21 : cbP09E21
    {
        public int df;
    }
}
