﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E2
{
    /*
     * 9 反射
     * 9.1获取Type对象
     * 例2 通过typeof
     */
    class P09E20
    {
        static void main(string[] args)
        {
            Type t = typeof(cdP09E20);
            Console.WriteLine(t.Name);

            FieldInfo[] fis = t.GetFields();
            foreach (var fi in fis)
            {
                Console.WriteLine(fi.Name);
            }
        }
    }

    class cbP09E20
    {
        public int bf;
    }

    class cdP09E20:cbP09E20
    {
        public int df;
    }
}
