﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P09Reflection.P09E3
{
    /*
     * 9 反射
     * 例3 获取命名空间、类名、方法名
     */
    class P09E30
    {
        static string method()
        {
            string s = "";
            s+="namespace"+MethodBase.GetCurrentMethod().DeclaringType.Namespace+ "\n";
            s+="class"+MethodBase.GetCurrentMethod().DeclaringType.FullName+ "\n";
            s+="method"+MethodBase.GetCurrentMethod().Name+ "\n";
            s += "\n";

            StackTrace ss = new StackTrace(true);
            MethodBase mb = ss.GetFrame(1).GetMethod();
            s += mb.DeclaringType.Namespace + "\n";
            s += mb.DeclaringType.Name + "\n";
            s += mb.Name + "\n";

            return s;
        }
        static void main(string[] args)
        {
            Console.WriteLine(method());
        }
    }
}
