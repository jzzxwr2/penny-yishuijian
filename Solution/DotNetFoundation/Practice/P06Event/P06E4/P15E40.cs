﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E4
{
    /*
     * 15 事件
     * 例4 
     */
    class P15E40
    {
        static void main(string[] args)
        {
            decP15e40 dec = new decP15e40();
            subP15E40 sub = new subP15E40();
            dec.eventP15E40 += sub.subMethod;
            dec.SetValue(7);
            dec.SetValue(11);
        }
    }

    delegate void deleP15E40();
    class decP15e40
    {

        public event deleP15E40 eventP15E40;
        private int value;

        protected virtual void decMethod()
        {
            if (eventP15E40!=null)
            {
                eventP15E40();
            }
            else
            {
                Console.WriteLine("not fire");
            }
        }
        public void SetValue(int i)
        {
            if (value != i)
            {
                value = i;
                decMethod();
            }
        }
        public decP15e40()
        {
            int n = 5;
            SetValue(n);
        }

        
    }

    class subP15E40
    {
        public void subMethod()
        {
            Console.WriteLine("fire");
        }
    }
}
