﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E4
{
    class P15E44
    {
        static void main(string[] args)
        {
            decP15E44 dec = new decP15E44();
            subP15E44 sub = new subP15E44();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E44(int i);

    class decP15E44
    {
        public event deleP15E44 eventP15E44;

        public void decMethod(int i)
        {
            if (i>10)
            {
                if (eventP15E44!=null)
                {
                    eventP15E44(i);
                }
            }
            else
            {
                Console.WriteLine("not fire");
            }
        }
    }

    class subP15E44
    {
        public void method(int i)
        {
            if (i>5)
            {
                Console.WriteLine("fire");
            }
        }

        public void subMethod(decP15E44 dec)
        {
            dec.eventP15E44 += method;
        }
    }
}
