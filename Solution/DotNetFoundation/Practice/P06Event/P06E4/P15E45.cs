﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E4
{
    class P15E45
    {
        static void main(string[] args)
        {
            decP15E45 dec = new decP15E45();
            subP15E45 sub = new subP15E45();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E45(int i);

    class decP15E45
    {
        public event deleP15E45 eventP15E45;

        public void decMethod(int i)
        {
            if (i%2==0)
            {
                if (eventP15E45!=null)
                {
                    eventP15E45(i);
                }
            }
            else
            {
                Console.WriteLine("not fire");
            }
        }
    }

    class subP15E45
    {
        public void method(int i)
        {
            if (i%2==0)
            {
                Console.WriteLine("fire");
            }
        }

        public void subMethod(decP15E45 dec)
        {
            dec.eventP15E45 += method;
        }
    }
}
