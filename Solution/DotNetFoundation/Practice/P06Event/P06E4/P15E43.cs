﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E4
{
    class P15E43
    {
        static void main(string[] args)
        {
            decP15E43 dec = new decP15E43();
            subP15E43 sub = new subP15E43();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E43(int i);

    class decP15E43
    {
        public event deleP15E43 eventP15E43;

        public void decMethod(int i)
        {
            if (i>5)
            {
                if (eventP15E43!=null)
                {
                    eventP15E43(i);
                }
            }
            else
            {
                Console.WriteLine("not fire");
            }
        }
    }

    class subP15E43
    {
        public void method(int i)
        {
            if (i>5)
            {
                Console.WriteLine("fire");
            }
        }

        public void method2(int i)
        {
            if (i>5&&i%2==0)
            {
                Console.WriteLine("fire again");
            }
        }
        public void subMethod(decP15E43 de)
        {
            de.eventP15E43+= method;
            de.eventP15E43 += method2;
        }
    }
}
