﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E4
{
    class P15E42
    {
        static void main(string[] args)
        {
            decP15E42 dec = new decP15E42();
            subP15E42 sub = new subP15E42();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E42(int i);

    class decP15E42
    {
        public event deleP15E42 eventP15E42;

        public void decMethod(int i)
        {
            if (i%2==0)
            {
                if (eventP15E42 != null)
                {
                    eventP15E42(i);
                }
            }
            
            else
            {
                Console.WriteLine("not fire");
            } 
        }
    }

    class subP15E42
    {
        public void method(int i)
        {
            if (i%2==0)
            {
                Console.WriteLine("fire");
            }
        }

        public void subMethod(decP15E42 de)
        {
            de.eventP15E42 += method;
        }
    }
}
