﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E4
{
    class P15E41
    {
        static void main(string[] args)
        {
            decP15E41 dec = new decP15E41();
            subP15E41 sub = new subP15E41();

            dec.eventP15E41 += sub.subMethod;
            dec.SetValue(1);
            dec.SetValue(5);
            dec.SetValue(1);
        }
    }

    delegate void deleP15E41();

    class decP15E41
    {
        public event deleP15E41 eventP15E41;
        private int value;

        public void decMethod()
        {
            if (eventP15E41!=null)
            {
                eventP15E41();
            }
            else
            {
                Console.WriteLine("not fire");
            }
        }

        public void SetValue(int i)
        {
            if (value!=i)
            {
                value = i;
                decMethod();
            }
        }

        public decP15E41()
        {
            int n = 1;
            SetValue(n);
        }
    }

    class subP15E41
    {
        public void subMethod()
        {
            Console.WriteLine("fire");
        }
    }
}
