﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E6
{
    /*
     * 15 事件
     * 15.3 移除事件处理程序
     * 例6 
     */
    class P15E60
    {
        static void main(string[] args)
        {
            pubP15E60 p = new pubP15E60();
            subP15E60 s = new subP15E60();

            p.eventP15E60 += s.subMethodA;
            p.eventP15E60 += s.subMethodB;
            p.pubMehtod();

            Console.WriteLine("remove b");
            p.eventP15E60 -= s.subMethodB;
            p.pubMehtod();

        }
    }

    class pubP15E60
    {
        public event EventHandler eventP15E60;

        public void pubMehtod()
        {
            eventP15E60(this, null);
        }
    }

    class subP15E60
    {
        public void subMethodA(object o, EventArgs e)
        {
            Console.WriteLine("A");
        }
        public void subMethodB(object o, EventArgs e)
        {
            Console.WriteLine("B");
        }
    }
}
