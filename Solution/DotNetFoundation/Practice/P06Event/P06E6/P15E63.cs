﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E6
{
    class P15E63
    {
        static void main(string[] args)
        {
            pubP15E63 p = new pubP15E63();
            subP15E63 s = new subP15E63();

            p.eventP15E63 += s.methodA;
            p.eventP15E63 += s.methodB;
            p.pubMethod();

            p.eventP15E63 -= s.methodA;
            p.pubMethod();
        }
    }

    class pubP15E63
    {
        public event EventHandler eventP15E63;

        public void pubMethod()
        {
            eventP15E63(this, null);
        }
    }

    class subP15E63
    {
        public void methodA(object o, EventArgs e)
        {
            Console.WriteLine("A");
        }

        public void methodB(object o, EventArgs e)
        {
            Console.WriteLine("B");
        }
        
    }
}
