﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E6
{
    class P15E61
    {
        static void main(string[] args)
        {
            pubP15E61 p = new pubP15E61();
            subP15E61 s = new subP15E61();

            p.eventP15E61 += s.subMethodA;
            p.eventP15E61 += s.subMethodB;
            p.pubMethod();
            p.eventP15E61 -=s.subMethodA;
            p.pubMethod();
        }
    }

    class pubP15E61
    {
        public event EventHandler eventP15E61;

        public void pubMethod()
        {
            eventP15E61(this, null);
        }
    }

    class subP15E61
    {
        public void subMethodA(object o,EventArgs e)
        {
            Console.WriteLine("A");
        }
        public void subMethodB(object o, EventArgs e)
        {
            Console.WriteLine("B");
        }
    }
}
