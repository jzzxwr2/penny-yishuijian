﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E6
{
    class P15E62
    {
        static void main(string[] args)
        {
            pubP15E62 p = new pubP15E62();
            subP15E62 s = new subP15E62();

            p.eventP15E62 += s.subMethodA;
            p.eventP15E62 += s.subMethodB;
            p.pubMethod();

            p.eventP15E62 -=s.subMethodA;
            p.pubMethod();
        }
    }

    class pubP15E62
    {
        public event EventHandler eventP15E62;

        public void pubMethod()
        {
            eventP15E62(this, null);
        }

    }

    class subP15E62
    {
        public void subMethodA(object o, EventArgs e)
        {
            Console.WriteLine("A");
        }

        public void subMethodB(object o, EventArgs e)
        {
            Console.WriteLine("B");
        }
    }
}
