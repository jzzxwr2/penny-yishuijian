﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DotNetFoundation.Practice.P06Event.P15E3
{
    /*
     * 15 事件
     * 15.1 标准事件的用法
     * 例3 
     */
    class P15E30
    {
        static void main(string[] args)
        {
            pubP15E30 p = new pubP15E30();
            subP15E30 s = new subP15E30();

            s.subMethod(p);
            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    class pubP15E30
    {
        public event EventHandler eventP15E30;

        public void pubMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%20==0)
                {
                    if (eventP15E30!=null)
                    {
                        eventP15E30(this,null);
                    }
                }
            }
        }
    }

    class subP15E30
    {
        public int subnum { get; private set; }

        private void method(object o,EventArgs e)
        {
            subnum++;
        }

        public void subMethod(pubP15E30 p)
        {
            p.eventP15E30 += method;
        }
    }
//    class P15E30
//    {
//        private static int i = 0;
//        private static string str = "a";
//
//        static void WriteChar(object source, ElapsedEventArgs e)
//        {
//            Console.Write(str[i++%str.Length]);
//        }
//        static void main(string[] args)
//        {
//            Timer timer = new Timer(3);
//            //timer.Elapsed+=new ElapsedEventHandler(WriteChar);
//            timer.Elapsed+=WriteChar;
//            timer.Start();
//            System.Threading.Thread.Sleep(1);
//            Console.ReadKey();
//        }
//    }

}
