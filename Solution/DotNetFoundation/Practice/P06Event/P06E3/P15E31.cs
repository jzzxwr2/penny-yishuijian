﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E3
{
    class P15E31
    {
        static void main(string[] args)
        {
            pubP15E31 p = new pubP15E31();
            subP15E31 s = new subP15E31(p);

            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    class pubP15E31
    {
        public event EventHandler eventP15E31;

        public void pubMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%8==0)
                {
                    if (eventP15E31!=null)
                    {
                        eventP15E31(this, null);
                    }
                }
            }
        }
    }

    class subP15E31
    {
        public int subnum { get; private set; }

        private void method(object o, EventArgs e)
        {
            subnum++;
        }

        public subP15E31(pubP15E31 p)
        {
            p.eventP15E31 += method;
        }
    }
}
