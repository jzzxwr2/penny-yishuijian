﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E5
{
    class P15E55
    {
        static void main(string[] args)
        {
            pubP15E55 p = new pubP15E55();
            subP15E55 s = new subP15E55();

            s.subMethod(p);
            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    class eventArgsP15E55 : EventArgs
    {
        public int argnum { get; set; }
    }
    class pubP15E55
    {
        public event EventHandler<eventArgsP15E55> eventP15E55;

        public void pubMethod()
        {
            eventArgsP15E55 args = new eventArgsP15E55();
            for (int i = 1; i < 80; i++)
            {
                if (i%10==0)
                {
                    if (eventP15E55!=null)
                    {
                        args.argnum = i;
                        eventP15E55(this, args);
                    }
                }
            }
        }
    }

    class subP15E55
    {
        public int subnum { get; private set; }

        private void method(object o, eventArgsP15E55 e)
        {
            Console.WriteLine(e.argnum);
            subnum++;
        }

        public void subMethod(pubP15E55 p)
        {
            p.eventP15E55 += method;
        }
    }
}
