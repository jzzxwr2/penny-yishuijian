﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E5
{
    class P15E54
    {
        static void main(string[] args)
        {
            pubP15E54 p = new pubP15E54();
            subP15E54 s = new subP15E54(p);

            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    public class eventArgsP15E54 : EventArgs
    {
        public int argnum { get; set; }
    }

    class pubP15E54
    {
        public event EventHandler<eventArgsP15E54> eventP15E54;

        public void pubMethod()
        {
            eventArgsP15E54 args = new eventArgsP15E54();
            for (int i = 1; i < 50; i++)
            {
                if (i%8==0)
                {
                    if (eventP15E54!=null)
                    {
                        args.argnum = i;
                        eventP15E54(this, args);
                    }
                }
            }
        }
    }

    class subP15E54
    {
        public int subnum { get; private set; }

        private void method(object o, eventArgsP15E54 e)
        {
            Console.WriteLine(e.argnum);
            subnum++;
        }

        public subP15E54(pubP15E54 p)
        {
            p.eventP15E54 += method;
        }
    }
}
