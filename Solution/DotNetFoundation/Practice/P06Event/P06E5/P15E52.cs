﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E5
{
    class P15E52
    {
        static void main(string[] args)
        {
            pubP15E52 p = new pubP15E52();
            subP15E52 s = new subP15E52(p);

            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    class eventArgsP15E52 : EventArgs
    {
        public int argnum { get; set; }
    }

    class pubP15E52
    {
        public event EventHandler<eventArgsP15E52> eventP15E52;

        public void pubMethod()
        {
            eventArgsP15E52 args = new eventArgsP15E52();
            for (int i = 1; i < 50; i++)
            {
                if (i%8==0)
                {
                    if (eventP15E52!=null)
                    {
                        args.argnum = i;
                        eventP15E52(this, args);
                    }
                }
            }
        }
    }

    class subP15E52
    {
        public int subnum { get; private set; }

        private void method(object o, eventArgsP15E52 e)
        {
            Console.WriteLine("{0},{1}", e.argnum, o.ToString());
            subnum++;
        }

        public subP15E52(pubP15E52 p)
        {
            p.eventP15E52 += method;
        }
    }
}
