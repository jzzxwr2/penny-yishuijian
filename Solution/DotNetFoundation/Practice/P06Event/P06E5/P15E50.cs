﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E5
{
    /*
     * 15 事件
     * 15.2 通过扩展EventArgs来传递数据
     * 例5 
     */
    class P15E50
    {
        
        static void main(string[] args)
        {
            pubP15E50 p = new pubP15E50();
            subP15E50 s = new subP15E50(p);


        }
    }

    public class EventArgsP15E50 : EventArgs
    {
        public int argnum { get; set; }
    }

    class pubP15E50
    {
        public event EventHandler<EventArgsP15E50> eventP15E50;

        public void decMethod()
        {
            EventArgsP15E50 args = new EventArgsP15E50();
            for (int i = 1; i < 50; i++)
            {
                if (i%6==0)
                {
                    if (eventP15E50!=null)
                    {
                        args.argnum = i;
                        eventP15E50(this, args);
                    }
                }
            }
        }
    }

    class subP15E50
    {
        public int subnum { get; private set; }

        
        public subP15E50(pubP15E50 pub)
        {
            subnum = 0;
            pub.eventP15E50 += subMethod;
        }
        private void subMethod(object source, EventArgsP15E50 e)
        {
            Console.WriteLine("{0},{1}", e.argnum, source.ToString());
            subnum++;
        }
    }
}
