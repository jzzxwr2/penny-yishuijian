﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E5
{
    class P15E51
    {
        static void main(string[] args)
        {
            pubP15E51 p = new pubP15E51();
            subP15E51 s = new subP15E51(p);

            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    class eventArgsP15E51 : EventArgs
    {
        public int argnum { get; set; }
    }

    class pubP15E51
    {
        public event EventHandler<eventArgsP15E51> eventP15E51;

        public void pubMethod()
        {
            eventArgsP15E51 args = new eventArgsP15E51();
            for (int i = 1; i < 50; i++)
            {
                if (i%6==0)
                {
                    if (eventP15E51!=null)
                    {
                        args.argnum = i;
                        eventP15E51(this, args);
                    }
                }
            }
        }
    }

    class subP15E51
    {
        public int subnum { get; private set; }

        public subP15E51(pubP15E51 p)
        {
            subnum = 0;
            p.eventP15E51 += method;
        }

        private void method(object o, eventArgsP15E51 e)
        {
            Console.WriteLine("{0},{1}", e.argnum, o.ToString());
            subnum++;
        }
    }
}
