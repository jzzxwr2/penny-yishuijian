﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E5
{
    class P15E53
    {
        static void main(string[] args)
        {
            pubP15E53 p = new pubP15E53();
            subP15E53 s = new subP15E53(p);

            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    class eventArgsP15E53 : EventArgs
    {
        public int argnum { get; set; }
    }

    class pubP15E53
    {
        public event EventHandler<eventArgsP15E53> eventP15E53;

        public void pubMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%5==0)
                {
                    if (eventP15E53!=null)
                    {
                        eventArgsP15E53 args = new eventArgsP15E53();
                        args.argnum = i;
                        eventP15E53(this, args);
                    }
                }
            }
        }
    }

    class subP15E53
    {
        public int subnum { get; private set; }

        private void method(object o,eventArgsP15E53 e)
        {
            Console.WriteLine("{0},{1}", e.argnum, o.ToString());
            subnum++;
        }

        public subP15E53(pubP15E53 p)
        {
            p.eventP15E53 += method;
        }
    }
}
