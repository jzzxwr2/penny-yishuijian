﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E12
    {
        static void main(string[] args)
        {
            IncrementerP15E12 incrementer=new IncrementerP15E12();
            DozensP15E12 dozensCounter = new DozensP15E12(incrementer);
            incrementer.DoCount();
            Console.WriteLine(dozensCounter.DozensCount);
        }
    }

    delegate void HandlerP15E12();

    class IncrementerP15E12
    {
        public event HandlerP15E12 CountedDozen;
        public void DoCount()
        {
            for (int i = 1; i < 100; i++)
            {
                if (i%12==0&&CountedDozen!=null)
                {
                    CountedDozen();
                }
            }
        }
    }

    class DozensP15E12
    {
        public int DozensCount { get; private set; }

        public DozensP15E12(IncrementerP15E12 incrementer)
        {
            DozensCount = 0;
            incrementer.CountedDozen += IncrementDozensCount;
        }

        void IncrementDozensCount()
        {
            DozensCount++;
        }
    }
}
