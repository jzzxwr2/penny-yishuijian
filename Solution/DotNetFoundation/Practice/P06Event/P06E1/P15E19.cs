﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E19
    {
        static void main(string[] args)
        {
            pubP15E19 p = new pubP15E19();
            subP15E19 s=new subP15E19();

            s.subMethod(p);
            p.pubMethod();
            Console.WriteLine(s.subnum);
        }
    }

    delegate void deleP15E19();

    class pubP15E19
    {
        public event deleP15E19 eventP15E19;

        public void pubMethod()
        {
            for (int i = 0; i < 50; i++)
            {
                if (i%6==0)
                {
                    if (eventP15E19!=null)
                    {
                        eventP15E19();
                    }
                }
            }
        }
    }

    class subP15E19
    {
        public int subnum { get; private set; }

        private void method()
        {
            subnum++;
        }

        public void subMethod(pubP15E19 p)
        {
            p.eventP15E19 += method;
        }
    }
}
