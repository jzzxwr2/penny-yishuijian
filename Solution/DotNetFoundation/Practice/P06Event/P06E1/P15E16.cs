﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E16
    {
        static void main(string[] args)
        {
            decP15E16 dec = new decP15E16();
            subP15E16 sub = new subP15E16(dec);
            dec.decMethod();
            Console.WriteLine(sub.num);

        }
    }

    delegate void deleP15E16();

    class decP15E16
    {
        public event deleP15E16 eventP15E16;

        public void decMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%6==0)
                {
                    if (eventP15E16!=null)
                    {
                        eventP15E16();
                    }
                }
            }
        }
    }

    class subP15E16
    {
        public int num { get; private set; }

        private void subMethod()
        {
            num++;
        }

        public subP15E16(decP15E16 dec)
        {
            num = 0;
            dec.eventP15E16 += subMethod;
        }
    }
}
