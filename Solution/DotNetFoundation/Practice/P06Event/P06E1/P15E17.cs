﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E17
    {
        static void main(string[] args)
        {
            decP15E17 dec = new decP15E17();
            subP15E17 sub = new subP15E17();
            sub.subMethod(dec);
            dec.decMethod();
            Console.WriteLine(sub.num);
        }
    }

    delegate void deleP15E17();

    class decP15E17
    {
        public event deleP15E17 eventP15E17;

        public void decMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%6==0)
                {
                    if (eventP15E17!=null)
                    {
                        eventP15E17();
                    }
                }
            }
        }
    }

    class subP15E17
    {
        public  int num { get; private set; }
        //public int Num { get; set; }
        private void method()
        {
            num++;
        }

        public void subMethod(decP15E17 dec)
        {
            dec.eventP15E17 += method;
        }
    }
}
