﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E14
    {
        static void main(string[] args)
        {
            decP15E14 dec = new decP15E14();
            subP15E14 sub = new subP15E14(dec);
            dec.deleMethod();
            Console.WriteLine(sub.j);
        }
    }

    delegate void deleP15E14();
    class decP15E14
    {
        public event deleP15E14 eventP15E14;

        public void deleMethod()
        {
            for (int i = 1; i < 100; i++)
            {
                if (i%10==0&&eventP15E14!=null)
                {
                    eventP15E14();
                }
            }
        }
    }

    class subP15E14
    {
        public int j { get; private set; }
        void subMethod()
        {
            j++;
        }
        public subP15E14(decP15E14 de)
        {
            j = 0;
            de.eventP15E14 += subMethod;
        }
    }
}
