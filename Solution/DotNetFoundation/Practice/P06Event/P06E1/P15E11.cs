﻿using System;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E11
    {
        static void main(string[] args)
        {
            IncrementerP15E11 incrementer = new IncrementerP15E11();
            DozensP15E11 dozensCounter = new DozensP15E11(incrementer);
            incrementer.DoCount();
            Console.WriteLine(dozensCounter.DozenCount);

        }
    }

    delegate void HandlerP15E11();

    class IncrementerP15E11
    {
        public event HandlerP15E11 ContedADozen;
        public void DoCount()
        {
            for (int i = 1; i < 100; i++)
            {
                if (i % 12 == 0 && ContedADozen != null)
                {
                    ContedADozen();
                }
            }
        }
    }

    class DozensP15E11
    {
        public int DozenCount { get; private set; }

        public DozensP15E11(IncrementerP15E11 incrementer)
        {
            DozenCount = 0;
            incrementer.ContedADozen += IncrementDozensCount;
        }

        void IncrementDozensCount()
        {
            DozenCount++;
        }
    }
}
