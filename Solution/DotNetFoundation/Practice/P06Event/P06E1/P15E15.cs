﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E15
    {
        static void main(string[] args)
        {
            decP15E15 dec = new decP15E15();
            subP15E15 sub = new subP15E15(dec);
            dec.decMethod();
            Console.WriteLine(sub.j);
        }
    }

    delegate void deleP15E15();

    class decP15E15
    {
        public event deleP15E15 eventP15E15;

        public void decMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%5==0&&eventP15E15!=null)
                {
                    eventP15E15();
                }
            }
        }
    }

    class subP15E15
    {
        public int j { get; private set; }

        void subMethod()
        {
            j++;
        }

        public subP15E15(decP15E15 de)
        {
            de.eventP15E15 += subMethod;
        }
    }
}
