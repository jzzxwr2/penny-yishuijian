﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    /*
     * 15 事件
     * 例1 
     */
    class P15E10
    {
        static void main(string[] args)
        {
            IncrementerP15E10 incrementer = new IncrementerP15E10();
            DozensP15E10 dozensCounter = new DozensP15E10(incrementer);
            incrementer.DoCount();
            Console.WriteLine(dozensCounter.DozenCount);

        }
    }

    delegate void Handler();

    class IncrementerP15E10
    {
        public event Handler ContedADozen;

        public void DoCount()
        {
            for (int i = 1; i < 100; i++)
            {
                if (i%12==0&&ContedADozen!=null)
                {
                    ContedADozen();
                }
            }
        }
    }

    class DozensP15E10
    {
        public int DozenCount { get; private set; }

        public DozensP15E10(IncrementerP15E10 incrementer)
        {
            DozenCount = 0;
            incrementer.ContedADozen += IncrementDozensCount;
        }

        void IncrementDozensCount()
        {
            DozenCount++;
        }
    }
}
