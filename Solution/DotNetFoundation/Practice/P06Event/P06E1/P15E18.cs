﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E18
    {
        static void main(string[] args)
        {
            pubP15E18 p = new pubP15E18();
            subP15E18 s = new subP15E18();

            p.eventP15E18 += s.method;
            p.pubMethod();
            Console.WriteLine(s.num);


        }
    }
    delegate void deleP15E18();

    class pubP15E18
    {
        public event deleP15E18 eventP15E18;

        public void pubMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i % 6 == 0)
                {
                    if (eventP15E18 != null)
                    {
                        eventP15E18();
                    }
                }
            }
        }
    }

    class subP15E18
    {
        public int num { get; private set; }

        public void method()
        {
//            num = 0;
            num++;
        }

    }
}
