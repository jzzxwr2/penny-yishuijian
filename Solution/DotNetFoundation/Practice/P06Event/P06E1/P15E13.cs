﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    class P15E13
    {
        static void main(string[] args)
        {
            decP15E13 de = new decP15E13();
            subP15E13 su = new subP15E13(de);
            de.deMethod();
            Console.WriteLine(su.i);
        }
    }
    delegate void deP15E13();
    class decP15E13
    {
        public event deP15E13 eventP15E13;

        public void deMethod()
        {
            for (int i = 1; i < 50; i++)
            {
                if (i%8==0&&eventP15E13!=null)
                {
                    eventP15E13();
                }
            }
        }
    }

    class subP15E13
    {
        public int i { get; private set; }

        public subP15E13(decP15E13 de)
        {
            i = 0;
            de.eventP15E13 += subMethod;

        }

        void subMethod()
        {
            i++;
        }
    }
}
