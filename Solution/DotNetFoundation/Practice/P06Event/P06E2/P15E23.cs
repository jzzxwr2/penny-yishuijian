﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E2
{
    class P15E23
    {
        static void main(string[] args)
        {
            decP15E23 dec = new decP15E23();
            subP15E23 sub=new subP15E23();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E23(int i);

    class decP15E23
    {
        public event deleP15E23 eventP15E23;

        public void decMethod(int i)
        {
            if (i==1||i==2)
            {
                if (i==1)
                {
                    Console.WriteLine("上课铃");
                }
                else if (i==2)
                {
                    Console.WriteLine("下课铃");
                }

                if (eventP15E23!=null)
                {
                    eventP15E23(i);
                }
            }
            else
            {
                Console.WriteLine("铃声参数不正确");
            }
        }
    }

    class subP15E23
    {
        public void method(int i)
        {
            if (i==1)
            {
                Console.WriteLine("学习");
            }
            else if (i==2)
            {
                Console.WriteLine("休息");
            }
        }

        public void subMethod(decP15E23 de)
        {
            de.eventP15E23 += method;
        }
    }

}
