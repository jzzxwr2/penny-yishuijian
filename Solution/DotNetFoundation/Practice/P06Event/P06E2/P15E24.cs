﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Console = System.Console;

namespace DotNetFoundation.Practice.P06Event.P15E2
{
    class P15E24
    {
        static void main(string[] args)
        {
            decP15E24 dec = new decP15E24();
            subP15E24 sub = new subP15E24();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E24(int i);

    class decP15E24
    {
        public event deleP15E24 eventP15E24;

        public void decMethod(int i)
        {
            if (i==1||i==2)
            {
                if (i==1)
                {
                    Console.WriteLine("上课铃");
                }
                else if (i==2)
                {
                    Console.WriteLine("下课铃");
                }

                if (eventP15E24 !=null)
                {
                    eventP15E24(i);
                }
            }
            else
            {
                Console.WriteLine("铃声参数不正确");
            }
        }
    }

    class subP15E24
    {
        public void method(int i)
        {
            if (i==1)
            {
                Console.WriteLine("学习");
            }
            else if (i==2)
            {
                Console.WriteLine("休息");
            }
        }

        public void subMethod(decP15E24 de)
        {
            de.eventP15E24 += method;
        }
    }
}
