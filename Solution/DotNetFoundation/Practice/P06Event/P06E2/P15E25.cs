﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//
//namespace DotNetFoundation.Practice.P15Event.P15E2
//{
//    class P15E25
//    {
//        static void main(string[] args)
//        {
//            pubP15E25 p = new pubP15E25();
//            subP15E25 s = new subP15E25();
//            p.pubnum = Convert.ToInt32(Console.ReadLine());
//            s.subMethod(p);
//            p.pubMethod();
//        }
//    }
//
//    class pubP15E25
//    {
//        public int pubnum { get; set; }
//        public event EventHandler eventP15E25;
//        public void pubMethod()
//        {
//            if (pubnum==1||pubnum==2)
//            {
//                if (pubnum==1)
//                {
//                    Console.WriteLine("上课");
//                    eventP15E25(this, null);
//                }
//                else if (pubnum ==2)
//                {
//                    Console.WriteLine("下课");
//                    eventP15E25(this, null);
//                }
//            }
//        }
//    }
//
//    class subP15E25
//    {
//        public int subnum { get; set; }
//
//        private void method(object o, EventArgs e)
//        {
//            if (subnum==1)
//            {
//                Console.WriteLine("学习");
//            }
//            else if (subnum==2)
//            {
//                Console.WriteLine("休息");
//            }
//        }
//
//        public void subMethod(pubP15E25 p)
//        {
//            p.eventP15E25 += method;
//        }
//    }
//}
