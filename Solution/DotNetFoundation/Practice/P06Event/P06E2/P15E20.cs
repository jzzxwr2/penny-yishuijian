﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E1
{
    /*
     * 15 事件
     * 例2 
     */
    class P15E20
    {
        static void main(string[] args)
        {
            SchoolRingP15E20 sr = new SchoolRingP15E20();
            StudentsP15E20 student = new StudentsP15E20();
            student.SubsctribeToRing(sr);
            Console.WriteLine("请输入打铃参数（1：打上课铃；2：打下课铃）：");
            sr.Jow(Convert.ToInt32(Console.ReadLine()));
            Console.ReadLine();
        }
    }

    public delegate void RingEventP15E20(int ringKind);

    public class SchoolRingP15E20
    {
        public RingEventP15E20 OnBellSound;

        public void Jow(int ringKind)
        {
            if (ringKind==1||ringKind==2)
            {
                Console.WriteLine(ringKind == 1 ? "上课铃响了，" : "下课铃响了，");
                if (OnBellSound!=null)
                {
                    OnBellSound(ringKind);
                }
            }
            else
            {
                Console.WriteLine("这个铃声参数不正确！");
            }
        }
    }

    public class StudentsP15E20
    {
        public void SubsctribeToRing(SchoolRingP15E20 schoolRing)
        {
            schoolRing.OnBellSound += SchoolJow;
        }

        public void SchoolJow(int rinnKind)
        {
            if (rinnKind==2)
            {
                Console.WriteLine("同学们开始课间休息！");
            }
            else if (rinnKind==1)
            {
                Console.WriteLine("同学们开始认真学习！");
            }
        }

        public void CancelSubscribe(SchoolRingP15E20 schoolRing)
        {
            schoolRing.OnBellSound -= SchoolJow;
        }
    }
}
