﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E2
{
    class P15E26
    {
        static void main(string[] args)
        {
            pubP15E26 p = new pubP15E26();
            subP15E26 s = new subP15E26();

            s.subMethod(p);
            p.pubMethod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E26(int delenum);
    class pubP15E26
    {
        public event deleP15E26 eventP15E26;

        public void pubMethod(int pubnum)
        {
            if (pubnum == 1 || pubnum == 2)
            {
                if (pubnum == 1)
                {
                    Console.WriteLine("上课");
                }
                else if (pubnum == 2)
                {
                    Console.WriteLine("下课");
                }

                if (eventP15E26 != null)
                {
                    eventP15E26(pubnum);
                }
            }
            else
            {
                Console.WriteLine("不正确");
            } 
        }
    }

    class subP15E26
    {
        private void method(int subnum)
        {
            if (subnum==1)
            {
                Console.WriteLine("学习");
            }
            else if (subnum==2)
            {
                Console.WriteLine("休息");
            }
        }

        public void subMethod(pubP15E26 p)
        {
            p.eventP15E26 += method;
        }
    }
}
