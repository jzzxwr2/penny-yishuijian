﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E2
{
    class P15E21
    {
        static void main(string[] args)
        {
            SchoolRingP15E21 sr=new SchoolRingP15E21();
            StuP15E21 st = new StuP15E21();
            st.SubscribeToRing(sr);
            Console.WriteLine("输入参数");
            sr.Jow(Convert.ToInt32(Console.ReadLine()));
            Console.ReadLine();
        }
    }

    public delegate void RingEventP15E11(int ringKind);

    public class SchoolRingP15E21
    {
        public RingEventP15E11 OnBellSound;

        public void Jow(int ringKind)
        {
            if (ringKind==1||ringKind==2)
            {
                Console.WriteLine(ringKind==1?"上课铃":"下课铃");
            }

            if (OnBellSound!=null)
            {
                OnBellSound(ringKind);
            }
            else
            {
                Console.WriteLine("铃声参数不正确");
            }
        }

        
    }
    public class StuP15E21
    {
        public void SchoolJow(int ringKind)
        {
            if (ringKind == 2)
            {
                Console.WriteLine("休息");
            }
            else if (ringKind == 1)
            {
                Console.WriteLine("学习");
            }
        }

        public void SubscribeToRing(SchoolRingP15E21 schoolRing)
        {
            schoolRing.OnBellSound += SchoolJow;
        }

        public void CancelSubscrive(SchoolRingP15E21 schoolRing)
        {
            schoolRing.OnBellSound -= SchoolJow;
        }
    }
}
