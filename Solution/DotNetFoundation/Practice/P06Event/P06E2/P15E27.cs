﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.Practice.P06Event.P15E2
{
    class P15E27
    {
        static void main(string[] args)
        {
            pubP15E27 p = new pubP15E27();
            subP15E27 s = new subP15E27();
            s.subMethod(p);
            p.pubMehthod(Convert.ToInt32(Console.ReadLine()));
        }
    }

    delegate void deleP15E27(int delenum);

    class pubP15E27
    {
        public event deleP15E27 eventP15E27;
        public void pubMehthod(int pubnum)
        {
            if (pubnum==1||pubnum==2)
            {
                if (pubnum==1)
                {
                    Console.WriteLine("上课");
                }
                else if (pubnum==2)
                {
                    Console.WriteLine("休息");
                }

                if (eventP15E27!=null)
                {
                    eventP15E27(pubnum);
                }
            }
        }
    }

    class subP15E27
    {
        public void method(int i)
        {
            if (i==1)
            {
                Console.WriteLine("学习");
            }
            else if (i==2)
            {
                Console.WriteLine("休息");
            }
        }
        public void subMethod(pubP15E27 p)
        {
            p.eventP15E27 += method;
        }
    }
}
