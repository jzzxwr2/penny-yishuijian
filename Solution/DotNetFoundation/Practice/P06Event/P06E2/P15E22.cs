﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotNetFoundation.Practice.P06Event.P15E2
{
    class P15E22
    {
        static void main(string[] args)
        {
            decP15E22 dec = new decP15E22();
            subP15E22 sub = new subP15E22();
            sub.subMethod(dec);
            dec.decMethod(Convert.ToInt32(Console.ReadLine()));
//            Console.ReadLine();
        }
    }

    delegate void deleP15E22(int i);

    class decP15E22
    {
        public event deleP15E22 eventP15E22;

        public void decMethod(int i)
        {
            if (i==1||i==2)
            {
                if (i==1)
                {
                    Console.WriteLine("上课铃");
                }
                else if (i==2)
                {
                    Console.WriteLine("下课铃");
                }

                if (eventP15E22!=null)
                {
                    eventP15E22(i);
                }
            }
            else
            {
                Console.WriteLine("铃声参数不正确");
            }
        }

    }

    class subP15E22
    {
        public void Method(int i)
        {
            if (i==1)
            {
                Console.WriteLine("学习");
            }
            else if (i==2)
            {
                Console.WriteLine("休息");
            }
        }

        public void subMethod(decP15E22 de)
        {
            de.eventP15E22 += Method;
        }
    }
}
