﻿using System;

namespace DotNetFoundation.Practice
{
    class Text
    {
        static void main(string[] args)
        {
            
        }
    }

    internal class Person2
    {
        public int _age;

        public int Age
        {
            get => _age;
            set => _age = value;
        }

        public void SayHello()
        {
            Console.WriteLine("hello");
        }

        
    }

    internal class Student2 : Person2
    {
        public int Age1
        {
            get => _age;
            set => _age = value;
        }
    }
}