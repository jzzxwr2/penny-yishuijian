﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.tangdashi.Chapter02.T01
{
    class T0201
    {
        static void main(string[] args)
        {
            //float f = 123.456f;
            float f = float.Parse(Console.ReadLine());
            double intpart = Math.Floor(f);
            var decimalpart = f.ToString().Substring(f.ToString().IndexOf('.')+1);
            Console.WriteLine(intpart);
            Console.WriteLine(decimalpart);
        }
    }
}
