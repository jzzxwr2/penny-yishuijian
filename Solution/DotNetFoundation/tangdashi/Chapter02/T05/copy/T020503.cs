﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.tangdashi.Chapter02.T05.copy
{
    class T020503
    {
        static void main(string[] args)
        {
            int i1 = Convert.ToInt32(Console.ReadLine());
            int i2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(SuShu(i1,i2));
        }

        static string SuShu(int a, int b)
        {
            string s = string.Empty;
            int num = 0;
            for (int i = a; i <=b; i++)
            {
                bool flag = true;
                for (int j = 2; j <Math.Sqrt(i); j++)
                {
                    if (i%j==0)
                    {
                        flag = false;
                    }
                }

                if (flag)
                {
                    s += i + "\t";
                    num++;
                }

                if (num==10)
                {
                    s += "\n";
                    num = 0;
                }
            }

            return s;
        }
    }
}
