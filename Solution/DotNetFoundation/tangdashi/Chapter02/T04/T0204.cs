﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.tangdashi.Chapter02.T04
{
    class T0204
    {
        static void main(string[] args)
        {
            int i = Convert.ToInt32(Console.ReadLine());
            if(i<1||i>12)
                Console.WriteLine("请重新输入");
            switch (i)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    Console.WriteLine(31);
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    Console.WriteLine(30);
                    break;
                case 2:
                    Console.WriteLine(29);
                    break;

            }
        }
    }
}
