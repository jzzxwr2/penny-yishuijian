﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.tangdashi.Chapter02.T06
{
    class T0206
    {
        static void main(string[] args)
        {
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(SSYZ(a));
        }

        static string SSYZ(int a)
        {
            string s = string.Empty;
            int num = 0;
            bool flag = true;
            for (int i = 2; i < Math.Sqrt(a); i++)
            {
                if (a % i == 0)
                {
                    flag = false;
                }

                if (flag)
                {
                    s += i + "\t";
                    num++;
                }

                if (num==10)
                {
                    s += "\n";
                    num = 0;
                }
            }

            return s;
        }
        /*static string SSYZ(int a)
        {
            string s = string.Empty;
            int num = 0;
            for (int i = 2; i <Math.Sqrt(i); i++)
            {
                bool flag = true;
                for (int j = 2; j <Math.Sqrt(i); j++)
                {
                    if (i%j==0)
                    {
                        flag = false;
                    }
                }

                if (flag)
                {
                    s += i + "\t";
                    num++;
                }

                if (num==10)
                {
                    s += "\n";
                }
            }

            return s;
        }*/
    }
    
}
