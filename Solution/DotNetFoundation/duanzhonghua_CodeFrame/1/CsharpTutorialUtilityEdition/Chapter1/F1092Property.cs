﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DotNetFoundation.duanzhonghua_CodeFrame._1.CsharpTutorialUtilityEdition.Chapter1
{
    /// <summary>
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.9.2  
     */
    /// <summary>
    class F1092
    {
        static void main(string[] args)
        {
            Person onePerson = new Person();

            onePerson.Name = "田七";
            string s = onePerson.Name;

            onePerson.Age = 20;
            int x = onePerson.Age;

            onePerson.Display();
        }
    }
    public class Person
    {
        private string _pName = "张三"; //_pName是私有字段
        private int _pAge = 12; //_pAge是私有字段
        public void Display()
        {
            Console.WriteLine("姓名：{0},年龄：{1}",_pAge,_pName);
        }

        //定义属性Name
        public string Name
        {
            get { return _pName; }
            set { _pName = value; }
        }
        //定义属性Age
        public int Age
        {
            get { return _pAge; }
            set { _pAge = value; }
        }
    }
}
