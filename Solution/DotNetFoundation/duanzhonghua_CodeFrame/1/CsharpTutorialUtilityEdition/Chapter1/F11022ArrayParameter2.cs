﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.duanzhonghua_CodeFrame._1.CsharpTutorialUtilityEdition.Chapter1
{
    /// <summary>
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.10.3  MethodParameter
     * 方法的参数为数组时，也可以不使用params，此种方法可以使用一维或多维数组
     */
    /// <summary>
    class F11022
    {
        static void main(string[] args)
        {
            int[,] a = { { 1,2,3}, { 4,5,6} };
            F(a);//实参为数组类引用的变量a

            //F(10,20,30,40);//此格式不能使用
            F(new int[,] { { 60,70}, { 80,90} });//实参为数组类引用
            //F();//此格式不能使用
            //F(new int[,] { });//此格式不能使用
        }

        //值参数，参数类型为数组类引用变量
        static void F(int[,] args)
        {
            Console.WriteLine("Array contains{0} elements:", args.Length);

            foreach (int i in args)
            {
                Console.Write("{0}", i);
            }
        }
    }
}
