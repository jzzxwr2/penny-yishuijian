﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.duanzhonghua_CodeFrame.CsharpTutorialUtilityEdition.Chapter1
{
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.4.1 C#的数据类型
     * C#的数据类型分为三种
     * 值类型，引用类型，指针类型  （栈至堆引用）
     * 指针类型仅用于非安全代码中
     */

    class F1041
    {
        static void main(string [] args)
        {
            MyClass4 r1 = new MyClass4();//引用变量r1存储在MyClass类对象的地址
            MyClass4 r2 = r1;//r1、r2都代表同一个MyClass对象
            r2.a = 2;//等价于r1.a=2;
            MyClass4.f1();
        }
                
    }
    
    class MyClass4
    {
        public int a = 0;

        static public void f1()
        {
            int v1 = 1;//值类型变量，值1 存储在栈中
            int v2 = v1;
        }
    }
}
