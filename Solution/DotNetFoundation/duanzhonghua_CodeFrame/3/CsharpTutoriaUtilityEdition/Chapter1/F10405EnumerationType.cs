﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.duanzhonghua_CodeFrame._3.CsharpTutoriaUtilityEdition.Chapter1
{
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.4.5 枚举类型
     */
    class F1045
    {
        static void main(string[] args)
        {
            Days day = Days.Tue;
            int x = (int)Days.Tue;
            Console.WriteLine("day={0},x={1}",day,x);
        }
    }
    enum Days
    {
        Sat=1,
        Sun,
        Mon,
        Tue,
        Thu,
        Fri
    }
}
