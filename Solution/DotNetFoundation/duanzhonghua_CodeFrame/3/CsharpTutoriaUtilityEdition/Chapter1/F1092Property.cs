﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DotNetFoundation.duanzhonghua_CodeFrame._3.CsharpTutoriaUtilityEdition.Chapter1
{
    class F1092
    {
        static void main(string[] args)
        {
            Person person = new Person();

            Console.WriteLine(person.Name);
            Console.WriteLine(person.Age);

            Person p1 = new Person();
            p1.Name = "赵六";
            string s = p1.Name;
            p1.Age = 50;
            int age = p1.Age;
            Console.WriteLine("{0},{1}",s,age);
        }
    }
    public class Person
    {
        private string _pName="王五";
        private int _pAge = 30;
        public void Display()
        {
            Console.WriteLine(_pName);
            Console.WriteLine(_pAge);
        }
        public string Name
        {
            get { return _pName; }
            set { _pName = value; }
        }
        public int Age
        {
            get { return _pAge; }
            set { _pAge = value; }
        }
    }
}
