﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.duanzhonghua_CodeFrame._3.CsharpTutoriaUtilityEdition.Chapter1
{
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.4.10 字符串类
     */
    class F10410
    {
        static void main(string[] args)
        {
            //字符串的自定
            string FirstName = "Ming";
            string LastName = "Zhang";
            string Name = FirstName + " " + LastName;
            char[] s22 = {'计','算','机','科','学'};
            string s3 = new string(s22);

            //字符串搜索
            string s = "ABC科学家";
            int i = s.IndexOf("科");

            //字符串比较函数
            string s1 = "abc";
            string s2 = "abc";
            int n = string.Compare(s1, s2);
            //Console.WriteLine(n.ToString());

            //判断字符串是否为空
            string s5 = "";
            string s6 = "不空";

            if (s5.Length==0)
            {
                s1 = "空";
            }

            //得到子字符串
            string s7 = "取子字符串";
            string s8 = s.Substring(2, 2);//从索引器第二个，取2个
            char sc1 = s7[0];//得到"取"

            //字符串删除
            string s9 = s7.Remove(0, 2);//从索引0开始，删除2个字符，s7内容不变
            //Console.WriteLine(s7) ;

            //插入字符串
            string s10 = "计算机科学";
            string s11 = s10.Insert(3, "软件");
            //Console.WriteLine(s11);

            //字符串替换函数
            string s12 = s10.Replace("计算机", "软件");

            //把string转换为字符数组
            char[] sc2 = s10.ToCharArray(0, s10.Length);
            Console.WriteLine(sc2);

            //把其他类型转换为字符串
            int i1 = 9;
            string s13 = i1.ToString();

            //大小写转换
            string s14 = "AaBbCc";
            string s15 = s14.ToLower();
            string s16 = s14.ToUpper();
            Console.WriteLine("{0},{1}",s15,s16);
        }
    }
}
