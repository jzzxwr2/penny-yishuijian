﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DotNetFoundation.duanzhonghua_CodeFrame._2.CsharpTutorialUtilityEdition.Chapter1
{
    /// <summary>
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.9.2
     */
    /// <summary>
    class F1092
    {

        static void main(string[] args)
        {
            Person person = new Person();

            person.Name = "小红";
            string s = person.Name;

            person.Age = 18;
            int i = person.Age;

            person.Display();
        }
    }
    public class Person
    {
        private string _pName = "张三";//私有字段
        private int _pAge = 12;//私有字段
        public void Display()
        {
            Console.WriteLine(_pName);
            Console.WriteLine(_pAge);
        }

        //定义属性
        public string Name
        {
            get { return _pName; }
            set { _pName = value; }
        }

        //定义属性Age
        public int Age
        {
            get { return _pAge; }
            set { _pAge = value; }
        }
    }
}
