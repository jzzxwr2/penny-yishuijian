﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.duanzhonghua_CodeFrame._2.CsharpTutorialUtilityEdition.Chapter1
{
    /// <summary>
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.10.3  MethodParameter
     */
    /// <summary>
    class F11021
    {
        static void main(string[] args)
        {
            int[] a = { 1, 2, 3 };
            F(a);//实参作为数组类引用变量a
            F(10, 20, 30, 40);//等价于F(new int[] { 10, 20, 30, 40 });
            F(new int[] { 60, 70, 80, 90 });//实参为数组类引用
            F();//等价于F(new int[] { });
            F(new int[] { });
        }

        //数组参数，用params声明
        static void F(params int[] args)
        {
            Console.WriteLine("Array contains{0} elements:",args.Length);

            foreach (int i in args)
            {
                Console.Write("{0}", i);
                Console.WriteLine() ;
            }
        }
    }
}
