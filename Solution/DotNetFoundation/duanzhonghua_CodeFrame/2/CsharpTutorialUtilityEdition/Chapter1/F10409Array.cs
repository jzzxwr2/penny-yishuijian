﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetFoundation.duanzhonghua_CodeFrame._2.CsharpTutorialUtilityEdition.Chapter1
{
    /*
     * CsharpTutorialUtilityEdition C#教程实用版
     * 1.4.9 一维数组和多维数组
     */
    class F1049
    {
        static void main(string[] args)
        {
            //一维数组
            int[] array = new int[3];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = i * i;
                Console.WriteLine("array[{0}]={1}", i, array[i]);
            }

            //for (int i = 0; i < array.Length; i++)
            //{
            //    Console.WriteLine("array[{0}]={1}",i,array[i]);
            //}

            string[,] a3;//二维数组
            string[][] j2;
            string[][][][][] j3;
            //在数组的声明的时候，可以对元素复制
            int[] a1 = new int[] { 1, 2, 3 };

            int[] a4 = { 1, 2, 3 };

        }
    }
}
