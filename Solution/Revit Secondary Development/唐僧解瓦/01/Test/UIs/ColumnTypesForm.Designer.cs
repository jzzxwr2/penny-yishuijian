﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Form = System.Windows.Forms.Form;

namespace Revit_Secondary_Development.唐僧解瓦._01.Test.UIs
{
    partial class ColumnTypesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.symbolCombo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "柱子族类型";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // symbolCombo
            // 
            this.symbolCombo.FormattingEnabled = true;
            this.symbolCombo.Location = new System.Drawing.Point(182, 88);
            this.symbolCombo.Name = "symbolCombo";
            this.symbolCombo.Size = new System.Drawing.Size(121, 20);
            this.symbolCombo.TabIndex = 1;
            // 
            // ColumnTypesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 326);
            this.Controls.Add(this.symbolCombo);
            this.Controls.Add(this.label1);
            this.Name = "ColumnTypesForm";
            this.Text = "ColumnTypesForm";
            this.Load += new System.EventHandler(this.ColumnTypesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox symbolCombo;
    }
}