﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Revit_Secondary_Development.唐僧解瓦._08.BinLibrary.Extensions
{
    public static class BitmapExtension
    {
        public static BitmapSource ToBitmapSource(this Bitmap bitmap)
        {
            BitmapSource result = null;
            IntPtr handel = bitmap.GetHbitmap();
            result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(handel, IntPtr.Zero, Int32Rect.Empty,
                System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            return result;
        }
    }
}
