﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;

namespace Revit_Secondary_Development.唐僧解瓦._08.BinLibrary.Extensions
{
    public static class ReferenceExtension
    {
        public static Element GetElement(this Reference thisref, Document doc)
        {
            return doc.GetElement(thisref);
        }
    }
}
