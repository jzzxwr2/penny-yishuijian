﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;

namespace Revit_Secondary_Development.唐僧解瓦._05.BinLibrary.Extensions
{
    public static class WindowExtension
    {
        public static WindowInteropHelper helper(this Window win)
        {
            var helper = new WindowInteropHelper(win);
            return helper;
        }
    }
}
