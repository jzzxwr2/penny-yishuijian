﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.唐僧解瓦._07.Test
{
    [Transaction(TransactionMode.Manual)]
    class OtherTest : IExternalCommand
    {
        Autodesk.Revit.DB.Document doc = null;
        UIApplication uiapp = null;
        UIDocument uidoc = null;
        string _volume = string.Empty;
        string _area = string.Empty;
        object obj = new object();

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            uiapp = commandData.Application;
            uidoc = commandData.Application.ActiveUIDocument;
            doc = commandData.Application.ActiveUIDocument.Document;
            object obj = new object();
            using (Transaction trans=new Transaction(doc,"xxx"))
            {
                trans.Start();
                Data data = new Data();
                FaceRecorder.Instance(doc, data).Recorder();
                double d = FaceRecorder.Instance(doc, data).Extract("a1");
                trans.Commit();
            }

            return Result.Succeeded;
        }
    }

    public class FaceRecorder
    {
        private Document _doc = null;
        private Schema _schema = null;
        private IFaceRecordData _data = null;
        private static FaceRecorder _instance;
        private static readonly object synRoot = new object();

        private FaceRecorder(Document doc, IFaceRecordData recordedData)
        {
            _doc = doc;
            _data = recordedData;
        }

        public static FaceRecorder Instance(Document doc, IFaceRecordData recordedData)
        {
            if (_instance==null)
            {
                lock (synRoot)
                {
                    if (_instance==null)
                    {
                        _instance = new FaceRecorder(doc, recordedData);
                    }
                }
            }

            return _instance;
        }
        public void Recorder()
        {
            SchemaBuilder builder = new SchemaBuilder(_data.Guid);
            builder.SetWriteAccessLevel(AccessLevel.Public);
            builder.SetReadAccessLevel(AccessLevel.Public);
            builder.SetSchemaName(_data.SchemaName);
            foreach (RecordData item in _data.Fields)
            {
                if (item.Type == typeof(string) || item.Type == typeof(bool))
                    builder.AddSimpleField(item.Key, item.Type);
                else
                {
                    FieldBuilder fb = builder.AddSimpleField(item.Key, item.Type);
                    fb.SetUnitType(UnitType.UT_Length);
                }
            }

            _schema = builder.Finish();
            Entity ent = new Entity(_schema);
            foreach (RecordData item in _data.Fields)
            {
                ent.Set(item.Key,item.Type);
            }

            DataStorage st = DataStorage.Create(_doc);
            st.Name = "myStorage";
            st.SetEntity(ent);
        }

        public dynamic Extract(string fieldName)
        {
            DataStorage ds = new FilteredElementCollector(_doc).OfClass(typeof(DataStorage)).Cast<DataStorage>()
                .FirstOrDefault(x => x.Name == "myStorage");
            Schema schema = Schema.Lookup(_data.Guid);
            Type t = _data.Fields.FirstOrDefault(x => x.Key == fieldName).Type;
            Entity e = ds.GetEntity(schema);
            var o = e.GetType().GetMethod("Get", new Type[] {typeof(string), typeof(DisplayUnitType)})
                .MakeGenericMethod(t).Invoke(e, new object[] {fieldName, DisplayUnitType.DUT_METERS});
            dynamic d = Convert.ChangeType(o, t);
            return d;
        }

    }
    public class Data : IFaceRecordData
    {
        public List<RecordData> Fields
        {
            get
            {
                return new List<RecordData>()
                {
                    new RecordData("a1,",2.2),
                    new RecordData("a2",true),
                    new RecordData("a3","你好")
                };
            }
        }

        
        public string SchemaName { get { return "mySchema";} }

        public string StorageName { get { return "myStorage";} }

        public Guid Guid
        {
            get
            {
                return new Guid("d07f0dc5-b028-45c0-b5e7-9583353315d7");
            }
        }
    }

    public interface IFaceRecordData
    {
        List<RecordData> Fields { get; }
        string SchemaName { get; }
        string StorageName { get; }
        Guid Guid { get; }
    }
    public class RecordData
    {
        public string Key { get; set; }
        public Type Type { get; set; }
        public dynamic Value { get; set; }

        public RecordData(string key, dynamic value)
        {
            Key = key;
            Value = value;
            Type = value.GetType();
        }
    }
}
