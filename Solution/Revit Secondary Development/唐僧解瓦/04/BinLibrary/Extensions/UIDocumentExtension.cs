﻿using System.Linq;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.唐僧解瓦._4.BinLibrary.Extensions
{
    public static class UIDocumentExtension
    {
        public static UIView ActiveUiView(this UIDocument uidoc)
        {
            var result = default(UIView);
            var doc = uidoc.Document;
            var acview = doc.ActiveView;

            var uiviews = uidoc.GetOpenUIViews();
            result = uiviews.FirstOrDefault(m => m.ViewId == acview.Id);
            return result;
        }
    }
}
