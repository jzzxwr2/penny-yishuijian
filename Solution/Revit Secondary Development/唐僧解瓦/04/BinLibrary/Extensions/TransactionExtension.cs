﻿using System;
using Autodesk.Revit.DB;
using Revit_Secondary_Development.唐僧解瓦.BinLibrary.Helpers;
// ReSharper disable All

namespace Revit_Secondary_Development.唐僧解瓦._4.BinLibrary.Extensions
{
    public static class TransactionExtension
    {
        public static void IgnoreFailure(this Transaction trans)
        {
            var options = trans.GetFailureHandlingOptions();
            options.SetFailuresPreprocessor(new failure_ignore());
        }

//        public static void Invoke(this Document doc, Action<Transaction> action,
//            string transactionName = "aaa")
//        {
//            Transaction ts = new Transaction(doc, transactionName);
//            LogHelper.LogException(delegate
//            {
//                ts.Start();
//                action(ts);
//                ts.Commit();
//            },@"c:\transactionExcepton.txt");
//        }
    }

    public class failure_ignore : IFailuresPreprocessor
    {
        public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
        {
            failuresAccessor.DeleteAllWarnings();
//            failuresAccessor.DeleteElements(failuresAccessor.el);
            return FailureProcessingResult.Continue;
        }
    }
}
