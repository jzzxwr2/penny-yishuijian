﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

namespace Revit_Secondary_Development.唐僧解瓦._4.BinLibrary.Helpers
{
    public static class collectorHelper
    {
        public static IList<T> TCollector<T>(this Document doc)
        {
            Type type = typeof(T);
            return new
                FilteredElementCollector(doc).OfClass(type).Cast<T>().ToList();
        }
    }
}
