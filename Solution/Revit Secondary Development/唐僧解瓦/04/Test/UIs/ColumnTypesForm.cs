﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Form = System.Windows.Forms.Form;

namespace Revit_Secondary_Development.唐僧解瓦._04.Test.UIs
{
    public partial class ColumnTypesForm : Form
    {
        private static ColumnTypesForm instance;
        private List<FamilySymbol> symbols;

        public static ColumnTypesForm Getinstance(List<Element> elements)
        {
            if (instance==null)
            {
                instance = new ColumnTypesForm(elements);
            }

            return instance;
        }
        public ColumnTypesForm()
        {
            InitializeComponent();
        }

        ColumnTypesForm(List<Element> elements)
        {
            symbols = elements.Cast<FamilySymbol>().ToList();
            InitializeComponent();
        }

        private void ColumnTypesForm_Load(object sender, EventArgs e)
        {
            this.symbolCombo.Items.Clear();
            this.symbolCombo.DataSource = symbols;
            this.symbolCombo.DisplayMember = "Name";
            symbolCombo.SelectedIndex = 0;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
//            base.OnClosing(e);
        }

        private void label1_Clicd(object sender, EventArgs e)
        {

        }

        private void symbolCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
