﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Revit_Secondary_Development.唐僧解瓦._06.BinLibrary.Extensions;

namespace Revit_Secondary_Development.唐僧解瓦._06.Test
{
    /// <summary>
    /// 计算墙的两面面积
    /// </summary>
    [Transaction(TransactionMode.Manual)]
    [Journaling(JournalingMode.UsingCommandData)]
    [Regeneration(RegenerationOption.Manual)]
    class Cmd_CalculateAreaOfShipment : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uiapp = commandData.Application;
            var uidoc = uiapp.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            var wall =
                sel.PickObject(ObjectType.Element, doc.GetSelecitonFilter(m => m is Wall)).GetElement(doc) as Wall;

            var facesoutRef = HostObjectUtils.GetSideFaces(wall, ShellLayerType.Exterior);
            var facesinRef = HostObjectUtils.GetSideFaces(wall, ShellLayerType.Interior);

            var faceout = wall.GetGeometryObjectFromReference(facesinRef.First()) as Face;
            var facein = wall.GetGeometryObjectFromReference(facesinRef.First()) as Face;

            var area = default(double);
            area += faceout.Area;
            area += facein.Area;
            area = UnitUtils.ConvertFromInternalUnits(area, DisplayUnitType.DUT_SQUARE_METERS);

            MessageBox.Show(area.ToString() + "m^2");

            return Result.Succeeded;
        }
    }
}
