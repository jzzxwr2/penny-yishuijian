﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.唐僧解瓦._06.BinLibrary.Extensions
{
    public static class DocumentExtension
    {
        public static UIView ActiveUiView(this UIDocument uidoc)
        {
            var acview = uidoc.ActiveView;
            var uiviews = uidoc.GetOpenUIViews();
            var acuiview = uiviews.Where(m => acview.Id == m.ViewId).FirstOrDefault();
            return acuiview;
        }
    }
}
