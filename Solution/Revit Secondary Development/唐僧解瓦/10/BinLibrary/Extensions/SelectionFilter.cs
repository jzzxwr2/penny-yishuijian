﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Extensions;
namespace Revit_Secondary_Development.唐僧解瓦._10.BinLibrary.Extensions
{
    public class SelectionFilter : ISelectionFilter
    {
        Document _doc;
        Type _type;
        public SelectionFilter(Document doc, Type type)
        {
            _doc = doc;
            _type = type;
        }
        public bool AllowElement(Element elem)
        {
            if (elem.GetType()==_type)
            {
                return true;
            }
            return false;
        }
        public bool AllowReference(Reference reference, XYZ position)
        {
            return true;
        }
    }
    public static class SelectionFilterHelper
    {
        public static MultSelecitonFilter GetSelecitonFilter(this Document doc, Func<Element, bool> func1,
            Func<Reference, bool> func2)
        {
            return new MultSelecitonFilter(func1,func2);
        }
    }
    public class MultiSelectionFilter : ISelectionFilter
    {
        Func<Element, bool> eleFunc;
        private Func<Reference, bool> refFunc;
        public MultiSelectionFilter(Func<Element, bool> eleFunc, Func<Reference, bool> refFunc)
        {
            this.eleFunc = eleFunc;
            this.refFunc = refFunc;
        }
        public bool AllowElement(Element elem)
        {
            return refFunc != null ? true : eleFunc(elem);
        }
        public bool AllowReference(Reference reference, XYZ position)
        {
            return refFunc == null ? false : refFunc(reference);
//            return refFunc(reference);
        }
    }
}
