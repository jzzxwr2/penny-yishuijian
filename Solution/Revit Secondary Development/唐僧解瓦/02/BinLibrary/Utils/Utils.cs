﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Utils
{
    internal class Utils
    {
        /// <summary>
        ///     Gets solid objects of specified element.
        /// </summary>
        /// <param name="elem">Element to retrieve solid geometry.</param>
        /// <param name="opt">geometry option.</param>
        /// <returns>Solid of the geometry.</returns>
        public static List<Solid> GetElementSolids(Element elem,
            Options opt = null)
        {
            if (null == elem) return null;

            if (null == opt) opt = new Options();
            var solids = new List<Solid>();
            GeometryElement gElem = null;
            try
            {
                gElem = elem.get_Geometry(opt);
                var glter = gElem.GetEnumerator();
                glter.Reset();
                while (glter.MoveNext()) solids.AddRange(GetSolids(glter.Current));
            }
            catch (Exception ex)
            {
                //In Revit,sometime get the geometry will failed.
                var error = ex.Message;
            }

            return solids;
        }

        public static List<Solid> GetSolids(GeometryObject gobj)
        {
            var solids = new List<Solid>();
            if (gobj is Solid) //already solid
            {
                var solid = gobj as Solid;
                if (solid.Faces.Size > 0 && solid.Volume > 0) //some solid may have not any face?!
                {
                    solids.Add(gobj as Solid);
                }
                else if (gobj is GeometryObject) //find solids from GeometryInstance
                {
                    var glter2 =
                        (gobj as GeometryInstance).GetInstanceGeometry().GetEnumerator();
                    glter2.Reset();
                    while (glter2.MoveNext()) solids.AddRange(GetSolids(glter2.Current));
                }
                else if (gobj is GeometryElement) //find solids from GeometryElement,this will not happen at all?
                {
                    var glter2 = (gobj as GeometryElement).GetEnumerator();
                    glter2.Reset();
                    while (glter2.MoveNext()) solids.AddRange(GetSolids(glter2.Current));
                }
            }

            return solids;
        }

        /// <summary>
        ///     Draws model curves by given geometry curves.
        /// </summary>
        /// <param name="revitDoc"></param>
        /// <param name="curves"></param>
        /// <param name="reviseTrf"></param>
        /// <returns></returns>
        public static List<ElementId> DrawModelCurves(Document revitDoc, List<Curve> curves, Transform reviseTrf = null)
        {
            var newIds = new List<ElementId>();
            foreach (var crv in curves)
                if (crv.IsBound)
                {
                    var reviseCrv = null != reviseTrf && !reviseTrf.IsIdentity
                        ? crv.CreateTransformed(reviseTrf)
                        : crv;
                }

            return newIds;
        }

        private static ElementId creatModelCurve(Document document, Curve curve, SketchPlane sp = null)
        {
            var line = curve as Line;
            var arc = curve as Arc;
            var ellipse = curve as Ellipse;
            var spline = curve as HermiteSpline;
            var nvSpline = curve as NurbSpline;
            if (line != null && null == sp)
            {
                var normal = getVertVec(line.Direction).Normalize();
                var origin = line.GetEndPoint(0);
#if Revit2016
                sp = SketchPlane.Create(document ,Plane.CreateByNormalAndOrigin(normal,origin));
#endif
#if Revit2019
                sp = SketchPlane.Create(document ,Plane.CreateByNormalAndOrigin(normal,origin;))
#endif
            }
            else if (arc != null && null == sp)
            {
                var normal = arc.Normal;
#if Revit2016
                sp = SketchPlane.Create(document,new Plane(normal,arc.Center));
#endif
#if Revit2019
                sp = SketchPlane.Create(document,Plane.CreateByNormalAndOrigin(normal,arc.Center;))
#endif
            }
            else if (ellipse != null && null == sp)
            {
                var normal = ellipse.Normal;
#if Revit2016
                sp = SketchPlane.Create(document,new Plane(normal, ellipse.Center));
#endif
#if Revit2019
                sp = SketchPlane.Create(document,Plane.CreateByNormalAndOrigin(normal,ellipse.Center));
#endif
            }
            else if (spline != null && null == sp)
            {
                var tran = spline.ComputeDerivatives(0, false);
                var normal = getVertVec(tran.BasisX).Normalize();
                var origin = spline.GetEndPoint(0);
#if Revit2016
                sp = SketchPlane.Create(document ,new Plane(normal,origin));
#endif
#if Revit2019
                sp = SketchPlane.Create(document, Plane.CreateByNormalAndOrigin(normal, origin));
#endif
            }

            if (sp == null)
                throw new ArgumentException("Not valid sketchplane to create curve:" + "curve.GetType().Name");
            //
            //create model line with curve and the specified sketch plane.
            var mCurve = document.Create.NewModelCurve(curve, sp);
            return null != mCurve ? mCurve.Id : ElementId.InvalidElementId;
        }

        /// <summary>
        ///     Gets one vertical vector of given vector,the returen vector is not normalized.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        private static XYZ getVertVec(XYZ vec)
        {
            var ret = new XYZ(-vec.Y + vec.Z, vec.X + vec.Z, -vec.Y - vec.X);
            return ret;
        }

        public static bool CanMakeBound(XYZ end0, XYZ end1)
        {
            try
            {
                var ln = Line.CreateBound(end0, end1);
                return null != ln;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}