﻿using System;
using Autodesk.Revit.DB;
using Revit_Secondary_Development.唐僧解瓦.BinLibrary.Extensions;

namespace Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Extensions
{
    public static class PointExtension2
    {
        /// <summary>
        ///  投影到线上
        /// </summary>
        /// <param name="po"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        public static XYZ ProjectToXline(this XYZ po,Line l)
        {
            Line l1 = l.Clone() as Line;
            if (l1.IsBound)
            {
                l1.MakeUnbound();
            }
            return l1.Project(po).XYZPoint;
        }
        private static double precision = 0.000001;
        /// <summary>
        /// 判断两double数值是否相等
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static bool IsEqual(double d1,double d2)
        {
            double diff = Math.Abs(d1 - d2);
            return diff < precision;
        }
        /// <summary>
        /// 判断点是否在线段上
        /// </summary>
        /// <param name="p"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        public static bool IsOnline(this XYZ p,Line l)
        {
            XYZ end1=l.GetEndPoint(0);
            XYZ end2 = l.GetEndPoint(1);

            XYZ vec_pToEnd1 = end1 - p;
            XYZ vec_pToEnd2 = end2 - p;

            double precision = 0.0000001d;

            if (p.DistanceTo(end1)<precision||p.DistanceTo(end2)<precision)
            {
                return true;
            }
            if (vec_pToEnd1.IsOppositeDirection(vec_pToEnd2))
            {
                return true;
            }
            return false;
        }/// <summary>
        /// 判断点是否在线 或线的延长线上
        /// </summary>
        /// <param name="p"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        public static bool IsXOnLine(this XYZ p,Line l)
        {
            double precison = 0.0000001d;
            var l1 = l.Clone() as Line;
            l1.MakeUnbound();
            if (p.DistanceTo(l1)<precison)
            {
                return true;
            }
            return false;
        }
        public static double DistanceTo(this XYZ p1,Line xline)
        {
            double result = double.NegativeInfinity;

            XYZ p1_onLine = p1.ProjectToXline(xline);

            result = p1.DistanceTo(p1_onLine);

            return result;
        }
    }
}
