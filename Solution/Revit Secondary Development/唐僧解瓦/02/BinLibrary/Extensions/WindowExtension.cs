﻿using System.Windows;
using System.Windows.Interop;

namespace Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Extensions
{
    public static class WindowExtension
    {
        public static WindowInteropHelper helpers(this Window win)
        {
            var helper = new WindowInteropHelper(win);
            return helper;
        }
    }
}
