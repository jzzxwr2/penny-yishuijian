﻿using System.Linq;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Extensions
{
    public static class UIDocumentExtension
    {
        public static UIView ActiveUiView(this UIDocument uidoc)
        {
            var result = default(UIView);
            var doc = uidoc.Document;
            var active = doc.ActiveView;

            var uiviews = uidoc.GetOpenUIViews();
            result = uiviews.FirstOrDefault(m => m.ViewId == active.Id);
            return result;
        }
    }
}
