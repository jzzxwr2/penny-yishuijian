﻿using Autodesk.Revit.DB;
using Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Helpers;
using System;
using static Revit_Secondary_Development.唐僧解瓦.BinLibrary.Extensions.TransactionExtension;

namespace Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.Extensions
{
    public static class TransactionExtension
    {
        public static void IgnoreFailure(this Transaction trans)
        {
            var options = trans.GetFailureHandlingOptions();
            options.SetFailuresPreprocessor(new failure_ignore());
        }
        public static void Invoke(this Document doc,Action<Transaction> action,string transactionName="aaa")
        {
            Transaction ts = new Transaction(doc, transactionName);
            LogHelper.LogException(delegate
            {
                ts.Start();
                action(ts);
                ts.Commit();
            }, @"c:\transactionException.txt");
        }
    }
    //public class failure_ignore:IFailuresPreprocessorf
    //{
    //    public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
    //    {
    //        failuresAccessor.DeleteAllWarnings();
    //        //failuresAccessor.DeleteElements(failuresAccessor.el);
    //        return FailureProcessingResult.Continue;
    //    }
    //}
}
