﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;

namespace Revit_Secondary_Development.唐僧解瓦._09.BinLibrary.Extensions
{
    public static class LineExtension
    {
        public static XYZ StartPoint(this Line line)
        {
            if (line.IsBound)
            {
                return line.GetEndPoint(0);
            }

            return null;
        }

        public static XYZ EndPoint(this Line line)
        {
            if (line.IsBound)
            {
                return line.GetEndPoint(1);
            }

            return null;
        }

        public static XYZ Intersect_cus(this Line line, Plane p)
        {
            var lineorigin = line.Origin;
            var linddir = line.Direction;

            var pointOnLine = lineorigin + linddir;

            var trans = Transform.Identity;
            trans.Origin = p.Origin;
            trans.BasisX = p.XVec;
            trans.BasisY = p.YVec;
            trans.BasisZ = p.Normal;

            var point1 = lineorigin;
            var point2 = pointOnLine;

            var point1Intrans = trans.Inverse.OfPoint(point1);
            var point2Intrans = trans.Inverse.OfPoint(point2);

            point1Intrans = new XYZ(point1Intrans.X, point1Intrans.Y, 0);
            point2Intrans = new XYZ(point2Intrans.X, point2Intrans.Y, 0);

            var point1Inworld = trans.OfPoint(point1Intrans);
            var point2Inworld = trans.OfPoint(point2Intrans);

            var newlineInPlan = Line.CreateBound(point1Inworld, point2Inworld);

            var unboundOriginalLine = line.Clone() as Line;
            unboundOriginalLine.MakeUnbound();

            return unboundOriginalLine.Intersect_cus(unboundOriginalLine);
        }

        public static XYZ Intersect_cus(this Line line1, Line line2)
        {
            var compareResults = line1.Intersect(line2, out IntersectionResultArray intersectResult);

            if (compareResults != SetComparisonResult.Disjoint) ;
            {
                var result = intersectResult.get_Item(0).XYZPoint;
                return result;
            }
            return null;
        }
    }
}
