﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Revit_Secondary_Development.唐僧解瓦._09.BinLibrary.Helpers
{
    public static class LogHelper
    {
        public static void LogException(Action action, string path)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                LogWrtite(e.ToString(),path);
            }
        }
        public static void LogWrtite(string msg, string path, bool append = false)
        {
            StreamWriter sw = new StreamWriter(path, append);
            sw.WriteLine(msg);
            sw.Close();
        }
    }
}
