﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.UI;
using Revit_Secondary_Development.唐僧解瓦._03.Test.UIs;
using Revit_Secondary_Development.唐僧解瓦._3.BinLibrary.RevitHelper;
using Revit_Secondary_Development.唐僧解瓦.BinLibrary.Helpers;
namespace Revit_Secondary_Development.唐僧解瓦._03.Test
{
    /// <summary>
    /// 在轴线交点处生成柱子
    /// </summary>
    [Transaction(TransactionMode.Manual)]
    [Journaling(JournalingMode.UsingCommandData)]
    [Regeneration(RegenerationOption.Manual)]
    class Cmd_CreateColumnAccordingGridIntersection : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uiapp = commandData.Application;
            var uidoc = uiapp.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;
            var acview = doc.ActiveView;
            ElementFilter arcitectureColunmFilter = new ElementCategoryFilter(BuiltInCategory.OST_Columns);
            ElementFilter structuralColumnFilter = new ElementCategoryFilter(BuiltInCategory.OST_StructuralColumns);
            ElementFilter orfilter = new LogicalOrFilter(arcitectureColunmFilter, structuralColumnFilter);
            var collector = new FilteredElementCollector(doc);
            var columnsypes = collector.WhereElementIsElementType().WherePasses(orfilter).ToElements();
            ColumnTypesForm typesform = ColumnTypesForm.Getinstance(columnsypes.ToList());
            typesform.ShowDialog(RevitWindowsHelper.GetRevitWindow());
            var familysymbol = typesform.symbolCombo.SelectedItem as FamilySymbol;
            var bottomlevel = default(Level);
            var bottomoffset = default(double);
            var toplevel = default(Level);
            var topoffset = default(double);
            var grids = doc.TCollector<Grid>();
            var points = new List<XYZ>();
            foreach (var grid in grids)
            {
                foreach (var grid1 in grids)
                {
                    if (grid.Id==grid1.Id)
                    {
                        continue;
                    }
                    var curve1 = grid.Curve;
                    var curve2 = grid1.Curve;
                    var res = new IntersectionResultArray();
                    var intersecRes = curve1.Intersect(curve2, out res);
                    if (intersecRes!=SetComparisonResult.Disjoint)
                    {
                        if (res!=null)
                        {
                            points.Add(res.get_Item(0).XYZPoint);
                        }
                    }
                }
            }
            points = points.Where((m, i) => points.FindIndex(n => n.IsAlmostEqualTo(m)) == i).ToList();
            TransactionGroup tsg = new TransactionGroup(doc);
            tsg.Start("统一创建柱子");
            foreach (var point in points)
            {
                doc.Invoke(m =>
                {
                    if(!familysymbol.IsActive) familysymbol.Activate();
                    var instance = doc.Create.NewFamilyInstance(point, familysymbol, acview.GenLevel,StructuralType.NonStructural);
                },"创建柱子");
            }
            tsg.Assimilate();
            return Result.Succeeded;
        }
        public XYZ Intersect_cus(Curve c, Curve c1)
        {
            XYZ result = null;
            IntersectionResultArray resultArray = new IntersectionResultArray();
            var comparisonResult = c.Intersect(c1, out resultArray);
            if (comparisonResult!=SetComparisonResult.Disjoint)
            {
                if (resultArray != null)
                    result = resultArray.get_Item(0).XYZPoint;
            }
            return result;
        }
    }
}
