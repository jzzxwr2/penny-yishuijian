﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Revit_Secondary_Development.唐僧解瓦._03.Test.UIs;
using Revit_Secondary_Development.唐僧解瓦._3.BinLibrary.RevitHelper;
using RevitWindowsHelper = Revit_Secondary_Development.唐僧解瓦._2.BinLibrary.RevitHelper.RevitWindowsHelper;


namespace Revit_Secondary_Development.唐僧解瓦._3.Test
{
    /// <summary>
    /// 工具说明
    /// </summary>
    [Transaction(TransactionMode.Manual)]
    [Journaling(JournalingMode.UsingCommandData)]
    [Regeneration(RegenerationOption.Manual)]
    class Cmd_About : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var uiapp = commandData.Application;
            var uidoc = uiapp.ActiveUIDocument;
            var doc = uidoc.Document;
            var sel = uidoc.Selection;

            AboutForm form = new AboutForm();
            form.Show(RevitWindowsHelper.GetRevitWindow());

            return Result.Succeeded;
        }
    }
}
