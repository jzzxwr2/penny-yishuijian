﻿using Autodesk.Revit.DB;

namespace Revit_Secondary_Development.唐僧解瓦._3.BinLibrary.Extensions
{
    public static class MepcurveExtension
    {
        public static Line LocationLine(this MEPCurve mep)
        {
            Line result = null;

            result = (mep.Location as LocationCurve).Curve as Line;

            return result;
        }
    }
}
