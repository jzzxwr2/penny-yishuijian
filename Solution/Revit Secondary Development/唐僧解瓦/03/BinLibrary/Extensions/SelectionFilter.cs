﻿using System;
using System.Runtime.Remoting.Messaging;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace Revit_Secondary_Development.唐僧解瓦._3.BinLibrary.Extensions
{
    public class SelectionFilter:ISelectionFilter
    {
        private Document _doc;
        private Type _type;

        public bool AllowElement(Element elem)
        {
            if (elem.GetType()==_type)
            {
                return true;
            }

            return false;
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return true;
        }
    }

    public static class SelectionFilterHelper
    {
        public static MultiSelectionFilter GetSelectionFilter(this Document doc,Func<Element,bool> func1, Func<Reference, bool> func2 = null)
        {
            return new MultiSelectionFilter(func1,func2);
        }
        
    }

    public class MultiSelectionFilter : ISelectionFilter
    {
        private Func<Element, bool> eleFunc;
        private Func<Reference, bool> refFunc;

        public MultiSelectionFilter(Func<Element,bool> func,Func<Reference,bool> func1)
        {
            eleFunc = func;
            refFunc = func1;
        }
        public bool AllowElement(Element elem)
        {
            return refFunc != null ? true : eleFunc(elem);
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return refFunc==null?false:refFunc(reference);
//            return refFunc;
        }
    }
}
