﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace Revit_Secondary_Development.PracticeR
{
    //class PR01
    //{
    //    static void main(string[] args)
    //    {
    //        var type = typeof(Element);
    //        var methods = type.GetMethods(System.Reflection.BindingFlags.Public 
    //            | System.Reflection.BindingFlags.Static);
    //        var groups = methods.GroupBy(m => m.Name);
    //        Console.WriteLine(groups.Count().ToString());
    //        foreach (var item in groups)
    //        {
    //            Console.WriteLine(item.First().Name);
    //        }
    //    }
    //}

    //[Regeneration(RegenerationOption.Manual)]
    [Transaction(TransactionMode.Manual)]
    class ElementMemebersTest : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {

            var type = typeof(Element);
            var members = type.GetMembers();

            foreach (var item in members)
            {
                Debug.WriteLine(item.Name);

            }
            //Console.WriteLine();

             
            return Result.Succeeded;
        }

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            throw new NotImplementedException();
        }
    }
}
