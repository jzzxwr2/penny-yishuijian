﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

namespace WinApi.WindowsSystem
{
    class GetSystemInfo
    {
        /// <summary>
        /// 返回任务栏所在窗口类Shell_TrayWnd的句柄
        /// </summary>
        /// <param name="IpClassName"></param>
        /// <param name="IpWindowName"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int FindWindow(string IpClassName, string IpWindowName);

        /// <summary>
        /// 获得任意一个窗口的矩阵范围的函数
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="IpRect"></param>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern int GetWindowRect(int hwnd, ref Rectangle IpRect);

        //41
    }
}
