﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace WinApi.MouseApi
{
    class Mouse
    {
        /// <summary>
        /// 获取鼠标双击时间间隔
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll", EntryPoint = "GetDoubleClickTime")]
        public extern static int GetDoubleClickTime();

        /// <summary>
        /// 获取光标闪烁的频率
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll", EntryPoint  = "GetCaretBlinkTime")]
        public extern static int GetCaretBlinkTime();

        /// <summary>
        /// 获取鼠标键数
        /// </summary>
        /// <param name="intcount"></param>
        /// <returns></returns>
        [DllImport("user32", EntryPoint = "GetSystemMetrics")]
        public static extern int GetSystemmetrics(int intcount);

        /// <summary>
        /// 在一个指针文件或一个动画指针文件（扩展名分别是.cur和.ani)的基础上创建一个指针
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr LoadCursorFromFile(string fileName);

        /// <summary>
        /// 将要修改的鼠标图片存入到指定目录下
        /// </summary>
        /// <param name="IpFileName"></param>
        /// <returns></returns>
        [DllImport("user32", EntryPoint = "LoadCursorFromFile")]
        public static extern int IntLoadCursorFromFile(string IpFileName);

        /// <summary>
        /// 改变任何一个标准系统指针
        /// </summary>
        /// <param name="hcur"></param>
        /// <param name="id"></param>
        [DllImport("user32", EntryPoint = "SetSystemCursor")]
        public static extern void SetSystemCursor(int hcur, int id);

        /// <summary>
        /// 交互鼠标左右键功能
        /// </summary>
        /// <param name="bSwap"></param>
        /// <returns></returns>
        [DllImport("user32.dll", EntryPoint = "SwapMouseButton")]
        public extern static int SwapMouseButton(int bSwap);

        /// <summary>
        /// 安装钩子
        /// </summary>
        /// <param name="HookType"></param>
        /// <param name="methodAddress"></param>
        /// <param name="handler"></param>
        /// <param name="dwThreadId"></param>
        /// <returns></returns>
//        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall,
//            SetLastError = true)]
//        public static extern int SetWindowsHookEx(int HookType, HOOKPROCEDURE methodAddress, IntPtr handler,
//            int dwThreadId);

        /// <summary>
        /// 释放钩子
        /// </summary>
        /// <param name="idHook"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall,
            SetLastError = true)]
        public static extern bool UnhookWindowsHookEx(IntPtr idHook);


        /// <summary>
        /// 安装系统热键
        /// </summary>
        /// <param name="Hwnd"></param>
        /// <param name="Id"></param>
        /// <param name="keyModifiers"></param>
        /// <param name="key"></param>
        /// <returns></returns>
//        [DllImport("user32.dll", SetLastError = true)]
//        public static extern bool RegisterHotKey(IntPtr Hwnd, int Id, KeyModifiers keyModifiers, Keys key);
        //14
    }
}
