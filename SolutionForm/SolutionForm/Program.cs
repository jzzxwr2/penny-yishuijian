﻿using SolutionForm.Practice.P10File.P10E1;
using SolutionForm.Practice.P15E6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolutionForm.Practice.P10File.P10E2;
using SolutionForm.Practice.P10File.P10E3GetDirecInfo;
using SolutionForm.Practice.P10File.P10E4CreateDirc;
using SolutionForm.Practice.P10File.P10E5ClassifyFileByType;
using SolutionForm.Practice.P00.P00E1Switch;
using SolutionForm.Practice.P10File.P10E7GetFileLength;
using SolutionForm.Practice.P10File.P10E7GetFileInfo;
using SolutionForm.Practice.P10File.P10E2CreateFile;
using SolutionForm.Practice.P10File.P10E7RecycleBin;
using SolutionForm.Csharp1200;
using SolutionForm.Common.Utility.CSV文件转换.CsvHelper;
using SolutionForm.Common.Utility;
using SolutionForm.Common.Utility.Excel操作类.DataToExcel;

namespace SolutionForm
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new N11N02());
        }
    }
}
