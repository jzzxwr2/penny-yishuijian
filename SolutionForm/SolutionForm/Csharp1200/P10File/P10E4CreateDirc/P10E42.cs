﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E7RecycleBin
{
    //364
    public partial class P10E42 : Form
    {
        public P10E42()
        {
            InitializeComponent();
        }

        private const int SHERB_NOCONFIRNATION = 0x000001;
        private const int SHERB_NOPROGRESSUI = 0X000002;
        private const int SHERB_NOSOUND = 0X000004;

        [DllImport("shell32.dll")]
        private static extern int SHEmptyRecycleBin(IntPtr handle, string root, int flags);
        private void button1_Click(object sender, EventArgs e)
        {
            SHEmptyRecycleBin(this.Handle, "", SHERB_NOCONFIRNATION + SHERB_NOPROGRESSUI + SHERB_NOSOUND);
        }
    }
}
