﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E4CreateDirc
{
    public partial class P10E40 : Form
    {
        public P10E40()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog()==DialogResult.OK)
            {
                string strPath = fbd.SelectedPath;
                if (strPath.EndsWith("\\"))
                    textBox1.Text = strPath;
                else
                    textBox1.Text = strPath + "\\";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string strName = DateTime.Now.ToString("yyyy MMMM dd");
            DirectoryInfo dinfo = new DirectoryInfo(textBox1.Text + strName);
            dinfo.Create();
            MessageBox.Show("文件夹创建成功，名称为" + strName + "");

        }
    }
}
