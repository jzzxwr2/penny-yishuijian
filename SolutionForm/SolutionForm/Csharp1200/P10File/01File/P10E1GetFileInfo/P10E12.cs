﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E1
{
    public partial class P10E12 : Form
    {
        public P10E12()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                FileInfo finfo = new FileInfo(textBox1.Text);
                string ctime = finfo.CreationTime.ToShortDateString();
                string name = finfo.Name;
                string fname = finfo.FullName;
                string dname = finfo.DirectoryName;
                MessageBox.Show(ctime + "\n" + name + "\n" + fname + "\n" + dname);
            }
        }

        private void P10E12_Load(object sender, EventArgs e)
        {

        }
    }
}
