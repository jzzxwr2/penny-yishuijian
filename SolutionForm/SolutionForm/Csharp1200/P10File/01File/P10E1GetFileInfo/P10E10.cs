﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E1
{
    public partial class P10E10 : Form
    {
        public P10E10()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                FileInfo finfo = new FileInfo(textBox1.Text);
                string time = finfo.CreationTime.ToShortDateString();
                string latime = finfo.LastAccessTime.ToShortDateString();
                string lwtime = finfo.LastWriteTime.ToShortDateString();
                string name = finfo.Name;
                string fname = finfo.FullName;
                string dname = finfo.DirectoryName;
                string isread = finfo.IsReadOnly.ToString();
                long length = finfo.Length;
                MessageBox.Show(time + "\n" + latime + "\n" + lwtime + "\n" + name + "\n" + fname + "\n" + dname +
                                "\n" + isread + "\n" + length);

            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void openFileDialog1_FileOk_1(object sender, CancelEventArgs e)
        {

        }
    }
}
