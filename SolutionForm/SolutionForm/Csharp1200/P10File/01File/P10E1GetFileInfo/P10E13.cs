﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E1
{
    public partial class P10E13 : Form
    {
        public P10E13()
        {
            InitializeComponent();
        }
        
        private void P10E13_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                FileInfo finfo = new FileInfo(textBox1.Text);
                string ctime = finfo.CreationTime.ToShortDateString();
                string fname = finfo.FullName;
                MessageBox.Show(ctime + fname);
            }
        }
    }
}
