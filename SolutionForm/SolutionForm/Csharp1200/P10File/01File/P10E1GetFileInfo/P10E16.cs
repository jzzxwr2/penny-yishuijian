﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E7GetFileInfo
{
    public partial class P10E16 : Form
    {
        public P10E16()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                richTextBox1.Text += c;
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void P10E16_Load(object sender, EventArgs e)
        {

        }
    }
}
