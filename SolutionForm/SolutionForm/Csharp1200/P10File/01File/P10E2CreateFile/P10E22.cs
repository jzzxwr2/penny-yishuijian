﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E2
{
    //c#开发实战1200例 实例363 根据日期动态地建立文件
    public partial class P10E22 : Form
    {
        public P10E22()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog()==DialogResult.OK)
            {
                System.IO.File.Create(fbd.SelectedPath+"\\"+DateTime.Now.ToString("yyyyMMdd")+".txt");
            }
        }
    }
}
