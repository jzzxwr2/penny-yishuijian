﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E2CreateFile
{
    //实例361 生成随机文件名或文件夹名
    public partial class P10E24 : Form
    {
        public P10E24()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog()==DialogResult.OK)
            {
                //                File.Create(folderBrowserDialog1.SelectedPath + "\\" + Guid.NewGuid().ToString() + ".txt");
                System.IO.File.Create(folderBrowserDialog1.SelectedPath + "\\" + Guid.NewGuid() + ".doc");
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog()==DialogResult.OK)
            {
//                Directory.CreateDirectory(folderBrowserDialog1.SelectedPath + "\\" + Guid.NewGuid().ToString());
                Directory.CreateDirectory(folderBrowserDialog1.SelectedPath + "\\" + Guid.NewGuid());
            }
        }
    }
}
