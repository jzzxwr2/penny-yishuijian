﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E2
{
    public partial class P10E21 : Form
    {
        public P10E21()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            if (f.ShowDialog()==DialogResult.OK)
            {
                System.IO.File.Create(f.SelectedPath + "\\" + DateTime.Now.ToString("yyMMdd") + ".txt");
            }
        }
    }
}
