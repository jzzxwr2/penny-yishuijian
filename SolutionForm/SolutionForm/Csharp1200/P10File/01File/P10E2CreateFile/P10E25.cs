﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E2CreateFile
{
    //c#开发实战1200例 实例362 建立临时文件
    public partial class P10E25 : Form
    {
        public P10E25()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
//            textBox1.Text = Path.GetTempFileName();
//            FileInfo fin = new FileInfo(textBox1.Text);
//            fin.AppendText();
            textBox1.Text = Path.GetTempFileName();
            FileInfo fin = new FileInfo(textBox1.Text);
            fin.AppendText();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
