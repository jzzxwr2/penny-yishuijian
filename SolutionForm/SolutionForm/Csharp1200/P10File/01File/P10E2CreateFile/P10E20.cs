﻿using System;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E2
{
    public partial class P10E20 : Form
    {
        public P10E20()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            if (f.ShowDialog()==DialogResult.OK)
            {
                System.IO.File.Create(f.SelectedPath + "\\" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt");
            }
        }
    }
}
