﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P10File.P10E5ClassifyFileByType
{
    public partial class P10E50 : Form
    {
        public P10E50()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog()==DialogResult.OK)
            {
                string strPath = fbd.SelectedPath;
                if (strPath.EndsWith("\\"))
                {
                    textBox1.Text = strPath;
                }
                else
                {
                    textBox1.Text = strPath + "\\";
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<string> listExten = new List<string>();
            DirectoryInfo dinfo = new DirectoryInfo(textBox1.Text);
            FileInfo[] finfos = dinfo.GetFiles();
            string strExten = "";
            foreach (FileInfo finfo in finfos)
            {
                strExten = finfo.Extension;
                if (!listExten.Contains(strExten))
                {
                    listExten.Add(strExten.TrimStart('.'));

                }
            }

            for (int i = 0; i < listExten.Count; i++)
            {
                Directory.CreateDirectory(textBox1.Text + listExten[i]);

            }

            foreach (FileInfo finfo in finfos)
            {
                finfo.MoveTo(textBox1.Text+finfo.Extension.TrimStart('.')+"\\"+finfo.Name);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(textBox1.Text);
        }
    }
}
