﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P00.P00E1Switch
{
    public partial class P00E11 : Form
    {
        public P00E11()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    this.BackColor=Color.Red;
                    break;
                case 1:
                    this.BackColor=Color.Green;
                    break;
                case 2:
                    this.BackColor=Color.Blue;
                    break;

            }
        }

        private void P00E11_Load(object sender, EventArgs e)
        {
            string[] str = new string[] {"红", "绿", "蓝"};
            comboBox1.DataSource = str;
        }
    }
}
