﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Practice.P00.P00E1Switch
{
    public partial class P00E10 : Form
    {
        public P00E10()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex + 1)
            {
                case 3:
                case 4:
                case 5:
                    MessageBox.Show("春", "提示");
                    break;
                case 6:
                case 7:
                case 8:
                    MessageBox.Show("夏");
                    break;
                case 9:
                case 10:
                case 11:
                    MessageBox.Show("秋");
                    break;
                case 12:
                case 1:
                case 2:
                    MessageBox.Show("冬");
                    break;
                default:
                    MessageBox.Show("请选择月份");
                    break;
 
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void P00E10_Load(object sender, EventArgs e)
        {
//            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            string[] str = new string[] { "1月", "2月","3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月", };
            comboBox1.DataSource = str;
//            comboBox1.SelectedIndex = 0;
        }
    }
}
