﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Common.Utility
{
    public partial class N02N01 : Form
    {
        public N02N01()
        {
            InitializeComponent();
            DataSet ds = new DataSet();

            DataTable dt = new DataTable("Student");
            ds.Tables.Add(dt);

            //dataGridView1.DataSource = ds.Tables[0];
            ds.Tables.Add(new DataTable("Course"));
            ds.Tables.Add(new DataTable("Sourse"));

            DataColumn dc = new DataColumn();
            dc.ColumnName = "SNO";
            dc.DataType = Type.GetType("System.Int32");
            dt.Columns.Add(dc);

            dt.Columns.Add(new DataColumn("SName", Type.GetType("System.String")));

            DataRow dr = dt.NewRow();
            dr["SNO"] = 1;
            dr["SName"] = "a";
            dt.Rows.Add(dr);

            dt.Rows.Add(new object[] {2, "b"});
            dt.Rows.Add(new object[] {3, "c"});
            dt.Rows.Add(new object[] {4, "d"});
            dt.Rows.Add(new object[] {5, "e"});
            dt.Rows.Add(new object[] {6, "f"});

            dgvStudent.DataSource = ds.Tables["Student"];


        }

        private void N02N01_Load(object sender, EventArgs e)
        {
            /*DataTable dt = new DataTable();
            DataTable dt2 = new DataTable("table_new");

            DataColumn dc = new DataColumn();
            dt.Columns.Add(dc);

            dt.Columns.Add("column0", typeof(String));

            DataColumn dc1 = new DataColumn("column1", typeof(DateTime));
            dt.Columns.Add(dc1);

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);

            dt.Rows.Add();

            dt.Rows.Add("a", DateTime.Now);
            dt.Rows.Add(dt2.Rows[0].ItemArray);*/



        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
