﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Common.Utility.Excel操作类.DataToExcel
{
    public partial class N11N01 : Form
    {
        public N11N01()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*SqlConnection conn=new SqlConnection("Data Source=127.0.0.1;InitialCatalog="stu_db";User ID=sa;Password=123456;");*/

            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            scsb.DataSource = "127.0.0.1";
            scsb.UserID = "sa";
            scsb.Password = "123456";
            scsb.InitialCatalog = "stu_db";

            SqlConnection conn = new SqlConnection(scsb.ToString());
            conn.Open();

            string stu_no = Console.ReadLine();
            string sqlStr = "select * from students where number='" + stu_no + "'";

            SqlDataAdapter adapter = new SqlDataAdapter(sqlStr,conn);

            /*SqlCommand cmdSelect = new SqlCommand("select*from score");*/


            /*SqlCommand cmdSelect = new SqlCommand(sqlStr, conn);
            cmdSelect.Connection = conn;
            adapter.SelectCommand = cmdSelect;*/

            DataSet ds = new DataSet();
            adapter.Fill(ds, "学生");

            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "学生";

            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                string path = openFileDialog1.FileName;
                DSToExcel(openFileDialog1.FileName, ds);
                MessageBox.Show("写入结束");
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                DataSet da = ExcelToDS(openFileDialog1.FileName);
                dataGridView1.DataSource = da;
                dataGridView1.DataMember = "学生";
            }
        }
        /// <summary>
        /// DataSet格式写入excel
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="oldds"></param>
        public void DSToExcel(string Path, DataSet oldds)
        {
            string strCon = "Provider=Microsoft.Jet.OLEDB.4.0" + "Data Source=" + Path + ";" +
                            "Extended Properties=Excel 8.0";
            OleDbConnection myConn = new OleDbConnection(strCon);
            string strCom = "select*from[Sheet1$]";
            OleDbDataAdapter myCommand = new OleDbDataAdapter(strCom, myConn);
            OleDbCommandBuilder builder = new OleDbCommandBuilder(myCommand);

            builder.QuotePrefix = "[";
            builder.QuoteSuffix = "]";
            DataSet newds = new DataSet();
            myCommand.Fill(newds, "Table1");
            for (int i = 0; i < oldds.Tables[0].Rows.Count; i++)
            {
                DataRow nrow = newds.Tables["Table1"].NewRow();
                for (int j = 0; j < oldds.Tables[0].Columns.Count; j++)
                {
                    nrow[j] = oldds.Tables[0].Rows[i][j];
                }

                newds.Tables["Table1"].Rows.Add(nrow);
            }

            myCommand.Update(newds, "Table1");
            myConn.Close();
        }
        /// <summary>
        /// 读取excel
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        public DataSet ExcelToDS(string Path)
        {
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + Path + ";" +
                             "Extended Properties=Excel 8.0";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            string strExcel = "";
            OleDbDataAdapter myCommand = null;
            DataSet ds = null;
            strExcel = "select*from[sheet1$]";
            myCommand = new OleDbDataAdapter(strExcel, strConn);
            ds = new DataSet();
            myCommand.Fill(ds, "table1");
            return ds;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void N11N01_Load(object sender, EventArgs e)
        {

        }
    }
}
