﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolutionForm.Common.Utility.Excel操作类.DataToExcel
{
    public partial class N11N02 : Form
    {
        public N11N02()
        {
            InitializeComponent();
        }

        private DataTable dt;
        private SqlConnection conn;
        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            scsb.DataSource = "127.0.0.1";
            scsb.UserID = "sa";
            scsb.Password = "123456";
            scsb.InitialCatalog = "stu_db";

            conn = new SqlConnection(scsb.ToString());
            if (conn.State==ConnectionState.Closed)
            {
                conn.Open();
            }

            //string strSql = "select * from students";
            string strSql = "select * from students where number like '%"+textBox1.Text.Trim()+ "'";

            SqlDataAdapter da = new SqlDataAdapter(strSql, conn);

            DataSet ds = new DataSet();
            da.Fill(ds, "学生");

            /*dataGridViewStu.DataSource = ds;
            dataGridViewStu.DataMember = "学生";*/

            /*dataGridViewStu.DataSource = ds.Tables["学生"];*/

            dt = ds.Tables["学生"];
            dataGridViewStu.DataSource = dt.DefaultView;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable changeDt=dt.GetChanges();
            
            foreach (DataRow dr in changeDt.Rows)
            {
                string strSQL = string.Empty;
                if (dr.RowState == DataRowState.Added)
                {
                    //MessageBox.Show(dr["name"].ToString());
                    strSQL = @"INSERT INTO [dbo].[students]
                                    ([number]
                                    ,[name]
                                    ,[gender]
                                    ,[age]
                                    ,[grade])
                                VALUES
                                    ('"+dr["number"].ToString()+ @"'
                                    , '" + dr["name"].ToString() + @"'
                                    , '" + dr["gender"].ToString() + @"'
                                    , "+Convert.ToInt32(dr["age"]) + @"
                                    , '"+dr["grade"].ToString() + @"')";
                }
                else if (dr.RowState==DataRowState.Deleted)
                {
                    
                }
                else if (dr.RowState == DataRowState.Modified)
                {

                }
                SqlCommand comm = new SqlCommand(strSQL, this.conn);
                comm.ExecuteNonQuery();
                //MessageBox.Show(dr.RowState.ToString());
                //MessageBox.Show(dr["number"].ToString());
            }
        }
    }
}
